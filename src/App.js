import React from "react";
import { Provider } from "react-redux";
import { ThemeProvider } from "@material-ui/core";
import { PersistGate } from "redux-persist/integration/react";
import getStore from "./store";
import Routes from "./Routes";
import theme from "./Theme";

const { store, persistor } = getStore();

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ThemeProvider theme={theme}>
          <Routes />
        </ThemeProvider>
      </PersistGate>
    </Provider>
  );
}

export default App;
