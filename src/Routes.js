import React from "react";
import { Switch, Route } from "react-router-dom";
import Inicio from "./screens/Inicio";
import Jugar from "./screens/Jugar";
import Quiniela from "./screens/Quiniela";
import Quiniela1 from "./screens/Quiniela1";
import QuinielaPlus from "./screens/QuinielaPlus";

import Billete from "./screens/Billete";
import Brinco from "./screens/Brinco";
import Loto from "./screens/Loto";
import Loto5 from "./screens/Loto5";
import Quini6 from "./screens/Quini6";
import Login from "./screens/Login";
import Register from "./screens/Register";

import MisJugadas from "./screens/MisJugadas";
import MisResultados from "./screens/MisResultados";
import Estadisticas from "./screens/Estadisticas";
import Pozos from "./screens/Pozos";

import Carrito from "./screens/Carrito";
import ModalTeminosCondiciones from "./screens/ModalTeminosCondiciones";
import CodigoCliente from "./screens/CodigoCliente";
import Perfil from "./screens/Perfil";
import PagoTarjeta from "./screens/PagoTarjeta";
import RoutePrivate from "./components/RoutePrivate";

export default function Routes() {
  return (
    <Switch>
      <Route exact name="Login" path="/Login" component={Login} />
      <Route exact name="Register" path="/Register" component={Register} />
      <RoutePrivate exact name="Inicio" path="/:payment/:id" component={Inicio} />
      <RoutePrivate exact name="Inicio" path="/" component={Inicio} />
      <RoutePrivate exact name="Jugar" path="/Jugar" component={Jugar} />
      <RoutePrivate
        exact
        name="Quiniela"
        path="/Quiniela"
        component={Quiniela}
      />
      <RoutePrivate
        exact
        name="Quiniela1"
        path="/Quiniela1"
        component={Quiniela1}
      />
      <RoutePrivate
        exact
        name="QuinielaPlus"
        path="/QuinielaPlus"
        component={QuinielaPlus}
      />
      <RoutePrivate exact name="Billete" path="/Billete" component={Billete} />
      <RoutePrivate exact name="Brinco" path="/Brinco" component={Brinco} />
      <RoutePrivate exact name="Loto" path="/Loto" component={Loto} />
      <RoutePrivate exact name="Loto5" path="/Loto5" component={Loto5} />
      <RoutePrivate exact name="Quini6" path="/Quini6" component={Quini6} />
      <RoutePrivate
        exact
        name="MisJugadas"
        path="/MisJugadas"
        component={MisJugadas}
      />
      <RoutePrivate
        exact
        name="MisResultados"
        path="/MisResultados"
        component={MisResultados}
      />
      <RoutePrivate
        exact
        name="Estadisticas"
        path="/Estadisticas"
        component={Estadisticas}
      />
      <RoutePrivate
        exact
        name="Pozos"
        path="/Pozos"
        component={Pozos}
      />
      <RoutePrivate exact name="Carrito" path="/Carrito" component={Carrito} />
      <RoutePrivate
        exact
        name="ModalTeminosCondiciones"
        path="/ModalTeminosCondiciones"
        component={ModalTeminosCondiciones}
      />
      <RoutePrivate
        exact
        name="CodigoCliente"
        path="/CodigoCliente"
        component={CodigoCliente}
      />
      <RoutePrivate exact name="Perfil" path="/Perfil" component={Perfil} />
      <RoutePrivate
        exact
        name="PagoTarjeta"
        path="/PagoTarjeta"
        component={PagoTarjeta}
      />
    </Switch>
  );
}
