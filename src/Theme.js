import { createMuiTheme } from "@material-ui/core";

export const theme = createMuiTheme({
  colors: {
    white: "#ffffff",
    black: "#000000",
    gray: "#808080",
    lightGray: "#eeeeee",
    red: "#FF0000",
    darkRed: "#b20000",
    blue: "#0000FF",
    lightBlue: "#00b2b2",
    skyBlue: "#87CEEB",
    green: "#008000",
    darkGreen: "#2b9b2b",
    mediumGreen: "#51a351",
    lightGreen: "#98ce98",
    yellow: "#f7d400",
    darkYellow: "#79796a",
    orange: "#ff7f00",
    darkOrange: "#e97400",
  },
  fonts: {
    primary: "Roboto, sans-serif",
  },
});

export default theme;
