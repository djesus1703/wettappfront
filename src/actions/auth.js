export const AUTH_SUCCESS = "AUTH";
export const START_SESSION = "START_SESSION";
export const CLOSE_SESSION = "CLOSE_SESSION";

export const autenticarUsuario = (payload) => ({
  type: AUTH_SUCCESS,
  payload,
});
export const iniciarSesion = (payload) => ({
  type: START_SESSION,
  payload,
});
export const cerrarSesion = () => ({
  type: CLOSE_SESSION,
});
