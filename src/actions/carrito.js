export const AGREGAR_JUGADA_QUINELA = "AGREGAR_JUGADA_QUINELA";
export const JUGADA_EXITOSA = "JUGADA_EXITOSA";
export const JUGADA_ERRADA = "JUGADA_ERRADA";
export const ELIMINAR_JUGADA = "ELIMINAR_JUGADA";

export const agregarJugada = (payload) => ({
  type: AGREGAR_JUGADA_QUINELA,
  payload,
});

export const eliminarJugada = (payload) => ({
  type: ELIMINAR_JUGADA,
  payload,
});
