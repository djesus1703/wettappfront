export const CHECK_QUINELA = "CHECK_QUINELA";
export const CLEAR_QUINELA = "CLEAR_QUINELA";

export const checkQuinela = (payload) => ({
  type: CHECK_QUINELA,
  payload,
});

export const clearQuineela = () => ({
  type: CLEAR_QUINELA,
});

