
export const dataQuinelaToObj = (item) => {
    let juego = [];
    juego[0] = {dato:[{},{},{},{}]}
    juego[1] = {dato:[{},{},{},{}]}
    juego[2] = {dato:[{},{},{},{}]}
    juego[3] = {dato:[{},{},{},{}]}
    juego[4] = {dato:[{},{},{},{}]}
    juego[5] = {dato:[{},{},{},{}]}

    juego[0].dato[0].selecte = item.provincia_primero === 1;
    juego[0].dato[1].selecte = item.provincia_matutina  === 1;
    juego[0].dato[2].selecte = item.provincia_vespertina === 1; 
    juego[0].dato[3].selecte = item.provincia_nocturno === 1; 
    juego[1].dato[0].selecte = item.nacional_primero === 1; 
    juego[1].dato[1].selecte = item.nacional_matutina === 1; 
    juego[1].dato[2].selecte = item.nacional_vespertina === 1; 
    juego[1].dato[3].selecte = item.nacional_nocturno === 1; 
    juego[2].dato[0].selecte = item.montevideo_primero === 1; 
    juego[2].dato[1].selecte = item.montevideo_matutina === 1; 
    juego[2].dato[2].selecte = item.montevideo_vespertina === 1; 
    juego[2].dato[3].selecte = item.montevideo_nocturno === 1; 
    juego[3].dato[0].selecte = item.santafe_primero === 1; 
    juego[3].dato[1].selecte = item.santafe_matutina === 1; 
    juego[3].dato[2].selecte = item.santafe_vespertina === 1; 
    juego[3].dato[3].selecte = item.santafe_nocturno === 1; 
    juego[4].dato[0].selecte = item.cordoba_primero === 1;  
    juego[4].dato[1].selecte = item.cordoba_matutina === 1;   
    juego[4].dato[2].selecte = item.cordoba_vespertina === 1; 
    juego[4].dato[3].selecte = item.cordoba_nocturno === 1;   
    juego[5].dato[0].selecte = item.entrerios_primero === 1;  
    juego[5].dato[1].selecte = item.entrerios_matutina === 1; 
    juego[5].dato[2].selecte = item.entrerios_vespertina === 1; 
    juego[5].dato[3].selecte = item.entrerios_nocturno === 1; 

    return juego;
}

export const getDataFromCarrito = (item) => {
    switch (item.nombre) {
        case 'Quini6':
            return {
                number_one : item.jugadas.campo0,
                number_two : item.jugadas.campo1,
                number_three : item.jugadas.campo2,
                number_four : item.jugadas.campo3,
                number_five : item.jugadas.campo4,
                number_six : item.jugadas.campo5,
                tipo_jugada: item.tipoJugada,
                monto: item.total,
                status: true,
                url: "/quini6"
            };
        case 'Billete':
            return {
                "numero" : item.jugadas.campo0,
                "posicion" : item.posicion,
                "monto" : item.total,
                "entero" : item.entero,
                "status" : true,
                "url": "/billetes"
            };  
        case 'Brinco':
            return {
                "number_one" : item.jugadas.campo0,
                "number_two" : item.jugadas.campo1,
                "number_three" : item.jugadas.campo2,
                "number_four" : item.jugadas.campo3,
                "number_five" : item.jugadas.campo4,
                "number_six" : item.jugadas.campo5,
                "number_seven" : item.jugadas.campo6,
                "number_eight" : item.jugadas.campo7,
                "monto" : item.total,
                "status" : true,
                "url": "/brinco"
            };  
        case 'Quinela Plus':
            return {
                "number_one" : item.jugadas.campo0,
                "number_two" : item.jugadas.campo1,
                "number_three" : item.jugadas.campo2,
                "number_four" : item.jugadas.campo3,
                "number_five" : item.jugadas.campo4,
                "number_six" : item.jugadas.campo5,
                "number_seven" : item.jugadas.campo6,
                "number_eight" : item.jugadas.campo7,
                "monto" : item.total,
                "status" : true,
                "url": "/quinela_plus",
            };  
        case 'Loto Plus':
            return {
                "number_one" : item.jugadas.campo0,
                "number_two" : item.jugadas.campo1,
                "number_three" : item.jugadas.campo2,
                "number_four" : item.jugadas.campo3,
                "number_five" : item.jugadas.campo4,
                "number_six" : item.jugadas.campo5,
                "number_seven" : item.jugadas.campo6,
                "number_eight" : item.jugadas.campo7,
                /*jackpot_one: 0,
                jackpot_two: 0,*/
                "monto" : item.total,
                "status" : true,
                "url": "/loto_plus",
            };  
        case 'Loto':
            return {
                "number_one" : item.jugadas.campo0,
                "number_two" : item.jugadas.campo1,
                "number_three" : item.jugadas.campo2,
                "number_four" : item.jugadas.campo3,
                "number_five" : item.jugadas.campo4,
                "number_six" : item.jugadas.campo5,
                "number_seven" : item.jugadas.campo6,
                "number_eight" : item.jugadas.campo7,
                jackpot_one: item.jugadas.campo6,
                jackpot_two: item.jugadas.campo7,
                tipo_jugada: item.tipoJugada,
                "monto" : item.total,
                "status" : true,
                "url": "/loto",
            };  
        case 'Quinela':
            const detalles = item.jugadas.map((elem) => {
               return {
                   "numero": elem.numero,
                    "posicion" : elem.posicion,
                    "importe" : elem.importe,
                    "redoblona" : (elem.tipo === "redoblona" || elem.tipo === "redoblona2")
                };
            });
            return {
                "monto_total" : item.total,
                "provincia_primero" : item.juego[0].dato[0].selecte,
                "provincia_matutina" : item.juego[0].dato[1].selecte,
                "provincia_vespertina" : item.juego[0].dato[2].selecte,
                "provincia_nocturno" : item.juego[0].dato[3].selecte,
                "nacional_primero" : item.juego[1].dato[0].selecte,
                "nacional_matutina" : item.juego[1].dato[1].selecte,
                "nacional_vespertina" : item.juego[1].dato[2].selecte,
                "nacional_nocturno" : item.juego[1].dato[3].selecte,
                "montevideo_primero" : item.juego[2].dato[0].selecte,
                "montevideo_matutina" : item.juego[2].dato[1].selecte,
                "montevideo_vespertina" : item.juego[2].dato[2].selecte,
                "montevideo_nocturno" : item.juego[2].dato[3].selecte,
                "santafe_primero" : item.juego[3].dato[0].selecte,
                "santafe_matutina" : item.juego[3].dato[1].selecte,
                "santafe_vespertina" : item.juego[3].dato[2].selecte,
                "santafe_nocturno" : item.juego[3].dato[3].selecte,
                "cordoba_primero" : item.juego[4].dato[0].selecte,
                "cordoba_matutina" : item.juego[4].dato[1].selecte,
                "cordoba_vespertina" : item.juego[4].dato[2].selecte,
                "cordoba_nocturno" : item.juego[4].dato[3].selecte,
                "entrerios_primero" : item.juego[5].dato[0].selecte,
                "entrerios_matutina" : item.juego[5].dato[1].selecte,
                "entrerios_vespertina" : item.juego[5].dato[2].selecte,
                "entrerios_nocturno" : item.juego[5].dato[3].selecte,
                "detalle" : detalles,
                "url": "/quinela",
            };  
        default:
            break;
    }


}