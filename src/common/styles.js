import { makeStyles } from "@material-ui/styles";

export default makeStyles((theme) => ({
  textCenter: {
    textAlign: "center",
  },
  textBold: {
    fontWeight: "bold",
  },
  alertGrid: {
    width: "100%",
    height: "100%",
    textAlign: "center",
  },
  alertCard: {
    backgroundColor: theme.colors.white,
    width: "70%",
    borderRadius: 10,
    "& h1": {
      fontSize: "100%",
      fontFamily: theme.fonts.primary,
      textAlign: "center",
    },
    "& p": {
      fontFamily: theme.fonts.primary,
    },
  },
  card: {
    width: "90%",
    height: "90%",
    backgroundColor: theme.colors.white,
    borderRadius: 10,
    overflowY: "scroll",
  },
  alertErrorIcon: {
    fontSize: "400%",
    color: theme.colors.yellow,
  },
  buttonAlert: {
    marginBottom: 10,
    borderRadius: 10,
  },
  button: {
    width: "100%",
    height: "100%",
    borderRadius: 10,
  },
  buttonText: {
    margin: 0,
    fontFamily: theme.fonts.primary,
    fontSize: "100%",
    textAlign: "center",
    textTransform: "none",
    color: theme.colors.white,
    paddingTop: "5%",
    paddingBottom: "5%",
  },
  buttonColorPrimary: {
    backgroundColor: theme.colors.orange,
  },
  buttonColorSecondary: {
    backgroundColor: theme.colors.darkYellow,
  },
  buttonColorTertiary: {
    backgroundColor: theme.colors.lightBlue,
  },
  btnColorTertiaryWhitoutHover: {
    backgroundColor: theme.colors.lightBlue,
    "&:hover": {
      backgroundColor: theme.colors.lightBlue,
    }
  },
  gridGame: {
    marginBottom: "5%",
    padding: "0 10px",
  },
  inputGame: {
    width: "100%",
    height: "100%",
    textAlign: "center",
    backgroundColor: theme.colors.lightGray,
    borderRadius: 10,
    marginRight: "3%",
    paddingTop: "6%",
    paddingBottom: "6%",
    "& input": {
      width: "50%",
      textAlign: "center",
      backgroundColor: "transparent",
      border: "none",
      fontSize: "130%",
      fontWeight: "bold",
      padding: 8,
      outline: 0,
    },
  },
  logosSvg: {
    width: "95%",
    height: "95%",
  },
  inputSelect: {
    width: "100%",
    height: "100%",
    backgroundColor: theme.colors.lightGray,
    borderRadius: 10,
    textAlign: "center",
    padding: "6% 0",
  },
  buttonCloseModal: {
    justifyContent: "flex-end",
    display: "flex",
    "& span": {
      color: theme.colors.gray,
    },
  },
  titleModal: {
    fontSize: "100%",
    fontFamily: theme.fonts.primary,
    textAlign: "center",
  },
  inputText: {
    width: "100%",
    height: "100%",
    backgroundColor: theme.colors.lightGray,
    borderRadius: 10,
  },
  finalMargin: {
    marginBottom: "20%",
  },
  containerImageBanner: {
    width: "80%",
    height: "80%",
    border: "2px solid",
    borderColor: theme.colors.lightGray,
    textAlign: "center",
    marginTop: "3%",
    marginLeft: "6%",
    borderRadius: 10,
  },
  iconBanner: {
    color: theme.colors.orange,
    textAlign: "center",
  },
}));
