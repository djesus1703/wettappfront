import React from "react";
import {
  Card,
  Grid,
  Button,
  IconButton,
  Modal,
  makeStyles,
} from "@material-ui/core";
import AvTimerIcon from "@material-ui/icons/AvTimer";
import ErrorIcon from "@material-ui/icons/Error";
import { useHistory } from "react-router-dom";
import styles from "../common/styles";

export default function Alert({
  openAlert,
  setOpenAlert,
  title,
  message,
  closeGame,
  error,
}) {
  const classes = useStyles();
  const style = styles();
  const history = useHistory();

  const closeAlert = () => {
    setOpenAlert(false);
    closeGame && history.goBack();
  };

  return (
    <Grid container item xs={12}>
      <Modal
        open={openAlert}
        onClose={() => setOpenAlert()}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className={style.alertGrid}
        >
          <Card elevation={5} className={style.alertCard}>
            <div className={classes.containerButtons}>
              <IconButton onClick={closeAlert}>
                {error ? (
                  <ErrorIcon className={classes.icons} />
                ) : (
                  <AvTimerIcon className={classes.icons} />
                )}
              </IconButton>
              <h1>{title}</h1>
              <p>{message}</p>
            </div>

            <Button
              variant="contained"
              className={`${style.buttonAlert} ${style.buttonColorTertiary}`}
              size="small"
              onClick={closeAlert}
            >
              <p className={style.buttonText}>Confirmar</p>
            </Button>
          </Card>
        </Grid>
      </Modal>
    </Grid>
  );
}

const useStyles = makeStyles((theme) => ({
  containerButtons: {
    margin: "4%",
    textAlign: "center",
  },
  icons: {
    fontSize: "300%",
    color: theme.colors.red,
  },
}));
