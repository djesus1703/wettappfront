import React, { useState } from "react";
import { Card, Grid, Button, IconButton, Modal } from "@material-ui/core";
import { HelpOutline } from "@material-ui/icons";
import styles from "../common/styles";

export default function AlertAutomatica({setRandomNumber}) {
  const style = styles();
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const generateRandom = () => {
    setOpen(false);
    setRandomNumber();
  }
  return (
    <Grid container item xs={12}>
      <Button
        variant="contained"
        className={`${style.button} ${style.buttonColorSecondary}`}
        size="large"
        onClick={handleOpen}
      >
        <p className={style.buttonText}>Automática</p>
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className={style.alertGrid}
        >
          <Card elevation={5} className={style.alertCard}>
            <div style={{ margin: "4%", textAlign: "center" }}>
              <IconButton onClick={handleClose}>
                <HelpOutline style={{ fontSize: "400%", color: "blue" }} />
              </IconButton>

              <h1>
                Se va a realizar una jugada automática. ¿Deseas continuar?
              </h1>
            </div>
            <Button
              variant="contained"
              className={`${style.buttonAlert} ${style.buttonColorTertiary}`}
              size="small"
              onClick={generateRandom}
            >
              <p className={style.buttonText}>Confirmar</p>
            </Button>
          </Card>
        </Grid>
      </Modal>
    </Grid>
  );
}
