import React, { useState } from "react";
import { Card, Grid, Button, IconButton, Modal } from "@material-ui/core";
import { useDispatch } from "react-redux";
import HighlightOff from "@material-ui/icons/HighlightOff";
import { ErrorOutline } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import { agregarJugada } from "../actions/carrito";
import styles from "../common/styles";
import logo_loto_plus from "../../src/assets/iconlotoplus.png";
import logo_loto from "../../src/assets/iconloto.png";
import logo_brinco from "../../src/assets/iconbrinco.png";
import logo_quinela from "../../src/assets/iconQP.png";
import logo_quinela_plus from "../../src/assets/iconQPlus.png";
import logo_quini6 from "../../src/assets/icon_quini6.png";

export default function AlertJugar(props) {
  const [open, setOpen] = useState(false);
  const [open2, setOpen2] = useState(false);
  const [msgError, setMsgError] = useState("¡Debes completar todos los campos!");
  const dispatch = useDispatch();
  const history = useHistory();
  const style = styles();
  const handleLogo = (juego) => {
    switch (juego) {
      case "Quinela":
        return logo_quinela;
      case "Quinela Plus":
        return logo_quinela_plus;
      case "Brinco":
        return logo_brinco;
      case "Loto":
        return logo_loto;
      case "Loto Plus":
          return logo_loto_plus;
      case "Quini6":
          return logo_quini6;
      default:
        break
          
    }
  }
  const handleOpen = () => {
    let cantidad = 0;
    let valueTemp = [];
    for (let index in props.campo) {
      valueTemp.push(parseInt(props.campo[index]));
      if (props.campo[index] === "") {
        cantidad++;
      }
    }
    
    if (cantidad > 0) {
      setOpen(true);
    } else {
      let tempJack = [];
      if(props.jackpot){
        tempJack = valueTemp.splice(valueTemp.length-2);
      }
      let temp = new Set(valueTemp);

      if(temp.size < valueTemp.length){
        setMsgError("Hay campos duplicados");
        setOpen(true);
        return;
      }
      
      if(props.jackpot && tempJack[0] === tempJack[1]){
        setMsgError("Hay campos duplicados");
        setOpen(true);
        return;
      }

        dispatch(
          agregarJugada({
            id: props.id,
            nombre: props.nombre,
            jugadas: props.campo,
            total: props.valorJugada,
            posicion: props.posicion,
            entero: props.entero,
            tipo: 2,
            tipoJugada: props.tipoJugada,
            Logo: handleLogo(props.nombre),
          })
        );
        setOpen2(true);
      
      
    }
  };
  console.log(props.Logo);
  const handleClose = () => {
    setOpen(false);
  };
  const handleClose2 = () => {
    setOpen2(false);
    history.push("/Carrito");
  };

  const renderModal = (mensaje, abrirModal, cerrarModal) => (
    <Modal
      open={abrirModal}
      onClose={cerrarModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        className={style.alertGrid}
      >
        <Card elevation={5} className={style.alertCard}>
          <div style={{ justifyContent: "flex-end", display: "flex" }}>
            <IconButton aria-label="delete" size="small" onClick={cerrarModal}>
              <HighlightOff fontSize="small" style={{ color: "grey" }} />
            </IconButton>
          </div>

          <div style={{ margin: "4%", textAlign: "center" }}>
            <IconButton onClick={cerrarModal}>
              <ErrorOutline className={style.alertErrorIcon} />
            </IconButton>

            <h1>{mensaje}</h1>
          </div>
        </Card>
      </Grid>
    </Modal>
  );

  return (
    <Grid container item xs={12}>
      <Button
        variant="contained"
        className={`${style.button} ${style.buttonColorPrimary}`}
        size="large"
        onClick={handleOpen}
      >
        <p className={style.buttonText}>Jugar!</p>
      </Button>
      {renderModal("¡Jugada realizada con éxito!", open2, handleClose2)}
      {renderModal(msgError, open, handleClose)}
    </Grid>
  );
}
