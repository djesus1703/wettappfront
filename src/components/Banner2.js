import React, { useState } from "react";
import { makeStyles, Grid, IconButton } from "@material-ui/core";
import InfoTwoTone from "@material-ui/icons/InfoTwoTone";
import ModalPolitcs from "./ModalPolitcs";
import styles from "../common/styles";

export default function Banner2(props) {
  const classes = useStyles();
  const style = styles();
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <Grid container item xs={12}>
      <Grid item xs={4}>
        <div className={style.containerImageBanner}>{props.imagen}</div>
      </Grid>
      <Grid item xs={8} style={{ marginTop: "-3%" }}>
        <div style={{ width: "100%", height: "100%", textAlign: "flex-start" }}>
          <IconButton
            aria-label="delete"
            size="small"
            onClick={handleOpen}
            style={{ marginLeft: "85%", padding: 0 }}
          >
            <InfoTwoTone fontSize="small" className={style.iconBanner} />
          </IconButton>
          <div
            style={{
              fontFamily: "Roboto",
              fontSize: "160%",
              textAlign: "flex-start",
            }}
          >
            {props.tittle}
          </div>
          <div className={classes.text}>{props.name}</div>
          <div className={classes.text}>{props.yuyu1}</div>
          <div className={classes.text}>{props.yuyu2}</div>
          <div className={classes.text}>{props.yuyu3}</div>
        </div>
      </Grid>
      <ModalPolitcs open={open} close={handleClose} />
    </Grid>
  );
}
const useStyles = makeStyles((theme) => ({
  text: {
    fontFamily: theme.fonts.primary,
    fontSize: "70%",
    textAlign: "flex-start",
  },
}));
