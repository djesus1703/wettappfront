import React from "react";
import { Grid } from "@material-ui/core";

export default function Banner5(props) {

  return (
    <Grid container item xs={12} justify="center" alignItems="center">
      <Grid item xs={3}>
        <div style={{ textAlign: "center" }}>
          <img height="80%" width="80%" src={props.imagen} alt="logo" />
        </div>
      </Grid>
      <Grid item xs={9}>
        <div style={{ width: "100%", height: "100%", textAlign: "flex-start" }}>
          <div
            style={{
              fontFamily: "Roboto",
              fontSize: "160%",
              textAlign: "flex-start",
            }}
          >
            {props.tittle}
          </div>
        </div>
      </Grid>
    </Grid>
  );
}
