import React from "react";
import {
  Card,
  makeStyles,
  Grid,
  CardContent,
  CardMedia,
} from "@material-ui/core";

export default function BannerLogin(props) {
  const classes = useStyles();

  return (
    <Card elevation={0} className={classes.root}>
      <CardMedia className={classes.cover} image={props.imagen} />
      <Grid className={classes.details}>
        <CardContent className={classes.content}>
          <div
            variant="h5"
            className={`${classes.text} ${classes.fontSizeTitle}`}
          >
            {props.tittle}
          </div>
          <div
            variant="subtitle1"
            className={`${classes.text} ${classes.fontSizeName}`}
          >
            {props.name}
          </div>
        </CardContent>
      </Grid>
    </Card>
  );
}
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: "transparent",
  },
  details: {
    display: "flex",
    flexDirection: "column",
    width: "70%",
  },
  content: {
    flex: "1 0 auto",
  },
  cover: {
    width: 90,
    height: 80,
    alignContent: "center",
    justifyContent: "center",
    borderRadius: 7,
    borderColor: theme.colors.lightGray,
    border: "2px solid",
  },
  fontSizeTitle: {
    fontSize: 35,
  },
  fontSizeName: {
    fontSize: 20,
  },
  text: {
    fontFamily: "Roboto",
    color: theme.colors.gray,
  },
}));
