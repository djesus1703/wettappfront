import React from "react";
import {
  Card,
  IconButton,
  Grid,
  makeStyles,
  CardContent,
  CardMedia,
} from "@material-ui/core";
import InfoTwoTone from "@material-ui/icons/InfoTwoTone";
import ModalPolitcs from "./ModalPolitcs";
import styles from "../common/styles";

export default function BannerLotery(props) {
  const classes = useStyles();
  const style = styles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <Card elevation={0} className={classes.root}>
      <CardMedia className={classes.cover} image={props.imagen} />
      <Grid className={classes.details}>
        <div style={{ justifyContent: "flex-end", display: "flex" }}>
          <IconButton aria-label="delete" size="small" onClick={handleOpen}>
            <InfoTwoTone fontSize="small" className={style.iconBanner} />
          </IconButton>
        </div>
        <CardContent className={classes.content}>
          <div component="h5" variant="h5" style={{ fontFamily: "Roboto" }}>
            {props.tittle}
          </div>
          <div variant="subtitle1" color="textSecondary">
            {props.name}
          </div>
          <div
            variant="body2"
            color="textSecondary"
            component="p"
            className={classes.letra}
          >
            {props.yuyu1}
          </div>
          <div
            variant="body2"
            color="textSecondary"
            component="p"
            className={classes.letra}
          >
            {props.yuyu2}
          </div>
          <div
            variant="body2"
            color="textSecondary"
            component="p"
            className={classes.letra}
          >
            {props.yuyu3}
          </div>
        </CardContent>
      </Grid>
      <ModalPolitcs open={open} close={handleClose} />
    </Card>
  );
}
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: "transparent",
  },
  details: {
    display: "flex",
    flexDirection: "column",
    width: "70%",
  },
  content: {
    flex: "1 0 auto",
    marginTop: "-15%",
  },
  cover: {
    width: 90,
    height: 80,
    alignContent: "center",
    justifyContent: "center",
    borderRadius: 7,
    borderColor: theme.colors.lightGray,
    border: "2px solid",
  },
  cover1: {
    width: 90,
    height: 80,
    alignContent: "center",
    justifyContent: "center",
    borderRadius: 7,
    borderColor: theme.colors.lightGray,
    border: "2px solid",
    marginLeft: "5%",
  },
  playIcon: {
    height: 38,
    width: 38,
  },
  letra: {
    fontFamily: theme.fonts.primary,
    fontSize: "70%",
  },
}));
