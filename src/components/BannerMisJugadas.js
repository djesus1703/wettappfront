import React from "react";
import { makeStyles, Card, Button, Grid } from "@material-ui/core";

export default function BannerMisJugadas(props) {
  const classes = useStyles();

  return (
    <Card elevation={5} className={classes.root2}>
      <Grid container item xs={12}>
        <Grid item xs={3} style={{ alignSelf: "center", marginRight: "2%" }}>
          <img
            src={props.imagen}
            style={{ width: props.tittle === "Quinela" ? "85%" : "100%", margin: "5%" }}
            alt="logo"
          />
        </Grid>
        <Grid item xs={6} style={{paddingLeft:"1%"}}>
          <div>
            <div variant="subtitle1" className={classes.textTitle}>
              {props.tittle}
            </div>
            <div className={classes.text}>{props.numero}<span className={classes.jackpot}>{' '+props.jackpot}</span></div>
            <div style={{ fontFamily: "Roboto", fontSize: "70%" }}>
              {props.texto2}
            </div>
          </div>
        </Grid>
        <Grid item xs={2}>
          <div>
            <Button onClick={()=>props.repetir()} variant="contained" className={classes.button}>
              Repetir
            </Button>
            <div style={{ fontFamily: "Roboto", marginTop: "53%",float: "right" }}><strong>{'$'+props.precio}</strong></div>
          </div>
        </Grid>
      </Grid>
    </Card>
  );
}

const useStyles = makeStyles((theme) => ({
  root2: {
    display: "flex",
    width: "90%",
    borderRadius: 7,
    margin: "2%",
    maxHeight: 75,
    paddingBottom:"2%",
  },
  textTitle: {
    fontFamily: theme.fonts.primary,
    fontSize: "100%",
    color: theme.colors.gray,
    fontWeight: "bold",
    marginTop: "3%",
  },
  text: {
    fontFamily: theme.fonts.primary,
    fontSize: "100%",
    color: theme.colors.darkOrange,
    fontWeight: "bold",
  },
  button: {
    fontFamily: "Roboto",
    fontSize: "80%",
    textTransform: "none",
    marginTop: "10%",
    backgroundColor: theme.colors.darkRed,
    color: theme.colors.white,
    padding: 2,
  },
  jackpot: {
    color: theme.colors.black,//theme.palette.type === 'dark' ? theme.colors.white : theme.colors.black,
  }
}));
