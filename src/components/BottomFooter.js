import React from "react";
import { Card } from "@material-ui/core";
import { Grid } from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import { makeStyles } from "@material-ui/core/styles";
import imagen1 from "../assets/iconX.png";

export default function BottomFooter(props) {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <Grid container item xs={2} className={classes.root1}>
        <CardActionArea>
          <CardMedia className={classes.media} image={imagen1} />
          <CardContent>
            <div gutterBottom variant="h5" component="h2">
              Lizard
            </div>
          </CardContent>
        </CardActionArea>
      </Grid>
      <Grid container item xs={2} className={classes.root1}>
        <CardActionArea>
          <CardMedia className={classes.media} image={imagen1} />
          <CardContent>
            <div gutterBottom variant="h5" component="h2">
              Lizard
            </div>
          </CardContent>
        </CardActionArea>
      </Grid>
      <Grid container item xs={2} className={classes.root1}>
        <CardActionArea>
          <CardMedia className={classes.media} image={imagen1} />
          <CardContent>
            <div gutterBottom variant="h5" component="h2">
              Lizard
            </div>
          </CardContent>
        </CardActionArea>
      </Grid>
      <Grid container item xs={2} className={classes.root1}>
        <CardActionArea>
          <CardMedia className={classes.media} image={imagen1} />
          <CardContent>
            <div gutterBottom variant="h5" component="h2">
              Lizard
            </div>
          </CardContent>
        </CardActionArea>
      </Grid>
      <Grid container item xs={2} className={classes.root1}>
        <CardActionArea>
          <CardMedia className={classes.media} image={imagen1} />
          <CardContent>
            <div gutterBottom variant="h5" component="h2">
              Lizard
            </div>
          </CardContent>
        </CardActionArea>
      </Grid>
    </Card>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  media: {
    width: "50%",
    margin: "4%",
  },
}));
