import React from "react";
import { Card, makeStyles, CardActionArea } from "@material-ui/core";
import ListAlt from "@material-ui/icons/ListAlt";

export default function BottomItems(props) {
  const classes = useStyles();

  return (
    <Card elevation={3} className={classes.card}>
      <CardActionArea>
        <div style={{ width: "100%", height: "100%", textAlign: "center" }}>
          <div className={classes.image}>
            {props.imagen}
          </div>
          <div className={classes.icon}>
            <ListAlt fontSize="large" />
          </div>
          <div className={classes.text}>{props.title}</div>
        </div>
      </CardActionArea>
    </Card>
  );
}

const useStyles = makeStyles((theme) => ({
  image: {
    height: '130px',
  },
  card: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
    borderRadius: 10,
    backgroundColor: "transparent",
  },
  icon: {
    textAlign: "center",
    color: theme.colors.gray,
  },
  text: {
    fontFamily: theme.fonts.primary,
    fontSize: "90%",
    textAlign: "center",
    marginBottom: "25%",
  },
}));
