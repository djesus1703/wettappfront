import React from "react";
import { Card, makeStyles, CardActionArea } from "@material-ui/core";

export default function BottomJugar(props) {
  const classes = useStyles();

  return (
    <Card elevation={3} className={classes.card}>
      <CardActionArea>
        <div style={{ width: "100%", height: "100%", textAlign: "center" }}>
          {props.imagen}
          <div className={classes.title}>{props.tittle8}</div>
        </div>
      </CardActionArea>
    </Card>
  );
}

const useStyles = makeStyles((theme) => ({
  card: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
    borderRadius: 10,
    backgroundColor: "transparent",
  },
  title: {
    fontFamily: theme.fonts.primary,
    fontSize: "90%",
    textAlign: "center",
    marginBottom: "25%",
  },
}));
