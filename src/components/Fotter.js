import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import { Grid } from "@material-ui/core";
import { ReactComponent as Logo } from "../assets/inicio.svg";
import { ReactComponent as Logo1 } from "../assets/monedero.svg";
import { ReactComponent as Logo2 } from "../assets/jugadas.svg";
import { ReactComponent as Logo3 } from "../assets/resultados.svg";
import { ReactComponent as Logo4 } from "../assets/estadisticas.svg";
import { useHistory } from "react-router-dom";

export default function Fotter() {
  const history = useHistory();
  const classes = useStyles();
  function routePush(name) {
    history.push(name);
  }
  const routes = [
    { 
      name: "Inicio", 
      url: "/", 
      logo: <Logo className={classes.svg} /> 
    },
    {
      name: "Monedero",
      url: "/Jugar",
      logo: <Logo1 className={classes.svg} />,
    },
    {
      name: "Mis jugadas",
      url: "/MisJugadas",
      logo: <Logo2 className={classes.svg} />,
    },
    {
      name: "Resultados",
      url: "/MisResultados",
      logo: <Logo3 className={classes.svg} />,
    },
    {
      name: "Estadísticas",
      url: "/Estadisticas",
      logo: <Logo4 className={classes.svg} />,
    },
    {
      name: "Pozos",
      url: "/Pozos",
      logo: <Logo className={classes.svg} />,
    },
  ];
  return (
    <Card
      elevation={0}
      style={{
        justifyContent: "center",
        position: "fixed",
        bottom: 0,
        zIndex: 999,
        width: "100%",
      }}
    >
      <Grid
        container
        item
        xs={12}
        style={{
          justifyContent: "space-evenly",
        }}
      >
        {routes.map((route,index) => (
          <Grid key={index} item xs={2}>
            <div
              className={classes.containerButton}
              onClick={() => {
                routePush(route.url);
              }}
            >
              {route.logo}
              <div className={classes.textName}>{route.name}</div>
            </div>
          </Grid>
        ))}
      </Grid>
    </Card>
  );
}

const useStyles = makeStyles((theme) => ({
  containerButton: {
    width: "100%",
    height: "100%",
    cursor: "pointer",
    textAlign: "center",
  },
  textName: {
    fontFamily: theme.fonts.primary,
    fontSize: "65%",
    textAlign: "center",
  },
  svg: {
    width: "60%",
    height: "60%",
  },
}));
