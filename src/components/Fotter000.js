import React from 'react';
import { ReactSVG } from 'react-svg';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import imagen1 from '../assets/inicio.png';
import imagen2 from '../assets/monedero.png';
import imagen3 from '../assets/jugadas.png';
import imagen4 from '../assets/resultados.png';
import imagen5 from '../assets/estadisticas.png';

import { useHistory } from "react-router-dom";


export default function Fotter() {
    const history = useHistory();
    const classes = useStyles();
    const [linea, setLinea] = React.useState();
    const [linea1, setLinea1] = React.useState();
    const [linea2, setLinea2] = React.useState();
    const [linea3, setLinea3] = React.useState();
    const [linea4, setLinea4] = React.useState();
    const [value, setValue] = React.useState(0);



    const pey = (data) => {
        let print = ''
        console.log('eeer', print);

    }


    function routePush(name) {
        history.push(name)
    }
    return (

        <BottomNavigation
            // value={value}
            onChange={(event, newValue) => {
                setValue(newValue);
            }}
            showLabels
            className={classes.root}
        >
            <BottomNavigationAction label="Inicio" classes={{ label: classes.mg, wrapper: classes.cont, root: classes.cont1 }} icon={<img src={imagen1} style={{ width: '40%', }} />}
                onClick={() => { routePush("/") }}
            />
            <BottomNavigationAction label="Mi Monedero" classes={{ label: classes.mg, wrapper: classes.cont, root: classes.cont1 }} icon={<img src={imagen2} style={{ width: '57%', }} />}
                onClick={() => { routePush("/Jugar") }}
            />
            <BottomNavigationAction label="Mis Jugadas" classes={{ label: classes.mg, wrapper: classes.cont, root: classes.cont1 }} icon={<img src={imagen3} style={{ width: '40%', }} />}
                onClick={() => { routePush("/MisJugadas") }}
            />
            <BottomNavigationAction label="Resultados" classes={{ label: classes.mg, wrapper: classes.cont, root: classes.cont1 }} icon={<img src={imagen4} style={{ width: '35%', }} />} />
            <BottomNavigationAction label="Estadisticas" classes={{ label: classes.mg, wrapper: classes.cont, root: classes.cont2 }} icon={<img src={imagen5} style={{ width: '40%', }} />} />

        </BottomNavigation>

    );
}

const useStyles = makeStyles({
    root: {
        // width: 500,
        width: '100%',
        // height: '10%',
        // backgroundColor: 'green',
        // marginTop: '3%',
        position: 'fixed',
        bottom: 0,
        zIndex: 999,
        // paddingRight: '5%',
        // alignItems: 'stretch',
        // marginTop: '5%',

    },
    root1: {
        width: '100%',
        // height: '70%',


        alignContent: 'center',
        justifyContent: 'center',
        // marginBottom: '4%',
        // margin: '4%',
    },
    mg: {

        fontSize: '75%',


    },
    cont: {
        // backgroundColor: 'green',
        paddingLeft: 0,
        margin: 0,
    },
    cont1: {
        // backgroundColor: 'red',
        padding: 0,
        marginR: 0,
        // paddingRight: '10%',

    },
    cont2: {
        // backgroundColor: 'red',
        padding: 0,
        marginRight: 10,
        // paddingLeft: '5%',
        // paddingRight: '2%',
        // paddingTop: '1%',
        // paddingBottom: '1%',

    },
});