import React, { Fragment } from "react";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ArrowBackIos from "@material-ui/icons/ArrowBackIos";
import { useHistory } from "react-router-dom";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ExitToAppTwoTone from "@material-ui/icons/ExitToAppTwoTone";
import BubbleChartTwoTone from "@material-ui/icons/BubbleChartTwoTone";
import AccountCircleTwoTone from "@material-ui/icons/AccountCircleTwoTone";
import { useDispatch } from "react-redux";
import { cerrarSesion } from "../actions/auth";

export default function Header() {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const [state, setState] = React.useState(false);

  const toggleDrawer = (open) => (event) => {
    setState(open);
  };

  const item = [
    { titulo: "Jugar", ruta: "/Jugar" },
    { titulo: "Mi Carrito", ruta: "/Carrito" },
  ];

  const sup = [
    { titulo: "Perfil", ruta: "/Perfil" },
    { titulo: "Cerrar Sesion", ruta: "/Login" },
  ];

  const list = () => (
    <div
      role="presentation"
      onClick={toggleDrawer(false)}
      onKeyDown={toggleDrawer(false)}
    >
      <List>
        {item.map((text, index) => (
          <ListItem
            button
            key={index}
            onClick={() => {
              history.push(text.ruta);
            }}
          >
            <ListItemIcon>
              <BubbleChartTwoTone
                fontSize="large"
                className={classes.colorIcons}
              />
            </ListItemIcon>
            <ListItemText primary={text.titulo} />
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        {sup.map((text, index) => (
          <ListItem
            button
            key={index}
            onClick={() => {
              history.push(text.ruta);
              text.titulo === "Cerrar Sesion" && dispatch(cerrarSesion());
            }}
          >
            <ListItemIcon>
              {index % 2 === 0 ? (
                <AccountCircleTwoTone
                  fontSize="large"
                  className={classes.colorIcons}
                />
              ) : (
                <ExitToAppTwoTone
                  fontSize="large"
                  className={classes.colorIcons}
                />
              )}
            </ListItemIcon>
            <ListItemText primary={text.titulo} />
          </ListItem>
        ))}
      </List>
      <Button
        style={{
          position: "absolute",
          bottom: 0,
          fontSize: "90%",
          fontFamily: "Roboto",
          textTransform: "none",
        }}
        onClick={() => {
          history.push("/ModalTeminosCondiciones");
        }}
      >
        Terminos y Condiciones
      </Button>
    </div>
  );

  return (
    <Fragment>
      <AppBar position="relative" color="transparent" elevation={0}>
        <Toolbar>
          <Button
            color="inherit"
            onClick={() => {
              history.goBack();
            }}
            className={classes.back}
          >
            <ArrowBackIos className={classes.menuButton} />
          </Button>
          {/* <div className={classes.title}>Resultados</div> */}
          <div className={classes.title}>¡Jugar!</div>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
            onClick={toggleDrawer(true)}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Grid container className={classes.root}></Grid>
      <SwipeableDrawer
        anchor={"right"}
        open={state}
        onClose={toggleDrawer(false)}
        onOpen={toggleDrawer(true)}
      >
        {list()}
      </SwipeableDrawer>
    </Fragment>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    background:
      "linear-gradient(180deg, #e97400 33%, #cf6700 66%, #b75b00 99%)",
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "30%",
    borderBottomLeftRadius: "40%",
    borderBottomRightRadius: "40%",
    justify: "center",
    direction: "row",
    zIndex: 0,
  },
  menuButton: {
    color: theme.colors.white,
  },
  title: {
    fontFamily: theme.fonts.primary,
    fontWeight: "bold",
    fontSize: "160%",
    flexGrow: 1,
    display: "flex",
    justifyContent: "center",
    color: theme.colors.white,
    marginRight: "10%",
  },
  colorIcons: {
    color: theme.colors.orange,
  },
  back: {
    justifyContent: "flex-start",
  },
}));
