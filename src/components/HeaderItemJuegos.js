import { Grid, makeStyles } from "@material-ui/core";
import React from "react";
import Banner1 from "./Banner1";

export default function HeaderItemJuegos(props) {
    const classes = useStyles();

    return (
        <Grid item xs={12} className={classes.root1}>
            <Banner1
                imagen={props.image}
                tittle={props.title}
            />
        </Grid>
    )
}

const useStyles = makeStyles((theme) => ({
    root1: {
        margin: "3%",
    },
}))