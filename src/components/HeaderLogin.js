import React, { Fragment } from "react";
import {
  Grid,
  IconButton,
  Button,
  Toolbar,
  AppBar,
  makeStyles,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import ArrowBackIos from "@material-ui/icons/ArrowBackIos";
import { useHistory } from "react-router-dom";

export default function HeaderLogin() {
  const classes = useStyles();
  const history = useHistory();
  return (
    <Fragment>
      <AppBar position="relative" color="transparent" elevation={0}>
        <Toolbar>
          <Button
            color="inherit"
            onClick={() => {
              history.goBack();
            }}
            className={classes.back}
          >
            <ArrowBackIos className={classes.menuButton} />
          </Button>
          <div variant="h6" className={classes.title}>
            {" "}
          </div>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Grid container className={classes.root}></Grid>
    </Fragment>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    background:
      "linear-gradient(180deg, #e97400 33%, #cf6700 66%, #b75b00 99%)",
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "30%",
    borderBottomLeftRadius: "40%",
    borderBottomRightRadius: "40%",
    justify: "center",
    direction: "row",
    zIndex: 0,
  },
  menuButton: {
    color: theme.colors.white,
  },
  title: {
    fontFamily: theme.fonts.primary,
    fontWeight: "bold",
    fontSize: "160%",
    flexGrow: 1,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: theme.colors.white,
  },

  back: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
}));
