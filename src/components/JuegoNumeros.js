import React, { useState } from "react";
import {
  Grid,
  makeStyles,
  Card,
  IconButton,
  Modal,
  Button,
  Select,
  MenuItem,
  withStyles,
  InputBase,
} from "@material-ui/core";
import HighlightOff from "@material-ui/icons/HighlightOff";
import { useHistory } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import Banner3 from "../components/Banner3";
import AlertAutomatica from "../components/AlertAutomatica";
import { CheckBox, CheckBoxOutlineBlank, ErrorOutline } from "@material-ui/icons";
import AlertJugar from "../components/AlertJugar";
import styles from "../common/styles";

export default function Brinco({
  Logo,
  valorJugada,
  campos,
  rango,
  setCampos,
  cantidad,
  nombre,
  juega,
  modificarValor,
  setValorJugada,
  precioTradicional,
  preciosDesquite,
  precioSale,
  jackpot,
  camposJackpot,
  setCamposJackpot,
  rangoJackpot,
  select,
}) {
  
  const history = useHistory();
  const classes = useStyles();
  const style = styles();
  const [open, setOpen] = useState(false);
  const [age, setAge] = useState("");
  const [tipoJugada, setTipoJugada] = useState("");
  const [entero, setEntero] = useState(false);

  const impar = cantidad % 2 > 0;

  const modificarValorJugada = [
    { nombre: "Tradicional", valor: precioTradicional },
    { nombre: "Desquite", valor: preciosDesquite },
    { nombre: "Sale o Sale", valor: precioSale },
  ];

  const getRandomNumber = (min, max) => {
    return Math.floor(Math.random()*(max-min+1)+min);
  }

  const handleValorJugada = (valor,tJugada) => {
    setValorJugada(valor);
    setTipoJugada(tJugada);
  }

  const setRandomNumber = () => {
    var min = rango.inicio;
    var max = rango.fin;
    let tempCampos = [];
    Object.keys(campos).map((key) => {
      var x = getRandomNumber(min, max);
      while (tempCampos.find(val => val === x) !== undefined) {
        x = getRandomNumber(min, max);
      };
      campos[key] = x.toString().length === 1 ? `0${x}` : x;
      tempCampos.push(x);
    });
    if(jackpot){
      min = rangoJackpot.inicio;
      max = rangoJackpot.fin;
      let tempJack = [];
      Object.keys(camposJackpot).map((key) => {
        var x = getRandomNumber(min, max);
        while (tempJack.find(val => val === x) !== undefined) {
          x = getRandomNumber(min, max);
        };
        camposJackpot[key] = x;
        tempJack.push(x);
      });
      console.log(tempJack);
      console.log(tempCampos);
      setCamposJackpot({...camposJackpot});
    }
    setCampos({...campos});
    
  };

  //console.log(Logo);
  return (
    <Grid container direction="row" justify="center">
      <Card elevation={0} className={classes.root}>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{ margin: "2%" }}
        >
          <Grid item xs={12} style={{ marginBottom: "10%" }}>
            <div
              style={{
                width: "100%",
                height: "100%",
                marginTop: "1%",
                marginBottom: "5%",
              }}
            >
              <Banner3
                imagen={<Logo className={classes.svg}/>}
                yuyu1={juega}
                yuyu2={`Precio: $${valorJugada}`}
                yuyu3={
                  jackpot
                    ? `Numeros de ${rango.inicio} al ${rango.fin}, Jack de ${rangoJackpot.inicio} al ${rangoJackpot.fin}`
                    : `Numeros de ${rango.inicio} al ${rango.fin}`
                }
              />
            </div>
          </Grid>

          {Object.keys(campos).map((key, index) => (
            <Grid item xs={cantidad === 1 ? 12 : (index === cantidad-1 && impar) ? 12 : 6} className={style.gridGame}>
              <div className={style.inputGame}>
                <input
                  type="number"
                  disableUnderline={true}
                  value={campos[key]}
                  onChange={(e) => {
                    let valor = e.target.value;
                    if (valor >= rango.inicio && valor <= rango.fin) {
                      setCampos({ ...campos, [key]: valor });
                    } else {
                      e.target.value = "";
                      setOpen(true);
                    }
                  }}
                />
              </div>
            </Grid>
          ))}
          {select && (
            <>
              <Grid item xs={5} className={style.finalMargin}>
                <Button
                  variant="contained"
                  className={`${style.button} ${style.btnColorTertiaryWhitoutHover}`}
                  size="large"
                  onClick={
                    ()=>setEntero(!entero)
                  }
                >
                  
                  <div style={{display: 'flex'}} className={style.buttonText}>
                    { entero ? <CheckBox /> : <CheckBoxOutlineBlank /> } 
                    Entero
                  </div>
                </Button>
              </Grid>
              <Grid item xs={1} className={style.finalMargin} />
              <Grid item xs={6} className={style.finalMargin}>
                <Select
                  labelId="demo-customized-select-label"
                  id="demo-customized-select"
                  value={age}
                  onChange={(e) => setAge(e.target.value)}
                  input={<BootstrapInput />}
                  className={style.inputSelect}
                >
                  <MenuItem value={10}>1</MenuItem>
                  <MenuItem value={20}>2</MenuItem>
                  <MenuItem value={30}>3</MenuItem>
                </Select>
              </Grid>
            </>
          )}
          {jackpot && (
            <>
              <Grid item xs={12}>
                <div
                  style={{
                    width: "100%",
                    height: "100%",
                    marginBottom: "5%",
                    fontFamily: "Roboto",
                    fontSize: "110%",
                  }}
                >
                  Jackpot
                </div>
              </Grid>
              {Object.keys(camposJackpot).map((key) => (
                <Grid item xs={6} className={style.gridGame}>
                  <div className={style.inputGame}>
                    <input
                      type="number"
                      disableUnderline={true}
                      value={camposJackpot[key]}
                      onChange={(e) => {
                        //console.log("eee", e.target.value);
                        let valor = e.target.value;
                        
                        if (
                          valor >= rangoJackpot.inicio &&
                          valor <= rangoJackpot.fin
                        ) {
                          setCamposJackpot({ ...camposJackpot, [key]: valor });
                        } else {
                          e.target.value = "";
                          setOpen(true);
                        }
                      }}
                    />
                  </div>
                </Grid>
              ))}
            </>
          )}
          {modificarValor && (
            <>
              {modificarValorJugada.map((item) => (
                <Grid item xs={4} style={{ marginBottom: "15%" }}>
                  <div
                    style={{
                      backgroundColor: "transparent",
                      borderRadius: 10,
                      textAlign: "center",
                    }}
                  >
                    <Button
                      variant="contained"
                      className={`${style.buttonAlert} ${style.buttonColorTertiary}`}
                      size="small"
                      onClick={() => handleValorJugada(item.valor, item.nombre)}
                    >
                      <p className={style.buttonText}>
                        {item.nombre} <br /> ${item.valor}
                      </p>
                    </Button>
                  </div>
                </Grid>
              ))}
            </>
          )}
          {!select && (
            <>
              <Grid item xs={5} style={{ marginBottom: "5%" }}>
                <AlertAutomatica setRandomNumber={setRandomNumber} />
              </Grid>
              <Grid item xs={2} />
            </>
          )}
          <Grid item xs={!select ? 5 : 12} style={{ marginBottom: "5%" }}>
            <AlertJugar
              id={uuidv4()}
              campo={!jackpot ? campos : { ...campos, ...camposJackpot }}
              cantidad={cantidad}
              jackpot={jackpot}
              tipoJugada={tipoJugada}
              posicion={age}
              valorJugada={valorJugada}
              nombre={nombre}
              entero={entero}
              Logo={Logo}
            />
          </Grid>
          <Grid item xs={12} className={style.finalMargin}>
            <Button
              onClick={() => {
                history.goBack();
              }}
              variant="contained"
              className={`${style.button} ${style.buttonColorPrimary}`}
              size="large"
            >
              <p className={style.buttonText}>Volver al menú anterior</p>
            </Button>
          </Grid>
        </Grid>
      </Card>
      <Modal
        open={open}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className={style.alertGrid}
        >
          <Card elevation={5} className={style.alertCard}>
            <div className={style.buttonCloseModal}>
              <IconButton
                aria-label="delete"
                size="small"
                onClick={() => {
                  setOpen(false);
                }}
              >
                <HighlightOff fontSize="small" />
              </IconButton>
            </div>
            <div style={{ margin: "4%", textAlign: "center" }}>
              <IconButton
                onClick={() => {
                  setOpen(false);
                }}
              >
                <ErrorOutline className={style.alertErrorIcon} />
              </IconButton>
              <h1>
                {`¡Debes ingresar un número del ${rango.inicio} al ${rango.fin}!`}
              </h1>
            </div>
          </Card>
        </Grid>
      </Modal>
    </Grid>
  );
}
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    borderRadius: 10,
  },
  textJackpot: {
    width: "100%",
    height: "100%",
    marginBottom: "5%",
    fontFamily: theme.fonts.primary,
    fontSize: "110%",
  },
  svg: {
    width: "95%",
    height: "95%",
   // marginTop: "5%",
  }
}));

const BootstrapInput = withStyles((theme) => ({
  root: {
    "label + &": {
      marginTop: 0,
    },
  },
  input: {
    borderRadius: 7,
    backgroundColor: "transparent",
    border: "0px solid transparent",
    fontSize: "100%",
    fontFamily: theme.fonts.primary,
    "&:focus": {
      backgroundColor: "transparent",
      borderRadius: 4,
      borderColor: "transparent",
    },
  },
}))(InputBase);
