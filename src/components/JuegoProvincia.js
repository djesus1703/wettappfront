import React, { useState } from "react";
import { makeStyles, Button, Grid } from "@material-ui/core";
import CheckCircleTwoTone from "@material-ui/icons/CheckCircleTwoTone";

export default function Quiniela(props) {
  const classes = useStyles();
  const [select, setSelect] = useState(false);
  return (
    <Grid container item xs={12} className={classes.root}>
      <Grid
        Grid
        item
        xs={3}
        style={{ marginRight: "2%", marginBottom: "5%" }}
        onClick={() => {
          setSelect(!select);
          props.click33(props.juego, select);
        }}
      >
        <div
          className={`${classes.containerButton} ${
            props.dato.select ? classes.buttonPress : classes.buttonColor
          }`}
        >
          <div className={classes.buttonTitle}>{props.dato.titulo}</div>
        </div>
      </Grid>
      {props.dato.dato.map((item, index) => (
        <Grid
          key={index}
          item
          xs={2}
          style={{ marginLeft: "1%", marginBottom: "5%" }}
        >
          <div
            className={`${classes.containerButtonIcon} ${
              !item.view && classes.containerButtonIconHide
            }`}
          >
            <Button
              className={classes.button}
              onClick={() => {
                props.click(props.juego, index);
              }}
              variant="text"
            >
              <CheckCircleTwoTone
                fontSize="small"
                className={`${classes.containerIcon} ${
                  item.selecte ? classes.colorIconSelect : classes.colorIcon
                }`}
              />
            </Button>
          </div>
        </Grid>
      ))}
    </Grid>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
  },
  root1: {
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    alignContent: "center",
    justifyContent: "center",
  },

  details: {
    display: "flex",
    flexDirection: "column",
  },
  content: {
    flex: "1 0 auto",
  },
  cover: {
    width: 90,
    height: 80,
    alignContent: "center",
    justifyContent: "center",
    borderRadius: 7,
    borderColor: theme.colors.lightGray,
    border: "2px solid",
  },
  ocultar: {
    display: "none",
  },
  button: {
    minWidth: "initial",
    padding: "0",
    height: "100%",
  },
  containerButton: {
    width: "100%",
    height: "100%",
    textAlign: "center",
    border: "1px solid",
    borderColor: theme.colors.lightGray,
    borderRadius: 7,
  },
  buttonPress: {
    backgroundColor: theme.colors.orange,
  },
  buttonColor: {
    backgroundColor: theme.colors.lightGray,
  },
  buttonTitle: {
    fontFamily: theme.fonts.primary,
    fontSize: "90%",
    fontWeight: "bold",
    textAlign: "center",
    paddingTop: "12%",
    paddingBottom: "12%",
  },
  colorIconSelect: {
    color: theme.colors.green,
  },
  colorIcon: {
    color: theme.colors.lightGray,
  },
  containerIcon: {
    padding: "10% 0",
  },
  containerButtonIcon: {
    width: "100%",
    height: "100%",
    textAlign: "center",
    backgroundColor: theme.colors.lightGray,
    borderRadius: 7,
    paddingTop: "6%",
  },
  containerButtonIconHide: {
    display: "none",
  },
}));
