import React from "react";
import { Grid, makeStyles } from "@material-ui/core";
import BottomItems from "./BottomItems";

import { ReactComponent as Logo } from "../assets/prestador1.svg";
import { ReactComponent as Logo1 } from "../assets/prestador2.svg";
import { ReactComponent as Logo2 } from "../assets/prestador3.svg";
import { ReactComponent as Logo3 } from "../assets/prestador4.svg";
import { ReactComponent as Logo4 } from "../assets/prestador5.svg";
import { ReactComponent as Logo5 } from "../assets/prestador6.svg";

export default function ListItemJuegos(props) {
    const classes = useStyles();
    const juegos = [
        {
            nombre: "Quiniela Provincia",
            ruta: "Quiniela",
            logo: <Logo className={classes.svg} />,
        },
        {
            nombre: "Quinela",
            ruta: "Quiniela+plus",
            logo: <Logo1 className={classes.svg} />,
        },
        {
            nombre: "Loto",
            ruta: "Loto",
            logo: <Logo2 className={classes.svg} />
        },
        {
            nombre: "Loto Plus",
            ruta: "Loto+5",
            logo: <Logo3 className={classes.svg} />,
        },
        {
            nombre: "Brinco",
            ruta: "Brinco",
            logo: <Logo4 className={classes.svg} />,
        },
        {
            nombre: "Prode",
            ruta: "Quiniela+6",
            logo: <Logo5 className={classes.svg} />,
        },
    ];

    return (
        <Grid container item xs={12} className={classes.root2}>
            {juegos.map((juego) => (
                <Grid
                    key={juego.nombre}
                    item
                    xs={3}
                    className={classes.root1}
                    onClick={() => {
                        window.location.href = `https://glucksfall.com.ar/${props.ruta}.php?juego=${juego.ruta}`;
                    }}
                >
                    <BottomItems
                        imagen={juego.logo}
                        title={props.title}
                    />
                </Grid>
            ))}
        </Grid>
    )
}

const useStyles = makeStyles((theme) => ({
    root1: {
        margin: "3%",
    },
    root2: {
        width: "100%",
        zIndex: 1,
        backgroundColor: theme.colors.white,
        alignContent: "center",
        justifyContent: "center",
    },
    svg: {
        width: "70%",
        height: "70%",
        marginTop: "10%",
    },
}))