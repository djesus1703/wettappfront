import React from "react";
import {
  Button,
  Card,
  Grid,
  IconButton,
  makeStyles,
  Modal,
} from "@material-ui/core";
import { HighlightOff } from "@material-ui/icons";
import styles from "../common/styles";

const ModalPolitcs = ({open, close}) => {
  const style = styles();
  const classes = useStyle();
  return (
    <Modal
      open={open}
      onClose={() => close()}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        className={style.alertGrid}
      >
        <Card elevation={5} className={style.card}>
          <div className={style.buttonCloseModal}>
            <IconButton aria-label="delete" size="small" onClick={close}>
              <HighlightOff fontSize="small" />
            </IconButton>
          </div>
          <div style={{ margin: "5%" }}>
            <h4 className={style.titleModal}>Terminos y condiciones</h4>
          </div>
          <div style={{ margin: "5%" }}>
            <p className={classes.text}>
              ¿Fracasás cuando querés controlar el impulso a jugar? ¿Engañas a a
              tu familia para volver a jugar?
              <br />
              ¿Tenes deudas por el juego?
              <br />
              ¿Sentís que perdés el control cuando jugás?
              <br />
              Si perdés, ¿buscás revancha?
              <br />
            </p>
            <p className={`${classes.text} ${classes.textBold}`}>
              Te podemos ayudar, llamanos:
              <br />
              0800-444-4000
              <br />
            </p>
            <p
              className={`${classes.text} ${classes.textBold} ${classes.textCenter}`}
            >
              Política de identificación y conocimiento del cliente
            </p>
            <p className={`${classes.text}  ${classes.textCenter}`}>
              Los apostadores que obtengan premios superiores a $100.000,
              deberán cumplimentar, al momento de cobro de los mismos, con los
              requisitos exigidos por la Ley Nº 25.246, RES. Nº 199/2011 y RES.
              Nº52/2012 de la UIF y RES. Nº354/2013 del IPLyC en cuanto a la
              identificación del cliente.
              <br />
              “En caso que el cliente se niegue a brindar la documentación
              solicitada o no la tenga consigo, el pago del premio quedará
              pendiente hasta tanto se cumpla con lo requerido ” NO es un juego.
              <br />
              #jugaOficial
              <br />
            </p>

            <Button
              variant="contained"
              className={`${style.buttonAlert} ${style.buttonColorPrimary} ${style.buttonText}`}
              size="large"
              onClick={close}
            >
              Aceptar
            </Button>
          </div>
        </Card>
      </Grid>
    </Modal>
  );
};

const useStyle = makeStyles((theme) => ({
  textBold: {
    fontWeight: "bold",
  },
  textCenter: {
    textAlign: "center",
  },
  text: {
    fontSize: "90%",
    fontFamily: theme.fonts.primary,
    padding: 0,
  },
}));

export default ModalPolitcs;
