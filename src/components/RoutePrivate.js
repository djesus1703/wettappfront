import React from "react";
import { useSelector } from "react-redux";
import { Route, Redirect } from "react-router-dom";
const RoutePrivate = ({ component: Component, ...props }) => {
  const userAuthenticate = useSelector((state) => state.auth.auth);
  return (
    <Route
      {...props}
      render={(props) =>
        userAuthenticate ? <Component {...props} /> : <Redirect to="/login" />
      }
    />
  );
};

export default RoutePrivate;
