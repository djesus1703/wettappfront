import { ruta } from './config'
import axios from "axios";
class RestApi {
    
    constructor() {
        console.log("arrancando API", ruta)
    }

    async post(data,token = '') {
        // dispatch({ type: "SET_LOADING", payload: { view: true, text: "Cargando...", color: "red" } })
        let that = this;
        return new Promise(async (resolve, reject) => {
            // const token = await AsyncStorage.getItem('token');
            if(token !== ''){
                axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
            }
            axios.defaults.headers.common["Content-Type"] = "application/json";
            //axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
            axios.defaults.headers.common["Accept"] = "application/json";

            let url = ruta + data.url;
            let form = {};
            form = data.data;
            axios
                .post(url, form)
                .then(response => {
                    // console.log("RESPONSE POST >>>>", response);
                    that.responseStatus(response, response.status);
                    resolve(response.data);
                    return;
                })
                .catch(err => {
                    // console.error("ERROR POST Mensaje >>>>", err.response.data);
                    // console.error("ERROR POST status >>>>", err.response.status);
                    console.log(err);
                    console.log(err.response);
                    that.responseStatus(err.response, err.response.status);
                    reject(err);
                    return;
                });
        })
    }

    async get(data, token = '') {
        // console.log("state actual",state)
        let that = this;
        return new Promise(async (resolve, reject) => {
            // const token = await AsyncStorage.getItem('token');
            // console.log("token get ", token)
            if(token !== ''){
                axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
            }
            axios.defaults.headers.common["Content-Type"] = "application/json";
            axios.defaults.headers.common["Accept"] = "application/json";

            let url = ruta + data.url;
            // console.log("url",url)
            //   console.log("ROUTE GET >>>", url);
            // console.log('DATA >>>>',data.data)
            axios
                .get(url)
                .then(response => {
                    // console.log("RESPONSE GET >>>>", response);
                    resolve(response.data);
                    return;
                })
                .catch(err => {
                    // console.error("ERROR GET Mensaje >>>>", err.response.data);
                    // console.error("ERROR GET status >>>>", err.response.status);
                    if(err.response !== undefined){
                        that.responseStatus(err.response, err.response.status);
                    }
                    reject(err);
                    return;
                });
        });
    }


    // async clearData() {
    //     await AsyncStorage.multiSet([
    //         ['token', ""],
    //         ['type', ""],
    //         ['user_id', ""],
    //         ['status', ""],

    //     ], () => {
    //         dispatch({ type: "SET_TOKEN", payload: "" })
    //         dispatch({ type: "SET_USER", payload: "" })
    //     });
    // }

    responseStatus(data, status) {
        console.log("responseStatus", data.data, status);
        let text = data.data.error;
        let color = "";
        let redirect = false;
        let alerta = true;
        switch (status) {
            case 401:
                color = "red";
                text = "Usuario no autorizado"
                break;
            case 200:
                color = "#4CAF50";
                text = "Accion realizada con éxito "
                break;
            case 404:
                color = "#FB8C00";
                text = "Error " + status
                break;
            case 405:
                color = "#FB8C00";
                text = "Error " + status
                break;
            case 406:
                color = "#2196F3";
                text = "Error " + status
                break;
            case 500:
                color = "red";
                text = "Error " + status
                break;
            // case 200:
            //   color = "success";
            //   break;

            default:
                text = "Error no especificado"
                color = "error";
                break;
        }

        return { text };
        // dispatch({ type: "RESET_LOADING" })
        // dispatch({ type: "SET_SNACKBAR", payload: { view: true, color: color, text: text } })
        // if (status == 401) {
        //     setTimeout(() => {
        //         this.clearData()
        //     }, 2000);

        // }

    }
}

export default new RestApi()