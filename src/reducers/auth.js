import { AUTH_SUCCESS, CLOSE_SESSION, START_SESSION } from "../actions/auth";

const initialState = {
  auth: false,
  user: "",
  token: "",
  tel: "",
  user_name: "",
  role: "",
  user_id: "",
  cliente_id: "",
  agencia: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case AUTH_SUCCESS:
      return { ...state, auth: action.payload };
    case START_SESSION:
      return { ...state,
        user: action.payload.email,
        token:action.payload.token,
        cliente_id: action.payload.cliente_id,
        user_id: action.payload.user_id,
        user_name: action.payload.user_name,
        role: action.payload.role,
        tel: action.payload.tel,
        agencia: { ...action.payload.agencia }
      };
    case CLOSE_SESSION:
      return { ...initialState };
    default:
      return state;
  }
}
