import { AGREGAR_JUGADA_QUINELA, ELIMINAR_JUGADA } from "../actions/carrito";

const initialState = [];

export default function (state = initialState, action) {
  switch (action.type) {
    case AGREGAR_JUGADA_QUINELA:
      return [...state, action.payload];
    case ELIMINAR_JUGADA:
      return action.payload;
    default:
      return state;
  }
}
