import { combineReducers } from "redux";
import carrito from "./carrito";
import auth from "./auth";
import jugadas from "./jugada";

const rootReducer = combineReducers({ carrito, auth, jugadas });

export default rootReducer;
