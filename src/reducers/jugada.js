import { CLEAR_QUINELA, CHECK_QUINELA } from "../actions/jugadas";

const initialState = {};

export default function (state = initialState, action) {
  switch (action.type) {
    case CHECK_QUINELA:
      return {...state, quinela: action.payload};
    case CLEAR_QUINELA:
      return initialState;
    default:
      return state;
  }
}
