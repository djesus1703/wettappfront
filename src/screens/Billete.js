import React, { useState } from "react";
import { Grid } from "@material-ui/core";
import { ReactComponent as Logo } from "../assets/prestador1.svg";
import Header from "../components/Header";
import Fotter from "../components/Fotter";
import JuegoNumeros from "../components/JuegoNumeros";

const VALOR_JUGADA_BILLETE = 40;
const CANTIDAD_CAMPOS = 1;

export default function Bilete() {
  const [campos, setCampos] = useState({ campo0: "" });
  const rango = { inicio: 0, fin: 99 };

  return (
    <Grid container direction="row" justify="center">
      <Header />
      <JuegoNumeros
        Logo={Logo}
        nombre="Billete"
        rango={rango}
        valorJugada={VALOR_JUGADA_BILLETE}
        campos={campos}
        setCampos={setCampos}
        cantidad={CANTIDAD_CAMPOS}
        juega="Domingos"
        select
      />
      <Fotter />
    </Grid>
  );
}
