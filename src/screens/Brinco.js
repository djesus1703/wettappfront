import React, { useEffect, useState } from "react";
import { Grid } from "@material-ui/core";
import moment from "moment";
import Header from "../components/Header";
import Fotter from "../components/Fotter";
import Alert from "../components/Alert";
import JuegoNumeros from "../components/JuegoNumeros";
import { ReactComponent as Logo } from "../assets/prestador5.svg";
const VALOR_JUGADA_BRINCO = 50;
const CANTIDAD_CAMPOS = 8;
const CIERRE_JUEGO = [
  {
    dia: "Sunday",
  },
  {
    dia: "Saturday",
    hora: "21:10",
  },
];

export default function Brinco() {
  const [alertCloseGame, setAlertCloseGame] = useState(false);
  const [campos, setCampos] = useState({
    campo0: "",
    campo1: "",
    campo2: "",
    campo3: "",
    campo4: "",
    campo5: "",
    /*campo6: "",
    campo7: "",*/
  });

  useEffect(() => {
    const date = moment().format("HH:mm");
    const day = moment().format("dddd");

    CIERRE_JUEGO.forEach((item) => {
      if (date >= item.hora && day === item.dia) {
        setAlertCloseGame(true);
      } else if (day === "Sunday") {
        setAlertCloseGame(true);
      }
    });
  }, []);

  const rango = { inicio: 0, fin: 39 };

  return (
    <Grid container direction="row" justify="center">
      <Header />
      <JuegoNumeros
        Logo={Logo}
        nombre="Brinco"
        rango={rango}
        valorJugada={VALOR_JUGADA_BRINCO}
        campos={campos}
        setCampos={setCampos}
        cantidad={CANTIDAD_CAMPOS}
        juega="Domingos"
      />
      <Fotter />
      <Alert
        openAlert={alertCloseGame}
        setOpenAlert={setAlertCloseGame}
        closeGame
        title="Quinela a cerrado"
        message="Este juego a cerrado te deseamos la mejor suerte!"
      />
    </Grid>
  );
}
