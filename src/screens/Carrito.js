import React, { useEffect, useState, Fragment } from "react";
import {
  Grid,
  useTheme,
  makeStyles,
  Card,
  Modal,
  IconButton,
  Input,
  Button,
  Divider,
} from "@material-ui/core";
import HighlightOff from "@material-ui/icons/HighlightOff";
import InputAdornment from "@material-ui/core/InputAdornment";
import { useSelector, useDispatch } from "react-redux";
import Cancel from "@material-ui/icons/Cancel";
import { ConfirmationNumberTwoTone, VerifiedUser } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import Banner5 from "../components/Banner5";
import { eliminarJugada } from "../actions/carrito";
import styles from "../common/styles";
import Api from "../config/Api";
import { getDataFromCarrito } from "../common/helper";
import PropagateLoader from "react-spinners/PropagateLoader";

export default function Carrito() {
  const classes = useStyles();
  const style = styles();
  const theme = useTheme();
  const history = useHistory();
  const [open, setOpen] = useState(true);
  const [loading, setLoading] = useState(false);
  const [open2, setOpen2] = useState(false);
  const [totalJugadas, setTotalJugadas] = useState(0);
  const dispatch = useDispatch();

  const jugadas = useSelector((state) => state.carrito);

  const { carrito } = useSelector((state) => state);
  const { token, cliente_id, agencia, user_id } = useSelector((state) => state.auth);

  useEffect(() => {
    const totales = [];
    jugadas.forEach((item) => totales.push(item.total));
    setTotalJugadas(
      totales.length > 0
        ? totales.reduce(function (a, b) {
            return a + b;
          })
        : 0
    );
  }, [jugadas]);

  const handleClose = () => {
    setOpen(false);
    history.goBack();
  };

  const eliminarJugadas = (id) => {
    console.log(id);
    const jugadasFiltradas = jugadas.filter((jugada) => jugada.id !== id);
    dispatch(eliminarJugada(jugadasFiltradas));
  };

  const handlePagar = async () => {
    setLoading(true);
    const data = {
      url: `/jugada`,
      data: {
        cliente_id: cliente_id,
        agencia_id: agencia.agencia_id,
        monto_total: totalJugadas,
      }
    };
    let jugadasDelete = [];
    setLoading(true);
    Api.post(data, token).then((res)=>{
      
      carrito.forEach(element => {
        const sendDataJugada = getDataFromCarrito(element);
        const data = {
          url: sendDataJugada.url,
          data: {...sendDataJugada,jugada_id: res.id},
        }
        Api.post(data,token).then(res=>{
          jugadasDelete.push(element.id)
          console.log("jugada ok",res)
        }).catch(err=>{
          console.log("error jugada",err)
        })
      });
      return res.id;
    }).then((res)=>{
      console.log("res",res);
      const jugadasFiltradas = carrito.filter((jug) => jugadasDelete.indexOf(jug.id) === -1);
      dispatch(eliminarJugada(jugadasFiltradas));
      //send paid
      const data_paid = {
        url: `/paid/${res}`,
      };
      Api.get(data_paid,token).then(res=>{
        console.log("r",res);
        setLoading(false);
        if(res.StatusCode !== -1){
          alert(res.StatusMessage)
        }else{
          let url = res.URL_Request;
          window.location.href = url;
        }
        
      }).catch(err=>{
        setLoading(false);
        console.log("error en el pago",err);
      })
    }).catch((err)=>{
      setLoading(false);
      console.log("error")
      console.log(err)
      //alert(Api.responseStatus(err.response, err.response.status).text)
    });
    
  }
 
  /*console.log(jugadas.logo);*/
  return (
    <Grid container direction="row" justify="center">
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className={classes.containerGrid}
        >
          <Card elevation={5} className={classes.containerCard}>
            <div className={style.buttonCloseModal}>
              <IconButton
                aria-label="delete"
                size="small"
                onClick={handleClose}
              >
                <HighlightOff fontSize="small" />
              </IconButton>
            </div>
            <div className={classes.containerScroll}>
              {jugadas.length === 0 && (
                <p className={`${classes.text} ${classes.textCenter}`}>
                  No has realizado ninguna jugada
                </p>
              )}
              {jugadas.map((jugada) => (
                <Grid container style={{ marginBottom: "3%" }} key={jugada.id}>
                  <Grid item xs={12} style={{ marginBottom: "3%" }}>
                    <Banner5 imagen={jugada.Logo} tittle={jugada.nombre} />
                  </Grid>
                  <Grid
                    item
                    xs={8}
                    container
                    direction="row"
                    alignItems="center"
                  >
                    {jugada.tipo === 1 ? (
                      jugada.jugadas.map((item) => (
                        
                        <Fragment key={item.numero + item.posicion}>
                          {item.tipo === "redoblona" && <Grid item xs={12} style={{ marginBottom: "1%" }}>
                            <div className={classes.text}>Redoblona</div>
                          </Grid>}
                          <Grid item xs={6} style={{ marginBottom: "1%" }}>
                            <div className={classes.text}>Número</div>
                          </Grid>
                          <Grid item xs={6} style={{ marginBottom: "1%" }}>
                            <div className={classes.text}>Posición</div>
                          </Grid>
                          <Grid item xs={6}>
                            <Input
                              type="number"
                              disableUnderline={true}
                              disabled
                              value={item.numero.length === 1 ? `0${item.numero}` : item.numero}
                              className={classes.inputsText}
                            />
                          </Grid>
                          <Grid item xs={6}>
                            <Input
                              type="number"
                              disableUnderline={true}
                              disabled
                              value={item.posicion}
                              className={classes.inputsText}
                            />
                          </Grid>
                        </Fragment>
                      ))
                    ) : (
                      <>
                        <Grid item xs={12} className={classes.text}>
                          Números
                        </Grid>
                        {Object.entries(jugada.jugadas).map(([key, value]) => (
                          <Grid item xs={4} key={key + value}>
                            <Input
                              type="number"
                              disableUnderline={true}
                              disabled
                              value={value}
                              className={classes.inputsText}
                            />
                          </Grid>
                        ))}
                      </>
                    )}
                  </Grid>
                  <Grid item xs={4}>
                    <div className={classes.iconClose}>
                      <IconButton onClick={() => eliminarJugadas(jugada.id)}>
                        <Cancel />
                      </IconButton>
                    </div>
                  </Grid>
                  {/* ////////////////////////////// TOTAL  ////////////////////////////////// */}
                  <Grid item xs={4} />
                  <Grid item xs={4} />
                  <Grid item xs={4}>
                    <Input
                      type="number"
                      disableUnderline={true}
                      startAdornment={
                        <InputAdornment
                          position="start"
                          color={theme.colors.black}
                        >
                          $
                        </InputAdornment>
                      }
                      value={jugada.total}
                      className={classes.inputsText}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Divider />
                  </Grid>
                </Grid>
              ))}
            </div>
            <Grid item xs={12} className={classes.totalContainer}>
              <div className={classes.total}>
                {`Total a cobrar: $ ${totalJugadas}`}
              </div>
              <div className={classes.containerButton}>
                <Button
                  variant="contained"
                  size="large"
                  startIcon={<ConfirmationNumberTwoTone />}
                  className={`${style.button} ${classes.colorButton} ${style.buttonText}`}
                  onClick={() => {
                    history.push("/Jugar");
                  }}
                >
                  Continuar Jugando
                </Button>
              </div>
              {jugadas.length > 0 && (
                <div className={classes.containerButton}>
                  <Button
                    variant="contained"
                    size="large"
                    startIcon={loading ? null : <ConfirmationNumberTwoTone />}
                    className={`${classes.showLoading} ${style.button} ${classes.colorButton} ${style.buttonText}`}
                    /*onClick={() => {
                      history.push("/PagoTarjeta");
                    }}*/
                    onClick={handlePagar}
                  >
                    { !loading && <>Terminar Compra</> }
                    { loading && <PropagateLoader color={'#e97400'} loading={loading} size={18} />}
                    
                  </Button>
                </div>
              )}
            </Grid>
          </Card>
        </Grid>
      </Modal>
      <Modal
        open={open2}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className={style.alertGrid}
        >
          <Card elevation={5} className={style.alertCard}>
            <div className={style.buttonCloseModal}>
              <IconButton
                aria-label="delete"
                size="small"
                onClick={() => {
                  setOpen2(false);
                }}
              >
                <HighlightOff fontSize="small" />
              </IconButton>
            </div>
            <IconButton
              className={classes.containerIconModal}
              onClick={() => {
                history.push("/");
              }}
            >
              <VerifiedUser />
            </IconButton>
            <h1>Gracias..¡Suerte!</h1>
          </Card>
        </Grid>
      </Modal>
    </Grid>
  );
}

const useStyles = makeStyles((theme) => ({
  cover1: {
    width: 45,
    height: 40,
    alignContent: "center",
    justifyContent: "center",
    borderRadius: 7,
    borderColor: theme.colors.lightGray,
    border: "2px solid",
    marginLeft: "5%",
  },
  showLoading: {
    minHeight: "50px",
  },
  containerGrid: {
    width: "100%",
    height: "100%",
    backgroundColor: "transparent",
  },
  containerCard: {
    width: "90%",
    height: "90%",
    backgroundColor: theme.colors.white,
    borderRadius: 10,
  },
  containerScroll: {
    width: "100%",
    height: "60%",
    overflow: "scroll",
  },
  textCenter: {
    textAlign: "center",
  },
  text: {
    fontFamily: theme.fonts.primary,
    fontWeight: "bold",
    paddingLeft: 20,
  },
  iconClose: {
    width: "100%",
    textAlign: "center",
    "& svg": {
      color: "red",
      fontSize: 40,
    },
  },
  inputsText: {
    width: "80%",
    height: "70%",
    textAlign: "center",
    backgroundColor: theme.colors.lightGray,
    fontWeight: "bold",
    padding: 8,
    borderRadius: 10,
    margin: "5% 0 5% 10%",
  },
  totalContainer: {
    position: "absolute",
    bottom: "5%",
    width: "90%",
    backgroundColor: theme.colors.white,
    borderRadius: 10,
    textAlign: "center",
  },
  containerButton: {
    width: "80%",
    height: "100%",
    textAlign: "center",
    borderRadius: 10,
    margin: "0 auto 5% auto",
  },
  colorButton: {
    background: theme.colors.mediumGreen,
  },
  total: {
    textAlign: "right",
    borderRadius: 10,
    fontSize: "105%",
    fontWeight: "bold",
    paddingTop: "4%",
    paddingBottom: "4%",
    marginRight: "5%",
    fontFamily: theme.fonts.primary,
  },
}));
