import React, { useState } from "react";
import {
  Grid,
  makeStyles,
  Card,
  Modal,
  IconButton,
  Button,
} from "@material-ui/core";
import HighlightOff from "@material-ui/icons/HighlightOff";
import { useHistory } from "react-router-dom";
import styles from "../common/styles";

export default function CodigoCliente() {
  const classes = useStyles();
  const style = styles();
  const history = useHistory();
  const [open, setOpen] = useState(true);
  const handleClose = () => {
    setOpen(false);
    history.goBack();
  };

  return (
    <Grid container direction="row" justify="center">
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className={style.alertGrid}
        >
          <Card elevation={5} className={classes.containerCard}>
            <div className={style.buttonCloseModal}>
              <IconButton
                aria-label="delete"
                size="small"
                onClick={handleClose}
              >
                <HighlightOff fontSize="small" />
              </IconButton>
            </div>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
              className={classes.contentGridCode}
            >
              <div className={classes.content}>
                <div className={classes.contentCode}>
                  <h4>Tu Codigo</h4>
                  <input type="text" disableUnderline={true} value="985632" />
                </div>

                <div
                  style={{
                    backgroundColor: "transparent",
                    borderRadius: 10,
                    textAlign: "center",
                  }}
                >
                  <Button
                    variant="contained"
                    className={`${style.buttonAlert} ${style.buttonText} ${style.buttonColorPrimary}`}
                    size="large"
                    onClick={() => {
                      history.goBack();
                    }}
                  >
                    Continuar
                  </Button>
                </div>
              </div>
              
            </Grid>
          </Card>
        </Grid>
      </Modal>
    </Grid>
  );
}

const useStyles = makeStyles((theme) => ({
  containerCard: {
    justifyContent: "center",
    width: "90%",
    height: "90%",
    backgroundColor: theme.colors.white,
    borderRadius: 10,
  },
  contentGridCode: {
    width: "100%",
    height: "90%",
    backgroundColor: theme.colors.white,
  },
  contentCode: {
    textAlign: "center",
    backgroundColor: theme.colors.lightGray,
    borderRadius: 10,
    padding: "3% 0",
    margin: "5%",
    "& h4": {
      fontSize: "120%",
      fontFamily: theme.fonts.primary,
      textAlign: "center",
    },
    "& input": {
      width: "50%",
      textAlign: "center",
      backgroundColor: "transparent",
      border: "none",
      fontSize: "150%",
      fontWeight: "bold",
      padding: 8,
      outline: 0,
    },
  },
  content: {

  }
}));
