import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { Grid, Card, Button, Box, makeStyles, Modal, IconButton } from "@material-ui/core";
import MailTwoTone from "@material-ui/icons/MailTwoTone";
import PhoneAndroidTwoTone from "@material-ui/icons/PhoneAndroidTwoTone";
import AccountCircleTwoTone from "@material-ui/icons/AccountCircleTwoTone";
import { useSelector } from "react-redux";

import { ReactComponent as Logo } from "../assets/monedero_grande.svg";
import { ReactComponent as Logo1 } from "../assets/codigo_grande.svg";
import { ReactComponent as Logo2 } from "../assets/jugadas_grande.svg";
import { ReactComponent as Logo3 } from "../assets/resultado_blanco.svg";
import { ReactComponent as Logo4 } from "../assets/estadisticas_blanco.svg";

import Header from "../components/Header";
import Fotter from "../components/Fotter";
import BottomJugar from "../components/BottomJugar";
import styles from "../common/styles";
import Api from "../config/Api";
import PropagateLoader from "react-spinners/PropagateLoader";
import HighlightOff from "@material-ui/icons/HighlightOff";
import { VerifiedUser, Warning } from "@material-ui/icons";

export default function Inicio(props) {
  const emailUser = useSelector((state) => state.auth.user);
  const history = useHistory();
  const classes = useStyles();
  const { agencia, tel, user_name } = useSelector((state) => state.auth);
  const [totalJugado, setTotalJugado] = useState(0);
  const [loadingTotal, setLoadingTotal] = useState(false);

  const { token, cliente_id } = useSelector((state) => state.auth);
  const style = styles();

  let { payment, id } = useParams();
  const [showPayment, setShowPayment] = useState(false);
  const [paymentSuccess, setPaymentSuccess] = useState(false);

  useEffect(() => {
    setLoadingTotal(true);
    if (payment) {
      if (payment === 'success') {

        Api.get({ url: `/mark_paid/${id}` }, token).then((result) => {
          console.log(result);
          setPaymentSuccess(true)

        }).catch(err => {
          setLoadingTotal(false);
        });
      } else {
        setPaymentSuccess(false)
      }
      setShowPayment(true)
    }

    Api.get({ url: `/get_total_plays/${cliente_id}` }, token).then((result) => {
      console.log(result);
      if (result.message !== undefined) {
        setTotalJugado(0);
      } else {
        setTotalJugado(result.total_historico);
      }
      setLoadingTotal(false);

    }).catch(err => {
      setLoadingTotal(false);
    });

  }, []);

  const user = [
    { icono: <AccountCircleTwoTone fontSize="small" />, valor: user_name },
    { icono: <PhoneAndroidTwoTone fontSize="small" />, valor: tel },
    { icono: <MailTwoTone fontSize="small" />, valor: emailUser },
  ];
  const links = [
    {
      icono: <Logo className={classes.svg} />,
      titulo: "Mi monedero",
      url: "/",
    },
    {
      icono: <Logo2 className={classes.svg} />,
      titulo: "Mis jugadas",
      url: "/MisJugadas",
    },  
    {
      icono: <Logo1 className={classes.svg1} />,
      titulo: "Código cliente",
      url: "/CodigoCliente",
    },
  ];
  const buttons = [
    {
      icono: <Logo3 style={{}} />,
      url: "/MisResultados",
      color: style.buttonColorSecondary,
      titulo: "Resultados últimas jugadas",
    },
    {
      icono: <Logo4 style={{}} />,
      url: "/Estadisticas",
      color: style.buttonColorTertiary,
      titulo: "Estadísticas",
    },
    {
      icono: "",
      url: "/Jugar",
      color: style.buttonColorPrimary,
      titulo: "Jugar!",
    },
  ];
  return (
    <Grid container direction="row" justify="center">
      <Header />
      <Card elevation={2} className={classes.root}>
        <Grid container item xs={12}>
          <Grid item xs={5}>
            {
              agencia.foto &&
              <div className={classes.containerImage}>
                <img className={classes.image2} src={agencia.foto} alt="userImage" />
              </div>
            }
          </Grid>
          <Grid item xs={agencia.foto ? 7 : 10}>
            <Box
              display="flex"
              justifyContent="flex-start"
              p={1}
              className={classes.boxContainer}
            >
              {agencia && agencia.nombre !== undefined && <><Box p={2}>
                <div className={classes.textUser}>Agencia:</div>
              </Box>
                <Box p={2}>
                  <div className={classes.textUser}>{agencia.nombre}</div>
                </Box></>}
            </Box>
            {user.map((dato) => (
              <Box
                display="flex"
                justifyContent="flex-start"
                p={1}
                className={classes.boxContainer}
              >
                <Box p={1}>{dato.icono}</Box>
                <Box p={1}>
                  <div className={classes.textUser}>{dato.valor}</div>
                </Box>
              </Box>
            ))}
          </Grid>
        </Grid>

        <Grid container item xs={12} className={classes.linksContainer}>
          {links.map((link) => (
            <Grid
              item
              xs={3}
              onClick={() => {
                history.push(link.url);
              }}
            >
              <BottomJugar imagen={link.icono} tittle8={link.titulo} />
            </Grid>
          ))}
          <Grid item xs={11} className={classes.containerTotal}>
          
            <div className={`${classes.text} ${classes.textCenter} ${classes.fixHeight}`}>
              { !loadingTotal && <>Total jugado ${totalJugado}</>}
              { loadingTotal && <><PropagateLoader color={'#e97400'} loading={loadingTotal} size={25} /></>}
            </div>
          </Grid>
        </Grid>
      </Card>

      {buttons.map((button) => (
        <Button
          kye={button.url}
          variant="contained"
          size="small"
          onClick={() => {
            history.push(button.url);
          }}
          className={`${classes.buttonContainer} ${button.color}`}
          startIcon={button.icono}
        >
          <div className={`${classes.text} ${classes.colorButtonText}`}>
            {button.titulo}
          </div>
        </Button>
      ))}
      <div className={`${classes.footerText} ${style.finalMargin}`}>
        JUGAR COMPULSIVAMENTE ES PERJUDICIAL PARA LA SALUD LEY N°15131
      </div>
      <Modal
        open={showPayment}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className={style.alertGrid}
        >
          <Card elevation={5} className={style.alertCard}>
            <div className={style.buttonCloseModal}>
              <IconButton
                aria-label="delete"
                size="small"
                onClick={() => {
                  setShowPayment(false);
                }}
              >
                <HighlightOff fontSize="small" />
              </IconButton>
            </div>

            {
              paymentSuccess ? (
                <>
                  <IconButton
                    className={classes.containerIconModalSuccess}
                    onClick={() => {
                      history.push("/");
                    }}
                  >
                    <VerifiedUser />
                  </IconButton>
                  <h1>¡Su pago se realizó correctamente!</h1>
                </>
              ) : (
                <>
                  <IconButton
                    className={classes.containerIconModalError}
                    onClick={() => {
                      history.push("/Carrito");
                    }}
                  >
                    <Warning />
                  </IconButton>
                  <h1>¡Ocurrió un error con su pago, intente nuevamente!</h1>
                </>
              )
            }
          </Card>
        </Grid>
      </Modal>
      <Fotter />
    </Grid>
  );
}
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    borderRadius: 10,
  },
  root1: {
    margin: "3%",
  },
  textCenter: {
    textAlign: "center",
  },
  boxContainer: {
    padding: 0,
    margin: "-3% 0",
    "&:first-child": {
      marginTop: "5%",
    },
    overflowWrap: 'anywhere',
  },
  textUser: {
    fontSize: "90%",
    fontFamily: theme.fonts.primary,
  },
  linksContainer: {
    // width: "100%",
    // zIndex: 1,
    // backgroundColor: theme.colors.white,
    // alignContent: "center",
    // justifyContent: "space-around",
    // marginBottom: "4%",
    // marginTop: "4%",
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    alignContent: "center",
    justifyContent: "center",
    marginTop: 20
  },
  buttonContainer: {
    width: "100%",
    justifyContent: "space-evenly",
    borderRadius: 10,
    marginBottom: "4%",
  },
  containerImage: {
    width: "100%",
    height: "100%",
    textAlign: "center",
    marginTop: "5%",
  },
  image2: {
    width: "70%",
    height: "70%",
    borderRadius: 500,
    borderColor: theme.colors.gray,
    border: "8px solid",
  },
  text: {
    fontFamily: theme.fonts.primary,
    fontWeight: "bold",
    fontSize: "150%",
    margin: "6%",
    textTransform: "none",
  },
  containerTotal: {
    backgroundColor: theme.colors.lightGray,
    borderRadius: 10,
    marginTop: "4%",
  },
  colorButtonText: {
    color: theme.colors.white,
  },
  footerText: {
    fontFamily: theme.fonts.primary,
    fontSize: "58%",
  },
  svg: {
    width: "70%",
    height: "70%",
    marginTop: "10%",
    marginBottom: "5%",
  },
  fixHeight: {
    height: '25px',
  },
  svg1: {
    width: "70%",
    height: "70%",
    marginTop: "13%",
    marginBottom: "15%",
  },
  containerIconModalSuccess: {
    margin: "4%",
    textAlign: "center",
    "& svg": {
      fontSize: "400%",
      color: theme.colors.darkGreen,
    },
  },
  containerIconModalError: {
    margin: "4%",
    textAlign: "center",
    "& svg": {
      fontSize: "400%",
      color: theme.colors.darkRed,
    },
  },
}));
