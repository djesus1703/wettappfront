import React from "react";
import { Grid, Card, makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { ReactComponent as Logo } from "../assets/prestador1.svg";
import { ReactComponent as Logo1 } from "../assets/prestador2.svg";
import { ReactComponent as Logo2 } from "../assets/prestador4.svg";
import { ReactComponent as Logo3 } from "../assets/prestador3.svg";
import { ReactComponent as Logo4 } from "../assets/prestador5.svg";
import { ReactComponent as Logo5 } from "../assets/quini_6.svg";
// import { ReactComponent as Logo6 } from "../assets/prestador1.svg";
import Header from "../components/Header";
import Fotter from "../components/Fotter";
import BottomJugar from "../components/BottomJugar";

export default function Jugar() {
  const history = useHistory();
  const classes = useStyles();

  const juegos = [
    {
      nombre: "Quiniela Provincia",
      ruta: "/Quiniela",
      logo: <Logo className={classes.svg} />,
    },
    {
      nombre: "Quinela",
      ruta: "/QuinielaPlus",
      logo: <Logo1 className={classes.svg2} />,
    },
    { nombre: "Loto", ruta: "/Loto", logo: <Logo3 className={classes.svg1} /> },
    {
      nombre: "Loto Plus",
      ruta: "/Loto5",
      logo: <Logo2 className={classes.svg3} />,
    },
    {
      nombre: "Brinco",
      ruta: "/Brinco",
      logo: <Logo4 className={classes.svg3} />,
    },
    {
      nombre: "Quini6",
      ruta: "/Quini6",
      logo: <Logo5 className={classes.svg3} />,
    }/*,
    {
      nombre: "Billetes",
      ruta: "/Billete",
      logo: <Logo6 className={classes.svg} />,
    },*/
  ];
  return (
    <Grid container direction="row" justify="center">
      <Header />
      <Card elevation={2} className={classes.root}>
        <div className={classes.letra1}>Buenos Aires</div>
        <Grid container item xs={12} className={classes.root2}>
          {juegos.map((juego) => (
            <Grid
              key={juego.ruta}
              item
              xs={3}
              className={classes.root1}
              onClick={() => {
                history.push(juego.ruta);
              }}
            >
              <BottomJugar imagen={juego.logo} tittle8={juego.nombre} />
            </Grid>
          ))}
        </Grid>
      </Card>
      <Fotter />
    </Grid>
  );
}
const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    alignContent: "center",
    justifyContent: "center",
    borderRadius: 10,
    marginBottom: "20%",
  },

  root1: {
    margin: "3%",
  },
  root2: {
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    alignContent: "center",
    justifyContent: "center",
  },
  letra1: {
    fontFamily: theme.colors.primary,
    fontWeight: "normal",
    fontSize: "25px",
    marginTop: "3%",
    marginLeft: "6%",
  },
  svg: {
    width: "70%",
    height: "70%",
    marginTop: "10%",
  },
  svg1: {
    width: "80%",
    height: "70%",
    marginTop: "30%",
    marginBottom: "15%",
  },
  svg2: {
    width: "70%",
    height: "70%",
    marginTop: "10%",
    marginBottom: "10%",
  },
  svg3: {
    width: "70%",
    height: "70%",
    marginTop: "20%",
    marginBottom: "20%",
  },
}));
