import React, { useState } from "react";
import {
  TextField,
  IconButton,
  Card,
  makeStyles,
  Grid,
  Button,
  Modal,
} from "@material-ui/core";
import imagen2 from "../../src/assets/icon_inicio_3.png";
import HeaderLogin from "../components/HeaderLogin";
import HighlightOff from "@material-ui/icons/HighlightOff";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import BannerLogin from "../components/BannerLogin";
import Api from "../config/Api";
import Alert from "../components/Alert";
import styles from "../common/styles";
import { autenticarUsuario, iniciarSesion } from "../actions/auth";

export default function Login() {
  const history = useHistory();
  const classes = useStyles();
  const dispatch = useDispatch();
  const style = styles();

  const [open, setOpen] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [openAlertSuccefull, setOpenAlertSuccefull] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("Usuario y/o contraseña inválidos");

  const handleOpen = () => {
    Api.post({
      url: "/login",
      data: {
        email: email,
        password: password,
      },
    })
      .then(async (res) => {
        console.log("SI ENTRO", res);
        const agencia = await Api.get({url:`/get_agencia_of_user`},res.access_token);
        dispatch(autenticarUsuario(true));
        dispatch(iniciarSesion({
          email: email,
          user_name: res.name,
          last_name: res.lastname,
          role: res.role,
          tel: res.tel,
          token: res.access_token,
          cliente_id: res.cliente_id,
          user_id: res.user_id,
          agencia: {
            agencia_id: agencia.id,
            nombre: agencia.nombre,
            foto: agencia.foto,
          }
        }));
        history.replace("/");
      })
      .catch(error => {
        setMessage(error.response.data.message);
        setOpenAlert(true);
      });
  };

  const sendEmail = () => {
    Api.post({
      url: "/forgot_password",
      data: {
        email: email,
      },
    })
      .then(async (res) => {
        console.log("SI ENTRO O NO ENTRO", res);
        setMessage("Se ha enviado un email a su cuenta con instrucciones.");
        setOpenAlertSuccefull(true);
        //history.replace("/login");
      })
      .catch(error => {
        //console.log(error.response.data.message);
        setMessage(error.response.data.message);
        setOpenAlert(true);
      });
  }

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <Grid container direction="row" justify="center">
      <HeaderLogin />
      <Card elevation={0} className={classes.root}>
        <Grid container item xs={12} className={classes.root1}>
          <BannerLogin imagen={imagen2} tittle="Hola!" />
          <div className={classes.textSubTitle}>
            Por favor ingresá tus datos:
          </div>
          <Grid item xs={12} style={{ marginTop: "2%", marginBottom: "2%" }}>
            <TextField
              label="Email"
              variant="outlined"
              className={style.inputText}
              onChange={(text) => {
                setEmail(text.target.value);
              }}
            />
          </Grid>
          <Grid item xs={12} style={{ marginTop: "2%", marginBottom: "2%" }}>
            <TextField
              label="Contraseña"
              variant="outlined"
              type="password"
              className={style.inputText}
              onChange={(text) => {
                setPassword(text.target.value);
              }}
            />
          </Grid>
          <Button
            onClick={() => setOpen(!open)}
            className={classes.rememberPass}
          >
            Olvidé mi contraseña
          </Button>
          <Button
            variant="contained"
            size="small"
            className={`${classes.containerButton} ${style.buttonText} ${style.buttonColorPrimary}`}
            onClick={() => {
              handleOpen();
            }}
          >
            Ingresar
          </Button>
          <Button
            variant="contained"
            size="small"
            className={`${classes.containerButton} ${style.buttonColorTertiary} ${style.buttonText}`}
            onClick={() => {
              history.push("/Register");
            }}
          >
            ¿No tenés cuenta? Regístrate aquí
          </Button>
        </Grid>
      </Card>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        style={{ alignContent: "center" }}
      >
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className={style.alertGrid}
        >
          <Card elevation={5} className={classes.containerCard}>
            <div className={style.buttonCloseModal}>
              <IconButton
                aria-label="delete"
                size="small"
                onClick={handleClose}
              >
                <HighlightOff fontSize="small" />
              </IconButton>
            </div>
            <Grid item xs={12} style={{ margin: "5%" }}>
              <BannerLogin imagen={imagen2} tittle="Enviar!" />
            </Grid>
            <Grid
              item
              xs={12}
              container
              justify="center"
              alignItems="center"
              style={{ marginTop: "30%" }}
            >
              <div style={{ width: "80%" }}>
                <TextField
                  label="Email"
                  variant="outlined"
                  className={style.inputText}
                  onChange={(text) => {
                    setEmail(text.target.value);
                  }}
                />
                <div
                  className={`${classes.textSubTitle} ${classes.textCenter}`}
                >
                  Coloca tu email, para recuperar tu cuenta.
                </div>
              </div>
            </Grid>

            <Grid
              item
              xs={12}
              container
              justify="center"
              alignItems="center"
              style={{ marginTop: "30%" }}
            >
              <Button
                variant="contained"
                size="small"
                onClick={() => sendEmail()}
                className={`${classes.buttonFooterModal} ${style.buttonColorPrimary} ${style.finalMargin}`}
              >
                <div className={classes.buttonTextFooterModal}>Continuar</div>
              </Button>
            </Grid>
          </Card>
        </Grid>
      </Modal>
      <Alert
        openAlert={openAlert}
        setOpenAlert={setOpenAlert}
        error
        title={"Upss!!"}
        message={message}
      />
      <Alert
        openAlert={openAlertSuccefull}
        setOpenAlert={setOpenAlertSuccefull}
        title={"Envio Satisfactorio"}
        closeGame={true}
        message={message}
      />
    </Grid>
  );
}
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    borderRadius: 10,
  },
  root1: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    marginTop: "5%",
    margin: "5%",
  },
  textCenter: {
    textAlign: "center",
  },
  textSubTitle: {
    fontFamily: theme.fonts.primary,
    margin: "4%",
    fontSize: "80%",
  },
  rememberPass: {
    textTransform: "none",
    fontFamily: theme.fonts.primary,
    margin: "4%",
    fontSize: 14,
    textDecoration: "underline",
    borderRadius: 10,
  },
  containerButton: {
    width: "100%",
    marginBottom: "2%",
    justifyContent: "center",
    alignContent: "center",
    borderRadius: 7,
  },
  buttonTextFooterModal: {
    fontSize: "150%",
    textTransform: "none",
    fontWeight: "bold",
    margin: "2%",
    color: theme.colors.white,
  },
  containerCard: {
    width: "90%",
    height: "90%",
    backgroundColor: theme.colors.white,
    borderRadius: 10,
  },
  buttonFooterModal: {
    width: "90%",
    justifyContent: "center",
    alignContent: "center",
    borderRadius: 7,
  },
}));
