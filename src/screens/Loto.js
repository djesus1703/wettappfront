import React, { useEffect, useState } from "react";
import { Grid } from "@material-ui/core";
import moment from "moment";
import { ReactComponent as Logo } from "../assets/prestador3.svg";
import Header from "../components/Header";
import Fotter from "../components/Fotter";
import Alert from "../components/Alert";
import JuegoNumeros from "../components/JuegoNumeros";

const PRECIO_INICIAL_JUEGO = 70;
const PRECIO_TRADICIONAL = 70;
const PRECIO_DESQUITE = 90;
const PRECIO_SASE_O_SALE = 100;
const CANTIDAD_CAMPOS = 6;
const CIERRE_JUEGO = [
  /*{
    dia: "Wednesday",
    hora: "21:10",
  },*/
  {
    dia: "Saturday",
    hora: "21:10",
  },
];

export default function Loto(props) {
  const [precio, setPrecio] = useState(PRECIO_INICIAL_JUEGO);
  const [alertCloseGame, setAlertCloseGame] = useState(false);
  const [campos, setCampos] = useState({
    campo0: "",
    campo1: "",
    campo2: "",
    campo3: "",
    campo4: "",
    campo5: "",
  });
  const [camposJackpot, setCamposJackpot] = useState({
    campo6: "",
    campo7: "",
  });

  useEffect(() => {
    const date = moment().format("HH:mm");
    const day = moment().format("dddd");

    CIERRE_JUEGO.forEach((item) => {
      if (date >= item.hora && day === item.dia) {
        setAlertCloseGame(true);
      } else if (day === "Sunday") {
        setAlertCloseGame(true);
      }
    });
  }, []);

  const rango = { inicio: 0, fin: 41 };
  const rango1 = { inicio: 0, fin: 9 };

  return (
    <Grid container direction="row" justify="center">
      <Header />
      <JuegoNumeros
        Logo={Logo}
        nombre="Loto"
        rango={rango}
        valorJugada={precio}
        campos={campos}
        setCampos={setCampos}
        cantidad={CANTIDAD_CAMPOS}
        juega="Sabados"
        modificarValor
        setValorJugada={setPrecio}
        precioSale={PRECIO_SASE_O_SALE}
        precioTradicional={PRECIO_TRADICIONAL}
        preciosDesquite={PRECIO_DESQUITE}
        jackpot
        camposJackpot={camposJackpot}
        setCamposJackpot={setCamposJackpot}
        rangoJackpot={rango1}
      />
      <Fotter />
      <Alert
        openAlert={alertCloseGame}
        setOpenAlert={setAlertCloseGame}
        closeGame
        title="Quinela a cerrado"
        message="Este juego a cerrado te deseamos la mejor suerte!"
      />
    </Grid>
  );
}
