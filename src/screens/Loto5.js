import React, { useEffect, useState } from "react";
import { Grid } from "@material-ui/core";
import moment from "moment";
import { ReactComponent as Logo }  from "../assets/prestador4.svg";
import Header from "../components/Header";
import Fotter from "../components/Fotter";
import Alert from "../components/Alert";
import JuegoNumeros from "../components/JuegoNumeros";

const VALOR_JUGADA_LOTO5 = 50;
const CANTIDAD_CAMPOS = 5;
const CIERRE_JUEGO = [
  {
    dia: "Friday",
    hora: "19:40",
  },
];

export default function Loto5() {
  const [alertCloseGame, setAlertCloseGame] = useState(false);
  const [campos, setCampos] = useState({
    campo0: "",
    campo1: "",
    campo2: "",
    campo3: "",
    campo4: "",
    /*campo5: "",
    campo6: "",
    campo7: "",*/
  });

  useEffect(() => {
    const date = moment().format("HH:mm");
    const day = moment().format("dddd");

    CIERRE_JUEGO.forEach((item) => {
      if (date >= item.hora && day === item.dia) {
        setAlertCloseGame(true);
      } else if (day === "Sunday") {
        setAlertCloseGame(true);
      }
    });
  }, []);

  const rango = { inicio: 0, fin: 36 };

  return (
    <Grid container direction="row" justify="center">
      <Header />
      <JuegoNumeros
        Logo={Logo}
        nombre="Loto Plus"
        rango={rango}
        valorJugada={VALOR_JUGADA_LOTO5}
        campos={campos}
        setCampos={setCampos}
        cantidad={CANTIDAD_CAMPOS}
        juega="Viernes por medio"
      />
      <Fotter />
      <Alert
        openAlert={alertCloseGame}
        setOpenAlert={setAlertCloseGame}
        closeGame
        title="Loto a cerrado"
        message="Este juego a cerrado te deseamos la mejor suerte!"
      />
    </Grid>
  );
}
