import React, { useEffect, useState } from "react";
import { Grid, Card, makeStyles, Button } from "@material-ui/core";
import imagen2 from "../../src/assets/iconM.png";
import logo_loto_plus from "../../src/assets/iconlotoplus.png";
import logo_loto from "../../src/assets/iconloto.png";
import logo_brinco from "../../src/assets/iconbrinco.png";
import logo_quinela from "../../src/assets/iconQP.png";
import logo_quinela_plus from "../../src/assets/iconQPlus.png";
import imagen6 from "../../src/assets/iconprode.png";
import Header from "../components/Header";
import Fotter from "../components/Fotter";
import BannerLogin from "../components/BannerLogin";
import BannerMisJugadas from "../components/BannerMisJugadas";
import styles from "../common/styles";
import { useHistory } from "react-router-dom";
import Api from "../config/Api";
import { v4 as uuidv4 } from "uuid";
import { useSelector } from "react-redux";
import { agregarJugada } from "../actions/carrito";
import { useDispatch } from "react-redux";
import { dataQuinelaToObj } from "../common/helper";
import Alert from "../components/Alert";
import SyncLoader from "react-spinners/SyncLoader";

export default function MisJugadas() {
  const classes = useStyles();
  const history = useHistory();
  const style = styles();
  const dispatch = useDispatch();
  const [jugadas, setJugadas] = useState([]);
  const [paginado, setPagindo] = useState(null);
  

  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("La jugada se agrego al carrito.");
  const [total, setTotal] = useState(0);
  const { token, user_id } = useSelector((state) => state.auth);

  const [url, setUrl] = useState(`/get_lastest_plays/${user_id}`);

  const getNombreILogo = nombre => {

    switch (nombre) {
      case 'brinco':
        return {nombre: "Brinco",logo: logo_brinco};
      case 'loto':
        return {nombre: "Loto",logo: logo_loto};  
      case 'quinela':
        return {nombre: "Quinela",logo: logo_quinela};
      case 'quinela_plus':
        return {nombre: "Quinela Plus",logo: logo_quinela_plus};  
      case 'loto_plus':
        return {nombre: "Loto Plus",logo: logo_loto_plus};  
      default:
        return {nombre: nombre,logo: ""}; 
    }

  };

  useEffect(() => {
    /*setTotal(100);
    setJugadas([{
        ident:'brinco',
        nombre: "Brinco",
        logo: logo_brinco,
        numero: "11 39 7 10 12 1 13 22",
        jackpot: "",
        elem: {
          "id": 1001,
          "jugada_id": 103,
          "number_one": 11,
          "number_two": 39,
          "number_three": 7,
          "number_four": 10,
          "number_five": 12,
          "number_six": 1,
          "number_seven": 13,
          "number_eight": 22,
          "monto": 40,
          "status": 1,
          "created_at": "2021-04-19T12:49:57.000000Z",
          "updated_at": "2021-04-19T12:49:57.000000Z"
        },
        fecha: new Date(),
    },{
      ident:'brinco',
      logo: logo_brinco,
      nombre: "Brinco",
      numero: "13 37 7 10 15 2 13 21",
      jackpot: "",
      elem: {
        "id": 1001,
        "jugada_id": 103,
        "number_one": 12,
        "number_two": 37,
        "number_three": 7,
        "number_four": 10,
        "number_five": 15,
        "number_six": 2,
        "number_seven": 13,
        "number_eight": 21,
        "monto": 60,
        "status": 1,
        "created_at": "2021-04-19T12:49:57.000000Z",
        "updated_at": "2021-04-19T12:49:57.000000Z"
      },
      fecha: new Date(),
  },{
      ident:'loto',
      logo: logo_loto,
      nombre: "Loto",
      numero: "32 26 6 38 33 23",
      jackpot: "7 1",
      elem: {
        created_at: "2021-05-07T13:22:40.000000Z",
        id: 1003,
        jackpot_one: 7,
        jackpot_two: 1,
        jugada_id: 105,
        monto: 70,
        number_five: 33,
        number_four: 38,
        number_one: 32,
        number_six: 23,
        number_three: 6,
        number_two: 26,
        status: 1,
        tipo_jugada: "Tradicional",
        updated_at: "2021-05-07T13:22:40.000000Z",
      },
      fecha: new Date(),
  },
]);*/
    setLoading(true);
    Api.get({url: url}, token).then((result)=>{
      console.log(result);
      if(result.message !== undefined){
        setJugadas([]);
      }else{
        //pagimnado

        let prev_url = result.prev_page_url != null ? result.prev_page_url.split("api/")[1] : null;
        let next_url = result.next_page_url != null ? result.next_page_url.split("api/")[1] : null;

        let _paginado = {
          from: result.from,
          to: result.to,
          total: result.total,
          current_page: result.current_page,
          per_page: result.per_page,
          next_page_url: `/${next_url}`,
          prev_page_url: `/${prev_url}`,
        };
        //console.log("p",_paginado);
        setPagindo(_paginado);
        const all_jugadas = [];
        let tempTotal = 0;
        result.jugadas.forEach((item)=>{
          //console.log(item);
          tempTotal += item.monto_total;
          for(const i in item.detalles) {
              item.detalles[i].forEach((it)=>{
                let jug = getNombreILogo(i);
                //let elem = it;
                let numero = "";
                let jackpot = "";
                if(i === 'quinela'){
                  let elemInterno = it.detalles;
                  for(const k in elemInterno) {
                    for(const y in elemInterno[k]){
                      if( y === 'numero'){
                        numero += ` ${elemInterno[k][y]}`;
                      }
                    }
                  }
                  
                  
                }else{
                  for(let y in it){
                    if(y.includes('number')){
                      numero += ` ${it[y]}`;
                    }
                  }
                  if(i === "loto"){
                    for(const y in it){
                      if(y.includes('jackpot')){
                        jackpot += ` ${it[y]}`;
                      }
                    }
                  }

                }
                jug.ident = i;
                jug.elem = it;
                jug.numero = numero;
                jug.jackpot = jackpot;
                jug.fecha = it.created_at === undefined ? new Date(item.created_at) : new Date(it.created_at);
                //console.log(jug);
                all_jugadas.push(jug);

              });
              //console.log("fecha",item.created_at);
          };
        });
        setTotal(total+tempTotal);
        setJugadas([...all_jugadas]);
      }
      setLoading(false);
    }).catch(err=>{
      setLoading(false);
    });
  }, [url]);

  const repetir = (item) => {

    let elem = item.elem;
    let campo_index = 0;
    let juego = [];
    
    let campos = {};
    let total = elem.monto;
    let tipo = 2;
    for(const y in elem){
      if(y.includes('number')){
        campos['campo'+campo_index++] = elem[y];
      }
    }
    if(item.ident === "loto"){
      for(const y in elem){
        if(y.includes('jackpot')){
          campos['campo'+campo_index++] = elem[y];
        }
      }
    }
    

    if(item.ident === "quinela"){
      juego = dataQuinelaToObj(elem);
      let redoblona = false;
      campos = elem.detalles.map(e =>{
        redoblona = ! redoblona ? e.redoblona === 1 : false;
        return {
          ...e, tipo: e.redoblona === 1 ? redoblona ? "redoblona2" : "redoblona" : "2"
        }
      });
      total = elem.monto_total;
      tipo = 1;
    }
    //console.log(campos);
    dispatch(
      agregarJugada({
        id: uuidv4() ,
        nombre: item.nombre,
        jugadas: campos,
        juego,
        total,
        posicion: elem.posicion,
        entero: elem.entero,
        tipo,
        tipoJugada: elem.tipo_jugada,
        Logo: item.logo,
      })
    );
    setOpen(true);
  }

  return (
    <Grid container direction="row" justify="center">
      <Header />
      <Card elevation={2} className={classes.root}>
        <Grid container item xs={12} className={classes.cardGridContainer}>
          <Grid item xs={12} className={classes.root1}>
            <BannerLogin imagen={imagen2} name="Mis Jugadas" />
          </Grid>
          {loading && 
            <div style={{marginTop:"5%"}}>
              <SyncLoader color={'#e97400'} loading={loading} /*css={override}*/ size={25} />
            </div>
          }
          {jugadas.map((item,index)=>
            (
            <BannerMisJugadas
              key={index}
              imagen={item.logo}
              tittle={item.nombre}
              numero={item.numero}
              jackpot={item.jackpot}
              precio={item.nombre === "Quinela" ? item.elem.monto_total : item.elem.monto}
              texto2={item.fecha.toLocaleDateString()}
              repetir={()=>repetir(item)}
            />)
          )}
          { (!loading && jugadas.length === 0) && 
            <Grid container item xs={12} style={{ display: "flex",height: 'max-content' }}>
              <Grid item xs={12} style={{ alignSelf: "center" }}>
              <div>
                <div  style={{ alignSelf: "center", textAlign: 'center', fontSize: 'x-large' }} variant="subtitle1" className={classes.textTitle}>No hay jugadas</div>
              </div>
            </Grid>
            </Grid>
          }
        
        </Grid>
      </Card>
      <Grid container item xs={12}>
        <div className={classes.totalText}>Total ${total}</div>
      </Grid>
      {paginado &&
      <Grid container item xs={12} style={{marginTop:"2%"}}>
          <Grid container className={`${classes.paginate}`} item xs={4} style={{flex:1}}>
            <Button
              variant="contained"
              size="small"
              className={`${classes.paginateBton}`}
              disabled={paginado.prev_page_url === "/null"}
              onClick={() => setUrl(paginado.prev_page_url) }
            >
              Anterior
            </Button>
          </Grid>
          <Grid container className={`${classes.paginate}`} item xs={4}>
            <Button
              variant="contained"
              size="small"
              className={`${classes.paginateBton}`}
            >
              {paginado.current_page}
            </Button>
          </Grid>
          <Grid container className={`${classes.paginate}`} item xs={4}>
            <Button
              variant="contained"
              size="small"
              disabled={paginado.next_page_url === "/null"}
              className={`${classes.paginateBton}`}
              onClick={() => setUrl(paginado.next_page_url) }
            >
              Siguiente
            </Button>
          </Grid>
        </Grid>
      }

      <Grid container item xs={12} className={`${classes.containerButtons} ${style.finalMargin}`}>
        <Button
          variant="contained"
          size="large"
          className={`${classes.button} ${style.buttonText} ${style.buttonColorSecondary}`}
          onClick={() => { history.goBack() }}
        >
          Volver
        </Button>
        {/*<Button
          variant="contained"
          size="large"
          className={`${classes.button} ${style.buttonText} ${style.buttonColorPrimary}`}
        >
          Pagar
        </Button>*/}
      </Grid>
      <Alert
        openAlert={open}
        setOpenAlert={setOpen}
        title={"Operación satisfactoria"}
        /*closeGame={true}*/
        message={message}
      />
      <Fotter />
    </Grid>
  );
}
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    height: 500,
    overflowY: "scroll",
    borderRadius: 10,
  },
  root1: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    marginTop: "5%",
    marginLeft: "3%",
  },
  cardGridContainer: {
    width: "100%",
    justifyContent: "center",
  },
  totalText: {
    width: "100%",
    textAlign: "right",
    fontFamily: theme.fonts.primary,
    fontSize: "100%",
    fontWeight: "bold",
    marginRight: "5%",
    marginTop: "3%",
  },
  containerButtons: {
    alignContent: "center",
    justifyContent: "space-between",
  },
  button: {
    marginTop: "8%",
    marginBottom: "2%",
    borderRadius: 7,
    width: "45%",
  },
  paginateBton: {
    backgroundColor: "#b20000d9",
    color: "white"
  },
  paginate: {
    justifyContent: "center",
  }
}));
