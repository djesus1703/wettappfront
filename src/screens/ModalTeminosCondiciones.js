import React, { useState } from "react";
import { Grid, Card, Modal, Button } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import HighlightOff from "@material-ui/icons/HighlightOff";
import { useHistory } from "react-router-dom";
import styles from "../common/styles";

export default function ModalTeminosCondiciones() {
  const style = styles();
  const history = useHistory();
  const [open, setOpen] = useState(true);
  const handleClose = () => {
    setOpen(false);
    history.goBack();
  };

  return (
    <Grid container direction="row" justify="center">
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className={style.alertGrid}
        >
          <Card elevation={5} className={style.card}>
            <div className={style.buttonCloseModal}>
              <IconButton
                aria-label="delete"
                size="small"
                onClick={handleClose}
              >
                <HighlightOff fontSize="small" />
              </IconButton>
            </div>

            <div style={{ margin: "5%" }}>
              <h4 className={style.titleModal}>Terminos y condiciones</h4>
            </div>

            <Button
              variant="contained"
              className={`${style.buttonText} ${style.buttonColorPrimary} ${style.buttonAlert}`}
              size="large"
              onClick={() => {
                history.push("/");
              }}
            >
              Continuar
            </Button>
          </Card>
        </Grid>
      </Modal>
    </Grid>
  );
}
