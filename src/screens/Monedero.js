import React from "react";
import { Grid, makeStyles } from "@material-ui/core";
import imagen1 from "../../src/assets/iconQP.png";
import Header from "../components/Header";
import BottomFooter from "../components/BottomFooter";
import { useHistory } from "react-router-dom";

export default function Monedero() {
  const history = useHistory();
  const classes = useStyles();
  return (
    <Grid container direction="row" justify="center">
      <Header />
      <Grid container item xs={12}>
        <Grid
          container
          item
          xs={3}
          className={classes.root1}
          onClick={() => {
            history.push("/Quiniela");
          }}
        >
          <BottomFooter
            imagen={imagen1}
            tittle8="Quiniela"
            tittle10="Provincia"
          />
        </Grid>
      </Grid>

      <BottomFooter />
    </Grid>
  );
}
const useStyles = makeStyles((theme) => ({
  root1: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    marginTop: "5%",
    marginLeft: "5%",
  },
}));
