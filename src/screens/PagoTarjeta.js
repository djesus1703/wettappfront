import React, { useState, useEffect } from "react";
import {
  Grid,
  Input,
  makeStyles,
  Card,
  Modal,
  IconButton,
  Button,
} from "@material-ui/core";
import Cards from "react-credit-cards";
import "react-credit-cards/es/styles-compiled.css";
import HighlightOff from "@material-ui/icons/HighlightOff";
import Header from "../components/Header";
import { useSelector, useDispatch } from "react-redux";
import Fotter from "../components/Fotter";
import { VerifiedUser } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import styles from "../common/styles";
import Api from "../config/Api";
import { getDataFromCarrito } from "../common/helper";
import { eliminarJugada } from "../actions/carrito";

export default function PagoTarjeta(props) {
  const history = useHistory();
  const classes = useStyles();
  const style = styles();
  const [open, setOpen] = useState(false);
  const [cvc, setCvc] = useState("");
  const [expiry, setExpiry] = useState("");
  const [focus, setFocus] = useState("");
  const [name, setName] = useState("");
  const [number, setNumber] = useState("");
  const [total, setTotal] = useState(0);
  const dispatch = useDispatch();

  const { carrito } = useSelector((state) => state);
  const { token, cliente_id, agencia, user_id } = useSelector((state) => state.auth);

  const handleInputFocus = (e) => {
    setFocus(e.target.name);
  };

  const handleInputChange = (e) => {
    const { value } = e.target;
    setNumber(value);
  };

  const handlePagar = async () => {

    const data = {
      url: `/jugada`,
      data: {
        cliente_id: cliente_id,
        agencia_id: agencia.agencia_id,
        monto_total: total,
      }
    };
    let jugadasDelete = [];
    Api.post(data, token).then((res)=>{
      carrito.forEach(element => {
        const sendDataJugada = getDataFromCarrito(element);
        const data = {
          url: sendDataJugada.url,
          data: {...sendDataJugada,jugada_id: res.id},
        }
        Api.post(data,token).then(res=>{
          jugadasDelete.push(element.id)
          console.log("jugada ok",res)
        }).catch(err=>{
          console.log("error jugada",err)
        })
      });
    }).then((res)=>{
      console.log("jugadas de",jugadasDelete);
      const jugadasFiltradas = carrito.filter((jug) => jugadasDelete.indexOf(jug.id) !== -1);
      dispatch(eliminarJugada(jugadasFiltradas));
    }).catch((err)=>{
      alert(Api.responseStatus(err.response, err.response.status).text)
    });
    setOpen(true);
  }


  useEffect(() => {
    const auxTotal = carrito.reduce((total,item) => total + item.total,0);
    setTotal(auxTotal);
    console.log(carrito)
  }, [carrito]);

  return (
    <Grid container direction="row" justify="center">
      <Header />
      <Card elevation={0} className={classes.root}>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{ margin: "2%" }}
        >
          <div id="PaymentForm">
            <Cards
              cvc={cvc}
              expiry={expiry}
              focused={focus}
              name={name}
              number={number}
            />
            <form style={{}}>
              <Grid item xs={12} style={{ marginTop: "5%" }}>
                <Input
                  disableUnderline={true}
                  type="tel"
                  name="number"
                  placeholder="N° Tarjeta"
                  onChange={handleInputChange}
                  className={classes.inputs}
                />
              </Grid>
              <Grid item xs={12} style={{ marginTop: "5%" }}>
                <Input
                  disableUnderline={true}
                  type="text"
                  name="name"
                  placeholder="Nombre y Apellidos"
                  onChange={(e) => {
                    setName(e.target.value);
                  }}
                  className={classes.inputs}
                />
              </Grid>
              <Grid container item justify="space-between">
                <Grid item xs={8} style={{ marginTop: "5%" }}>
                  <Input
                    disableUnderline={true}
                    type="tel"
                    name="expiry"
                    placeholder="Fecha Vencimiento"
                    onChange={(e) => {
                      setExpiry(e.target.value);
                    }}
                    className={classes.inputs}
                  />
                </Grid>
                <Grid item xs={3} style={{ marginTop: "5%" }}>
                  <Input
                    disableUnderline={true}
                    type="tel"
                    name="cvc"
                    placeholder="CVC"
                    onChange={(e) => {
                      setCvc(e.target.value);
                    }}
                    onFocus={handleInputFocus}
                    className={classes.inputs}
                  />
                </Grid>
                <Grid item xs={12} style={{ marginTop: "5%" }}>
                  <Input
                    disableUnderline={true}
                    type="number"
                    name="dni"
                    placeholder=" Tu DNI"
                    onFocus={handleInputFocus}
                    className={classes.inputs}
                  />
                </Grid>
              </Grid>
              <Grid item xs={12} className={`${classes.gridButtonContainer} ${style.finalMargin}`}>
                <Button
                  variant="contained"
                  className={`${style.buttonColorTertiary} ${style.button} ${style.buttonText}`}
                  size="small"
                  onClick={handlePagar}
                >
                  Pagar Ahora
                </Button>
              </Grid>
            </form>
          </div>
        </Grid>
      </Card>
      <Modal
        open={open}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className={style.alertGrid}
        >
          <Card elevation={5} className={style.alertCard}>
            <div className={style.buttonCloseModal}>
              <IconButton
                aria-label="delete"
                size="small"
                onClick={() => {
                  setOpen(false);
                }}
              >
                <HighlightOff fontSize="small" />
              </IconButton>
            </div>
            <IconButton
              className={classes.containerIconModal}
              onClick={() => {
                history.push("/");
              }}
            >
              <VerifiedUser />
            </IconButton>
            <h1>Gracias..¡Suerte!</h1>
          </Card>
        </Grid>
      </Modal>
      <Fotter />
    </Grid>
  );
}
const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    borderRadius: 10,
  },
  containerIconModal: {
    margin: "4%",
    textAlign: "center",
    "& svg": {
      fontSize: "400%",
      color: theme.colors.darkGreen,
    },
  },
  gridButtonContainer: {
    marginTop: "6%",
  },
  inputs: {
    width: "100%",
    height: "100%",
    backgroundColor: theme.colors.lightGray,
    borderRadius: 7,
    padding: "3%",
  },
}));
