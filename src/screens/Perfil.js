import React, { Fragment, useState } from "react";
import {
  Grid,
  Input,
  makeStyles,
  Card,
  Box,
  Modal,
  Button,
  Divider,
} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import BeenhereTwoTone from "@material-ui/icons/BeenhereTwoTone";
import Edit from "@material-ui/icons/Edit";
import HighlightOff from "@material-ui/icons/HighlightOff";
import { useHistory } from "react-router-dom";
import Header from "../components/Header";
import Fotter from "../components/Fotter";
import styles from "../common/styles";
import { useSelector } from "react-redux";

export default function Perfil() {
  const history = useHistory();
  const classes = useStyles();
  const style = styles();
  const [open, setOpen] = useState({
    nombre: false,
    apellido: false,
    contacto: false,
    email: false,
    passwod: false,
  });
  const { 
    token,
    cliente_id,
    agencia,
    user_name,
    tel,
    user,
    user_id
  } = useSelector((state) => state.auth);
  const [nameUser, setNameUser] = useState(user_name);
  const [lastNameUser, setLastNameUser] = useState("Bustamante");
  const [contactUser, setContactUser] = useState(tel);
  const [emailUser, setEmailUser] = useState(user);
  const [passlUser, setPassUser] = useState("*******");
  const handleOpen = (key) => {
    setOpen({ ...open, [key]: true });
  };

  const userData = [
    { nameInput: "nombre", value: nameUser, setState: setNameUser },
    { nameInput: "apellidos", value: lastNameUser, setState: setLastNameUser },
    { nameInput: "contacto", value: contactUser, setState: setContactUser },
    { nameInput: "email", value: emailUser, setState: setEmailUser },
    { nameInput: "password", value: passlUser, setState: setPassUser },
  ];

  const editData = (key, value, setState) => (
    <Modal
      open={open[key] || false}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        className={classes.gridModal}
      >
        <Card elevation={5} className={classes.cardModal}>
          <div className={style.buttonCloseModal}>
            <IconButton
              aria-label="delete"
              size="small"
              onClick={() => {
                setOpen({ ...open, [key]: false });
              }}
            >
              <HighlightOff fontSize="small" />
            </IconButton>
          </div>

          <div className={classes.inputModal}>
            <Input
              defaultValue={value}
              onChange={(e) => setState(e.target.value)}
            />
          </div>

          <div>
            <Button
            className={`${style.buttonAlert} ${style.buttonColorTertiary}`}
              onClick={() => {
                setOpen({ ...open, [key]: false });
              }}
            >
              <div className={style.buttonText}>Aceptar</div>
            </Button>
          </div>
        </Card>
      </Grid>
    </Modal>
  );

  return (
    <Grid container direction="row" justify="center">
      <Header />
      <Card elevation={0} className={classes.root}>
        <Grid container direction="row" justify="center" alignItems="center">
          <div className={classes.titleContent}>Datos Personales</div>

          <Grid item xs={12}>
            <Box display="flex" alignItems="center">
              <Box p={1} flexGrow={1}>
                <div className={`${classes.text} ${classes.title}`}>
                  Agencia:
                </div>
              </Box>
              <Box display="flex" p={1}>
                <div className={`${classes.textContent} ${classes.text}`}>
                  {agencia.nombre}
                </div>
              </Box>
              <Box display="flex" p={1}>
                <div>
                  <IconButton component="span">
                    <BeenhereTwoTone
                      fontSize="small"
                      className={classes.colorIconCheck}
                    />
                  </IconButton>
                </div>
              </Box>
            </Box>
            <Divider />
          </Grid>
          {userData.map((item) => (
            <Fragment key={item.nameInput}>
              <Grid item xs={12}>
                <Box display="flex" alignItems="center">
                  <Box p={1} flexGrow={1}>
                    <div className={`${classes.text} ${classes.title}`}>
                      {item.nameInput}:
                    </div>
                  </Box>
                  <Box display="flex" p={1}>
                    <div className={`${classes.textContent} ${classes.text}`}>
                      {item.value}
                    </div>
                  </Box>
                  <Box display="flex" p={1}>
                    <div>
                      <IconButton
                        component="span"
                        onClick={() => {
                          handleOpen(item.nameInput);
                        }}
                      >
                        <Edit fontSize="small" />
                      </IconButton>
                    </div>
                  </Box>
                </Box>
                <Divider />
              </Grid>
              {editData(item.nameInput, item.value, item.setState)}
            </Fragment>
          ))}
        </Grid>
      </Card>
      <Grid item xs={12} className={style.finalMargin}>
        <Button
          variant="contained"
          className={`${style.button} ${style.buttonColorPrimary}`}
          size="large"
          onClick={() => {
            history.goBack();
          }}
        >
          <div className={style.buttonText}>Volver al menú anterior</div>
        </Button>
      </Grid>

      <Fotter />
    </Grid>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    height: "90%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    marginBottom: "5%",
    borderRadius: 10,
    paddingTop: "10%",
  },
  titleContent: {
    fontSize: "130%",
    fontFamily: theme.fonts.primary,
    fontWeight: "bold",
    textAlign: "center",
    marginBottom: "5%",
  },
  text: {
    fontSize: "100%",
    fontFamily: theme.fonts.primary,
  },
  title: {
    textTransform: "capitalize",
  },
  textContent: {
    fontWeight: "bold",
  },
  colorIconCheck: {
    color: theme.colors.darkGreen,
  },
  gridModal: {
    width: "100%",
    height: "100%",
  },
  cardModal: {
    width: "70%",
    backgroundColor: theme.colors.white,
    borderRadius: 10,
    textAlign: "center",
  },
  inputModal: {
    width: "100%",
    height: "100%",
    borderRadius: 5,
    marginBottom: 20,
  },
}));
