import React from "react";
import { Card, Grid, makeStyles } from "@material-ui/core";

import { ReactComponent as Logo7 } from "../assets/resultados.svg";

import Header from "../components/Header";
import Fotter from "../components/Fotter";
import ListItemJuegos from "../components/ListItemJuegos";
import HeaderItemJuegos from "../components/HeaderItemJuegos";

export default function Pozos() {
    const classes = useStyles();

    return (
        <Grid container direction="row" justify="center">
            <Header />
            <Card elevation={0} className={classes.root}>
                <HeaderItemJuegos
                    image={<Logo7 className={classes.svg11} />}
                    title="Pozos"
                />
                <ListItemJuegos
                    title="Pozos"
                    ruta="pozos"
                />
            </Card>
            <Fotter />
        </Grid>
    )
}

const useStyles = makeStyles((theme) => ({
    root: {
        width: "100%",
        zIndex: 1,
        backgroundColor: theme.colors.white,
        alignContent: "center",
        justifyContent: "center",
        borderRadius: 10,
        marginBottom: "15%",
    },
    svg11: {
        width: "70%",
        height: "70%",
        marginTop: "10%",
    },
}))