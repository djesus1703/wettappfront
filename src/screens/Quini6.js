import React, { useEffect, useState } from "react";
import { Grid } from "@material-ui/core";
import { ReactComponent as Logo } from "../assets/quini_6.svg";
import Header from "../components/Header";
import Fotter from "../components/Fotter";
import moment from "moment";
import Alert from "../components/Alert";
import JuegoNumeros from "../components/JuegoNumeros";

const PRECIO_INICIAL_JUEGO = 80;
const CANTIDAD_CAMPOS = 6;
const PRECIO_TRADICIONAL = 50;
const PRECIO_DESQUITE = 65;
const PRECIO_SASE_O_SALE = 80;

const CIERRE_JUEGO = [
  {
    dia: "Sunday",
  },
  {
    dia: "Wednesday",
    hora: "18:40",
  },
  /*{
    dia: "Saturday",
    hora: "20:10",
  },*/
];

export default function Quini6() {
  const [precio, setPrecio] = useState(PRECIO_INICIAL_JUEGO);
  const [alertCloseGame, setAlertCloseGame] = useState(false);
  const [campos, setCampos] = useState({
    campo0: "",
    campo1: "",
    campo2: "",
    campo3: "",
    campo4: "",
    campo5: "",
  });

  useEffect(() => {
    const date = moment().format("HH:mm");
    const day = moment().format("dddd");

    CIERRE_JUEGO.forEach((item) => {
      if (date >= item.hora && day === item.dia) {
        setAlertCloseGame(true);
      } else if (day === "Sunday") {
        setAlertCloseGame(true);
      }
    });
  }, []);

  const rango = { inicio: 0, fin: 45 };

  return (
    <Grid container direction="row" justify="center">
      <Header />
      <JuegoNumeros
        Logo={Logo}
        nombre="Quini6"
        rango={rango}
        valorJugada={precio}
        campos={campos}
        setCampos={setCampos}
        cantidad={CANTIDAD_CAMPOS}
        juega="Miercoles y Domingos"
        modificarValor={true}
        setValorJugada={setPrecio}
        precioSale={PRECIO_SASE_O_SALE}
        precioTradicional={PRECIO_TRADICIONAL}
        preciosDesquite={PRECIO_DESQUITE}
      />
      <Fotter />
      <Alert
        openAlert={alertCloseGame}
        setOpenAlert={setAlertCloseGame}
        closeGame
        title="Quinela a cerrado"
        message="Este juego a cerrado te deseamos la mejor suerte!"
      />
    </Grid>
  );
}
