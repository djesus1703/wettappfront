import React, { useEffect, useState } from "react";
import {
  Grid,
  Button,
  IconButton,
  makeStyles,
  Card,
  Modal,
} from "@material-ui/core";
import moment from "moment";
import HighlightOff from "@material-ui/icons/HighlightOff";
import { ErrorOutline } from "@material-ui/icons";
import { ReactComponent as Logo } from "../assets/prestador1.svg";
import Header from "../components/Header";
import Fotter from "../components/Fotter";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import Banner1 from "../components/Banner1";
import JuegoProvincia from "../components/JuegoProvincia";
import Alert from "../components/Alert";
import styles from "../common/styles";
import { useSelector } from "react-redux";
import { checkQuinela } from "../actions/jugadas";

const HORA_CIERRE_JUEGO = "16:00";
const DIAS_CIERRE_JUEGO = ["Monday", "Sunday"];

export default function Quiniela() {
  const history = useHistory();
  const classes = useStyles();
  const style = styles();
  const dispatch = useDispatch();
  
  const [openAlert, setOpenAlert] = useState(false);
  const [alertCloseGame, setAlertCloseGame] = useState(false);
  const [open, setOpen] = useState(false);
  const [indicador2, setIndicador2] = useState([true, true, true, true]);
  const [indicador, setIndicador] = useState(false);
  const [count, setCount] = useState(0);
  const [juego, setJuego] = useState([
    {
      titulo: "Provincia",
      select: false,
      dato: [
        {
          selecte: false,
          view: true,
          cierre: "11:30",
          disable: false,
          nombre: "La primera",
        },
        {
          selecte: false,
          view: true,
          cierre: "14:00",
          disable: false,
          nombre: "matutina",
        },
        {
          selecte: false,
          view: true,
          cierre: "17:00",
          disable: false,
          nombre: "vespertina",
        },
        {
          selecte: false,
          view: true,
          cierre: "20:30",
          disable: false,
          nombre: "nocturna",
        },
      ],
    },
    {
      titulo: "Nacional",
      select: false,
      dato: [
        {
          selecte: false,
          view: true,
          cierre: "11:30",
          disable: false,
          nombre: "La primera",
        },
        {
          selecte: false,
          view: true,
          cierre: "14:00",
          disable: false,
          nombre: "matutina",
        },
        {
          selecte: false,
          view: true,
          cierre: "17:00",
          disable: false,
          nombre: "vespertina",
        },
        {
          selecte: false,
          view: true,
          cierre: "20:30",
          disable: false,
          nombre: "nocturna",
        },
      ],
    },
    {
      titulo: "Montevideo",
      select: false,
      dato: [
        {
          selecte: false,
          view: true,
          cierre: "11:30",
          disable: false,
          nombre: "La primera",
        },
        {
          selecte: false,
          view: true,
          cierre: "14:00",
          disable: false,
          nombre: "matutina",
        },
        {
          selecte: false,
          view: true,
          cierre: "17:00",
          disable: false,
          nombre: "vespertina",
        },
        {
          selecte: false,
          view: true,
          cierre: "20:30",
          disable: false,
          nombre: "nocturna",
        },
      ],
    },
    {
      titulo: "Santa fe",
      select: false,
      dato: [
        {
          selecte: false,
          view: true,
          cierre: "11:30",
          disable: false,
          nombre: "La primera",
        },
        {
          selecte: false,
          view: true,
          cierre: "14:00",
          disable: false,
          nombre: "matutina",
        },
        {
          selecte: false,
          view: true,
          cierre: "17:00",
          disable: false,
          nombre: "vespertina",
        },
        {
          selecte: false,
          view: true,
          cierre: "20:30",
          disable: false,
          nombre: "nocturna",
        },
      ],
    },
    {
      titulo: "Córdoba",
      select: false,
      dato: [
        {
          selecte: false,
          view: true,
          cierre: "11:30",
          disable: false,
          nombre: "La primera",
        },
        {
          selecte: false,
          view: true,
          cierre: "14:00",
          disable: false,
          nombre: "matutina",
        },
        {
          selecte: false,
          view: true,
          cierre: "17:00",
          disable: false,
          nombre: "vespertina",
        },
        {
          selecte: false,
          view: true,
          cierre: "20:30",
          disable: false,
          nombre: "nocturna",
        },
      ],
    },
    {
      titulo: "Entre Ríos",
      select: false,
      dato: [
        {
          selecte: false,
          view: true,
          cierre: "11:30",
          disable: false,
          nombre: "La primera",
        },
        {
          selecte: false,
          view: true,
          cierre: "14:00",
          disable: false,
          nombre: "matutina",
        },
        {
          selecte: false,
          view: true,
          cierre: "17:00",
          disable: false,
          nombre: "vespertina",
        },
        {
          selecte: false,
          view: true,
          cierre: "20:30",
          disable: false,
          nombre: "nocturna",
        },
      ],
    },
  ]);
  //console.log(history);
  const quinela = useSelector((state) => state.jugadas.quinela);
  //console.log("state",quinela);
  useEffect(() => {
    const date = moment().format("HH:mm");
    const day = moment().format("dddd");
    const cierreJuego = [];
    if(quinela !== undefined){
      let count = 0;
      quinela.forEach((item) => {
        item.dato.forEach((dato) => {
          dato.disable = date >= dato.cierre ? true : false;
          if(dato.selecte){
            count++;
          }
        });
        cierreJuego.push(item);
        setCount(count);
      });
      setJuego(cierreJuego);
    }else{
      juego.forEach((item) => {
        item.dato.forEach((dato) => {
          dato.disable = date >= dato.cierre ? true : false;
        });
        cierreJuego.push(item);
      });
      setJuego(cierreJuego);
    }
    
    DIAS_CIERRE_JUEGO.forEach((days) => {
      if (date >= HORA_CIERRE_JUEGO && day === days) {
        setAlertCloseGame(true);
        setOpenAlert(true);
      }
    });
  }, []);

  useEffect(() => {
    dispatch(checkQuinela(juego))
    /*return () => {
      cleanup
    }*/
  }, [juego])

  const clickColor = () => {
    let temp3 = [];
    for (let index in juego) {
      let x = 0;
      for (let index3 in juego[index].dato) {
        if (juego[index].dato[index3].selecte) {
          juego[index].select = true;
        } else {
          x++;
        }
      }

      if (x === juego[index].dato.length) {
        juego[index].select = false;
      }
      temp3.push(juego[index]);
    }
    setJuego(temp3);
  };

  const click2 = (juegop, data) => {
    juego[juegop].dato[data].selecte = juego[juegop].dato[data].disable
      ? false
      : !juego[juegop].dato[data].selecte;
    let temp2 = [];
    if(juego[juegop].dato[data].selecte){
      setCount(count + 1);
    }else{
      setCount(count - 1);
    }
    for (let index in juego) {
      temp2.push(juego[index]);
    }
    setJuego(temp2);
    clickColor();

    juego[juegop].dato[data].disable && setOpenAlert(true);
  };
  const click3 = (juegop, bandera) => {
    let ind = juego[juegop].dato;
    for (let index in ind) {
      ind[index].selecte = ind[index].disable ? false : bandera;
    }
    setJuego(juego);
    clickColor();
  };

  const click = (data, indi) => {
    let tempCount = 0;
    let temp = [];
    let temp_todas_disabled = false;
    for (let index in juego) {
      if (data === "ALL") {
        for (let index2 in juego[index].dato) {
          if(!juego[index].dato[index2].disable && !juego[index].dato[index2].selecte && indicador){
            tempCount++;
          }
          juego[index].dato[index2].selecte = juego[index].dato[index2].disable
            ? false
            : indicador;
        }
        if(!indicador){
          tempCount = 0;
        }
      } else {
        if(juego[index].dato[data].disable)
          temp_todas_disabled = true;
          if(!juego[index].dato[data].disable && !juego[index].dato[data].selecte && indi){
            tempCount++;
          }
          if(!juego[index].dato[data].disable && juego[index].dato[data].selecte && !indi){
            tempCount--;
          }
          juego[index].dato[data].selecte = juego[index].dato[data].disable
            ? false
            : indi;
          
      }
      temp.push(juego[index]);
    }
    if(data === "ALL" && tempCount === 0){
      setCount(0);
    } else {
      if(temp_todas_disabled){
        setOpenAlert(true);
      }
      setCount(count+tempCount);
    }
    
    setJuego(temp);
    clickColor();
    setIndicador(!indicador);
    indicador2[data] = !indicador2[data];
    setIndicador2(indicador2);
  };

  const jugadas = ["El primero", "Matutina", "Vespertina", "Nocturno"];

  return (
    <Grid container direction="row" justify="center">
      <Header />
      <Card elevation={0} className={classes.root}>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{ margin: "2%" }}
        >
          <Grid item xs={12}>
            <div
              style={{
                width: "100%",
                height: "100%",
                marginTop: "1%",
                marginBottom: "5%",
              }}
            >
              <Banner1
                imagen={<Logo className={style.logosSvg} />}
                name="Quiniela Provincia"
                tittle="Buenos Aires"
              />
            </div>
          </Grid>
          <Grid item xs={3} style={{ marginRight: "2%", marginBottom: "4%" }} />
          {jugadas.map((jugada) => (
            <Grid
              key={jugada}
              item
              xs={2}
              style={{ marginLeft: "1%", marginBottom: "4%" }}
            >
              <div className={classes.contenedorJugada}>
                <div className={classes.letra}>{jugada}</div>
              </div>
            </Grid>
          ))}
          {juego.length > 0 &&
            juego.map((item, index) => (
              <JuegoProvincia
                dato={item}
                key={index}
                juego={index}
                click={(juego, data) => {
                  click2(juego, data);
                }}
                click33={(juego, bandera) => {
                  click3(juego, bandera);
                }}
              />
            ))}
          <Grid item xs={3} style={{ marginRight: "1%" }} />
          {jugadas.map((jugada, index) => (
            <Grid key={jugada} item xs={2} style={{ marginLeft: "1%" }}>
              <div
                className={`${style.button} ${style.buttonColorPrimary}`}
                onClick={() => {
                  click(index, indicador2[index]);
                }}
              >
                <div className={classes.textAll}>Todas</div>
              </div>
            </Grid>
          ))}
        </Grid>
      </Card>
      <Grid container xs={12} >
            <div
              style={{
                width: "100%",
                marginTop: "2%",
                paddingLeft: "3%",
                fontFamily: "Roboto, sans-serif",
              }}
            >
              <label><strong>Cantidad seleccionada : {count}</strong></label>
            </div>
      </Grid>
      <Grid container item xs={12} className={`${classes.gridContainerButtons} ${style.finalMargin}`}>
        <Button
          variant="contained"
          size="large"
          className={`${classes.button} ${style.buttonColorSecondary}`}
          onClick={() => {
            click("ALL", indicador);
          }}
        >
          <div className={style.buttonText}>Seleccionar todas</div>
        </Button>
        <Button
          variant="contained"
          size="large"
          className={`${classes.button} ${style.buttonColorPrimary}`}
          onClick={() => {
            // let indicador = 0;
            /*for (let index in juego) {
              for (let index2 in juego[index].dato) {
                if (juego[index].dato[index2].selecte) {
                  indicador++;
                }
              }
            }*/
            if (count > 0) {
              dispatch(checkQuinela(juego))
              history.push({
                pathname: "/Quiniela1",
                state: { cantidad: count, juego },
              });
            } else {
              setOpen(true);
            }
          }}
        >
          <div className={classes.letra3}>Jugar!</div>
        </Button>
        <Alert
          openAlert={openAlert}
          closeGame={alertCloseGame}
          setOpenAlert={setOpenAlert}
          title={""}
          message={"Ya no puedes realizar esta jugada ya que a cerrado"}
        />
      </Grid>
      <Modal
        open={open}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className={style.alertGrid}
        >
          <Card elevation={5} className={style.alertCard}>
            <div className={style.buttonCloseModal}>
              <IconButton
                aria-label="delete"
                size="small"
                onClick={() => {
                  setOpen(false);
                }}
              >
                <HighlightOff fontSize="small" />
              </IconButton>
            </div>
            <div style={{ margin: "4%", textAlign: "center" }}>
              <IconButton
                onClick={() => {
                  setOpen(false);
                }}
              >
                <ErrorOutline className={style.alertErrorIcon} />
              </IconButton>

              <h1>¡Debes seleccionar al menos una casa de juego!</h1>
            </div>
          </Card>
        </Grid>
      </Modal>
      <Fotter />
    </Grid>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    borderRadius: 10,
  },
  letra: {
    fontFamily: theme.fonts.primary,
    fontSize: "60%",
  },
  textAll: {
    fontFamily: theme.fonts.primary,
    fontSize: "90%",
    textAlign: "center",
    color: theme.colors.white,
    marginTop: "2%",
    marginBottom: "3%",
  },
  contenedorJugada: {
    width: "100%",
    height: "100%",
    textAlign: "center",
    border: "1px solid",
    borderColor: theme.colors.lightGray,
    borderRadius: 7,
  },
  gridContainerButtons: {
    alignContent: "center",
    justifyContent: "space-around",
  },
  button: {
    alignContent: "center",
    justifyContent: "center",
    marginTop: "10%",
    marginBottom: "2%",
    borderRadius: 7,
    width: "45%",
  },
  letra2: {
    fontSize: "90%",
    color: theme.colors.white,
    textTransform: "none",
    fontFamily: theme.fonts.primary,
  },
  letra3: {
    fontFamily: theme.fonts.primary,
    fontSize: "150%",
    margin: "4%",
    color: theme.colors.white,
    fontWeight: "bold",
    textTransform: "none",
  },
}));
