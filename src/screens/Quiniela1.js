import React, { Fragment, useState, useEffect } from "react";
import {
  Grid,
  makeStyles,
  withStyles,
  Card,
  Modal,
  IconButton,
  InputAdornment,
  Input,
  Button,
  InputBase,
  MenuItem,
  Select,
  Radio,
} from "@material-ui/core";
import Cancel from "@material-ui/icons/Cancel";
import HighlightOff from "@material-ui/icons/HighlightOff";
import { useDispatch } from "react-redux";
import { ErrorOutline } from "@material-ui/icons";
import { v4 as uuidv4 } from "uuid";
import ListAltTwoTone from "@material-ui/icons/ListAltTwoTone";
import { ReactComponent as Logo } from "../assets/prestador1.svg";
import Header from "../components/Header";
import Fotter from "../components/Fotter";
import Logo7 from "../../src/assets/prestador1.svg";
import Banner5 from "../components/Banner5";
import Banner1 from "../components/Banner1";
import Divider from "@material-ui/core/Divider";
import { agregarJugada } from "../actions/carrito";
import styles from "../common/styles";
import { clearQuineela } from "../actions/jugadas";

export default function Quiniela1(props) {
  console.log(props);
  const classes = useStyles();
  const style = styles();
  // const [age3, setAge3] = useState(10);
  // const [age4, setAge4] = useState("");
  const [redoblonamostrar, setRedoblonamostrar] = useState(false);
  const [disabledBtnCarrito, setDisabledBtnCarrito] = useState(true);
  const [redoblonaNumero, setRedoblonaNumero] = useState({posicion: 1, tipo: "redoblona"});
  const [redoblonaNumero2, setRedoblonaNumero2] = useState({posicion: 5, tipo: "redoblona2"});
  const [open, setOpen] = useState(false);
  const [open22, setOpen22] = useState(false);
  
  // const handleChange3 = (event) => {
  //   setAge3(event.target.value);
  // };
  const handleChange5 = (event) => {
    setRedoblonaNumero({...redoblonaNumero, posicion: event.target.value});
  };
  const handleChangeRedoblona2Posicion = (event) => {
    setRedoblonaNumero2({...redoblonaNumero2, posicion: event.target.value});
  };
  // const handleChange4 = (event) => {
  //   setAge4(event.target.value);
  // };
  const handleChange2 = (event) => {
    // linea1.check = event.target.value
    // setLinea1(linea1);
  };

  const cambioa = () => {
    redoblonamostrar ? setRedoblonamostrar(false) : setRedoblonamostrar(true);
  };

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleOpen22 = () => {
    setOpen22(true);
  };
  const handleClose22 = () => {
    setOpen22(false);
  };
  const [linea1, setLinea1] = useState([
    {
      check: false,
      numero: "",
      posicion: 1,
      importe: "",
      tipo: 0,
    },
  ]);
  const [total, setTotal] = useState("0.00");
  const [vistaModal, setVistaModal] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    let disabled =  false;
    for (let index2 in linea1) {
      if(linea1[index2].importe === "" || linea1[index2].numero === "" ){
        disabled = true;
      }
    }
    if(redoblonamostrar){
      if(redoblonaNumero.numero === "" || redoblonaNumero.numero === undefined
       || redoblonaNumero.importe === "" || redoblonaNumero.importe === 0 || redoblonaNumero.importe === undefined ){
        disabled = true;
      }
      if(redoblonaNumero2.numero === "" || redoblonaNumero2.numero === undefined
       /*|| redoblonaNumero2.importe === "" || redoblonaNumero2.importe === 0 || redoblonaNumero2.importe === undefined */){
        disabled = true;
      }
    }
    if(linea1.length === 0 && !redoblonamostrar){
      disabled =  true;
    }
    setDisabledBtnCarrito(disabled);
    
  }, [redoblonaNumero,redoblonaNumero2, linea1, redoblonamostrar]);

  useEffect(() => {
    updateTotalRedoblona();
  }, [redoblonaNumero,redoblonaNumero2]);

  const repid = () => {
    let ultimajugada = vistaModal.length;
    if (ultimajugada > 0) {
      let ultima = ultimajugada - 1;
      setLinea1([
        {
          check: vistaModal[ultima].check,
          numero: vistaModal[ultima].numero,
          posicion: vistaModal[ultima].posicion,
          importe: vistaModal[ultima].importe,
          tipo: 2,
        },
      ]);
    }
  };
  const sumarTotal = (linea1) => {
    let total_aumentar = 0    
    for (let index2 in linea1) {
      if( linea1[index2].importe !== "" &&  linea1[index2].importe !== undefined){
        total_aumentar =
        total_aumentar + parseFloat(linea1[index2].importe);  
      }
      
    }
    /*total_aumentar =
      total_aumentar * props.location.state.cantidad;*/
    
    if(redoblonaNumero.importe !== undefined){
      total_aumentar += redoblonaNumero.importe;
    }
    /*if(redoblonaNumero2.importe !== undefined){
      total_aumentar += redoblonaNumero2.importe;
    }*/
    total_aumentar = total_aumentar * props.location.state.cantidad;
    setTotal(total_aumentar);
  }
  const updateTotal = (_redoblona) => {
    let total_aumentar = 0    
    for (let index2 in linea1) {
      if( linea1[index2].importe !== null &&  linea1[index2].importe !== undefined){
        total_aumentar =
          total_aumentar + parseFloat(linea1[index2].importe);
      }
    }
    total_aumentar =
      total_aumentar * props.location.state.cantidad;
    //console.log("hgh",redoblonaNumero2);
    let _total = total_aumentar + _redoblona * props.location.state.cantidad;
    
    setTotal(_total);
  }
  const updateTotalRedoblona = () => {
    let total_aumentar = 0    
    for (let index2 in linea1) {
      if( linea1[index2].importe !== null &&  linea1[index2].importe !== undefined){
        total_aumentar =
          total_aumentar + parseFloat(linea1[index2].importe);
      }
    }
    /*total_aumentar =
      total_aumentar * props.location.state.cantidad;*/
    let redoblona = redoblonaNumero.importe === "" ? 0 : redoblonaNumero.importe;
    //let redoblona2 = redoblonaNumero2.importe === "" ? 0 : redoblonaNumero2.importe;
    let _total = (total_aumentar + redoblona ) * props.location.state.cantidad;
    
    setTotal(_total);
  }
  const bajar = () => {
    let ultimajugada = linea1.length - 1;
    let copia = linea1[ultimajugada];
    let tamanoDeJugada = copia.numero.length;
    let cantidadRepetitiva = parseInt(tamanoDeJugada / 2);
    if (tamanoDeJugada <= 2) {
      setOpen22(true);
      return;
    }
    if (tamanoDeJugada > 4) {
      setOpen22(true);
      return;
    }
    if (tamanoDeJugada === 3 || tamanoDeJugada === 4) {
      let dup = linea1;

      for (let index = 0; index < cantidadRepetitiva; index++) {
        linea1.push({
          check: copia.check,
          numero: copia.numero.substring(index + 1),
          posicion: copia.posicion,
          importe: copia.importe,
          tipo: 4,
        });
      }
    }
    setLinea1([...linea1]);
    sumarTotal(linea1);
  };

  const jugarOtra = () => {
    linea1.push({
      check: false,
      numero: "",
      posicion: 1,
      importe: "",
      tipo: 3,
    });
    setDisabledBtnCarrito(true);
    setLinea1([...linea1]);
  };
  const buttons = [
    { name: "Redoblona", function: cambioa },
    { name: "Repetir", function: repid },
    { name: "Jugar otra", function: jugarOtra },
    { name: "Bajar", function: bajar },
  ];
  return (
    <Grid container direction="row" justify="center">
      <Header />
      <Card elevation={0} className={classes.root}>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{ margin: "2%" }}
        >
          <Grid item xs={12}>
            <div className={classes.containerBanner}>
              <Banner1
                imagen={<Logo className={classes.svg} />}
                name="Quiniela Provincia"
                tittle="Buenos Aires"
              />
            </div>
          </Grid>
          {buttons.map((button) => (
            <Grid
              key={button.name}
              item
              sm={2}
              style={{ margin: "0.1%", marginBottom: "5%" }}
            >
              <Button
                size="small"
                className={classes.gameButton}
                onClick={button.function}
              >
                {button.name}
              </Button>
            </Grid>
          ))}
          <Grid item sm={1} style={{ margin: "0.1%", marginBottom: "5%" }}>
            <IconButton
              onClick={() => {
                handleOpen();
              }}
            >
              <ListAltTwoTone fontSize="small" className={classes.iconList} />
            </IconButton>
          </Grid>

          <Grid container alignItems="center" style={{ marginBottom: "4%" }}>
            <Grid item xs={1} />
            <Grid item xs={3} style={{ marginRight: "0.5%", marginLeft: "1%" }}>
              <div
                className={`${style.alertGrid} ${style.textCenter} ${classes.textButton}`}
              >
                <h4>Número</h4>
              </div>
            </Grid>
            <Grid item xs={2} style={{ marginRight: "0.5%", marginLeft: "1%" }}>
              <div
                className={`${style.alertGrid} ${style.textCenter} ${classes.textButton}`}
              >
                <h4>Posición</h4>
              </div>
            </Grid>
            <Grid item xs={3} style={{ marginRight: "0.5%", marginLeft: "1%" }}>
              <div
                className={`${style.alertGrid} ${style.textCenter} ${classes.textButton}`}
              >
                <h4>Importe</h4>
              </div>
            </Grid>
            <Grid item xs={2} style={{ marginRight: "0.5%", marginLeft: "1%" }}>
              <div
                className={`${style.alertGrid} ${style.textCenter} ${classes.textButton}`}
              >
                <h4>Anular</h4>
              </div>
            </Grid>
          </Grid>
          {/* ////////////////////////////// LINEA 1 ////////////////////////////////// */}

          {linea1.map((item, index) => {
            return (
              <Grid
                container
                alignItems="center"
                style={{ marginBottom: "10%" }}
                key={index}
              >
                <Grid item xs={1} style={{}}>
                  <Radio
                    checked={item.check}
                    onClick={() => {
                      item.check = !item.check;
                      setLinea1([...linea1], item);
                    }}
                    onChange={handleChange2}
                    value={!item.check}
                    color="default"
                    name="radio-button-demo"
                    inputProps={{ "aria-label": "E" }}
                    size="small"
                    className={classes.inputRadio}
                  />
                </Grid>
                <Grid
                  item
                  xs={3}
                  style={{ marginRight: "0.5%", marginLeft: "0.5%" }}
                >
                  <div
                    type="number"
                    className={`${classes.inputText} ${classes.colorInputs}`}
                  >
                    <Input
                      type="number"
                      disableUnderline={true}
                      startAdornment={
                        <InputAdornment position="start"></InputAdornment>
                      }
                      onChange={(event) => {
                        let limite = event.target.value.length;
                        if (limite <= 4) {
                          item.numero = event.target.value;
                          setLinea1([...linea1], item);
                        }
                      }}
                      value={item.numero}
                      style={{ fontSize: "100%", fontWeight: "bold" }}
                    />
                  </div>
                </Grid>

                <Grid
                  item
                  xs={2}
                  style={{ marginRight: "1%", marginLeft: "0.5%" }}
                >
                  <Select
                    labelId="demo-customized-select-label"
                    id="demo-customized-select"
                    onChange={(event) => {
                      item.posicion = event.target.value;
                      setLinea1([...linea1], item);
                    }}
                    value={item.posicion}
                    input={<BootstrapInput />}
                    className={classes.inputSelect}
                  >
                    <MenuItem value={1}>01</MenuItem>
                    <MenuItem value={5}>05</MenuItem>
                    <MenuItem value={10}>10</MenuItem>
                    <MenuItem value={20}>20</MenuItem>
                  </Select>
                </Grid>
                <Grid
                  item
                  xs={3}
                  style={{ marginRight: "0.5%", marginLeft: "0.5%" }}
                >
                  <div
                    type="number"
                    className={`${classes.inputText} ${classes.colorInputs}`}
                  >
                    <Input
                      type="number"
                      disableUnderline={true}
                      startAdornment={
                        <InputAdornment position="start">$</InputAdornment>
                      }
                      onChange={(event) => {
                        item.importe = event.target.value !== "" ? parseFloat(event.target.value) : "";
                        setLinea1([...linea1], item);
                        sumarTotal(linea1);
                      }}
                      value={item.importe}
                      style={{ fontSize: "100%", fontWeight: "bold" }}
                    />
                  </div>
                </Grid>
                <Grid
                  item
                  xs={2}
                  style={{ marginRight: "0.5%", marginLeft: "0.5%" }}
                >
                  <div
                    className={classes.inputText}
                    onClick={() => {
                      linea1.splice(index,1);
                      setLinea1([...linea1]);
                      sumarTotal(linea1);
                    }}
                  >
                    <Cancel fontSize="medium" className={classes.iconCancel} />
                  </div>
                </Grid>
              </Grid>
            );
          })}
          {/* ////////////////////////////// REDOBLONA //////////////////////////////////////////// */}
          {redoblonamostrar && (
            <Grid container alignItems="center" style={{ marginBottom: "5%" }}>
              <Grid item xs={12}>
                <div
                  style={{
                    fontFamily: "Roboto",
                    marginLeft: "2%",
                    marginBottom: "4%",
                    fontSize: "150%",
                  }}
                >
                  Redoblona
                </div>
              </Grid>
              <Grid item xs={1} style={{}}>
                <Radio
                  checked={true}
                  onChange={handleChange2}
                  value={true}
                  color="default"
                  name="radio-button-demo"
                  inputProps={{ "aria-label": "F" }}
                  size="small"
                  className={classes.inputRadio}
                />
              </Grid>
              <Grid
                item
                xs={3}
                style={{ marginRight: "0.5%", marginLeft: "0.5%" }}
              >
                <div
                  type="number"
                  className={`${classes.inputText} ${classes.colorInputs}`}
                >
                  <Input
                    type="number"
                    disableUnderline={true}
                    startAdornment={
                      <InputAdornment position="start"></InputAdornment>
                    }
                    onChange={(event) => {
                      let _redoblona_numero = event.target.value;
                      setRedoblonaNumero({...redoblonaNumero, numero: _redoblona_numero});
                    }}
                  />
                </div>
              </Grid>
              <Grid
                item
                xs={2}
                style={{ marginRight: "1%", marginLeft: "0.5%" }}
              >
                <Select
                  labelId="demo-customized-select-label"
                  id="demo-customized-select"
                  value={redoblonaNumero.posicion}
                  onChange={handleChange5}
                  //IconComponent="none"
                  input={<BootstrapInput />}
                  className={classes.inputSelect}
                >
                  <MenuItem value={1}>01</MenuItem>
                  <MenuItem value={5}>05</MenuItem>
                  <MenuItem value={10}>10</MenuItem>
                  <MenuItem value={20}>20</MenuItem>
                </Select>
              </Grid>
              <Grid
                item
                xs={3}
                style={{ marginRight: "0.5%", marginLeft: "0.5%" }}
              >
                <div className={`${classes.inputText} ${classes.colorInputs}`}>
                  <Input
                    type="number"
                    disableUnderline={true}
                    startAdornment={
                      <InputAdornment position="start">$</InputAdornment>
                    }
                    onChange={(event) => {
                      let _redoblona = event.target.value !== undefined && event.target.value !== "" ? parseFloat(event.target.value) : 0;
                      setRedoblonaNumero({...redoblonaNumero, importe: _redoblona});
                      //updateTotal(_redoblona);
                    }}
                    style={{ fontSize: "100%", fontWeight: "bold" }}
                  />
                </div>
              </Grid>
              <Grid
                item
                xs={2}
                style={{ marginRight: "0.5%", marginLeft: "0.5%" }}
              >
                <div className={classes.inputText}>
                  <Cancel fontSize="medium" className={classes.iconCancel} />
                </div>
              </Grid>
                    
            
             <Grid item xs={1} style={{}}>
                 {/*<Radio
                  checked={true}
                  onChange={handleChange2}
                  value={true}
                  color="default"
                  name="radio-button-demo2"
                  inputProps={{ "aria-label": "F" }}
                  size="small"
                  className={classes.inputRadio}
                 />*/}
              </Grid>
              <Grid
                item
                xs={3}
                style={{ marginRight: "0.5%", marginLeft: "0.5%" }}
              >
                <div
                  className={`${classes.inputText} ${classes.colorInputs}`}
                >
                  <Input
                    type="number"
                    disableUnderline={true}
                    startAdornment={
                      <InputAdornment position="start"></InputAdornment>
                    }
                    onChange={(event) => {
                      let _redoblona_numero = event.target.value;
                      setRedoblonaNumero2({...redoblonaNumero2, numero: _redoblona_numero});
                    }}
                  />
                </div>
              </Grid>
              <Grid
                item
                xs={2}
                style={{ marginRight: "1%", marginLeft: "0.5%" }}
              >
                <Select
                  labelId="demo-customized-select-label"
                  id="demo-customized-select2"
                  value={redoblonaNumero2.posicion}
                  onChange={handleChangeRedoblona2Posicion}
                  //IconComponent="none"
                  input={<BootstrapInput />}
                  className={classes.inputSelect}
                >
                  <MenuItem value={1}>01</MenuItem>
                  <MenuItem value={5}>05</MenuItem>
                  <MenuItem value={10}>10</MenuItem>
                  <MenuItem value={20}>20</MenuItem>
                </Select>
              </Grid>
              <Grid
                item
                xs={3}
                style={{ marginRight: "0.5%", marginLeft: "0.5%" }}
              >
                {/*<div className={`${classes.inputText} ${classes.colorInputs}`}>
                  <Input
                    type="number"
                    disableUnderline={true}
                    startAdornment={
                      <InputAdornment position="start">$</InputAdornment>
                    }
                    onChange={(event) => {
                      let _redoblona = event.target.value !== undefined && event.target.value !== "" ? parseFloat(event.target.value) : 0;
                      setRedoblonaNumero2({...redoblonaNumero2, importe: _redoblona});
                      //updateTotal(_redoblona);
                    }}
                    style={{ fontSize: "100%", fontWeight: "bold" }}
                  />
                  </div>*/}
              </Grid>
              <Grid
                item
                xs={2}
                style={{ marginRight: "0.5%", marginLeft: "0.5%" }}
              >
                <div className={classes.inputText}>
                  <Cancel fontSize="medium" className={classes.iconCancel} />
                </div>
              </Grid>
            
            </Grid>
            
          )}

          {/* ////////////////////////////// TOTAL APUESTAAAA ////////////////////////////////// */}
          <Grid item xs={3} />
          <Grid item xs={8}>
            <div className={classes.containerTotal}>
              <Input
                type="number"
                disableUnderline={true}
                startAdornment={
                  <InputAdornment position="start">
                    <h3>Total Apuesta $</h3>
                  </InputAdornment>
                }
                className={classes.textTotal}
                value={total}
              />
            </div>
          </Grid>
          {/* ////////////////////////////// ------------------- ////////////////////////////////// */}
        </Grid>
      </Card>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{ width: "100%", height: "100%" }}
        >
          <Card elevation={5} className={style.card}>
            <div className={style.buttonCloseModal}>
              <IconButton
                aria-label="delete"
                size="small"
                onClick={handleClose}
              >
                <HighlightOff fontSize="small" />
              </IconButton>
            </div>

            <div style={{ width: "100%", height: "80%" }}>
              <Grid container style={{}}>
                <Grid item xs={12} style={{ marginBottom: "3%" }}>
                  <Banner5 imagen={Logo7} tittle="Quiniela" />
                </Grid>
                <Grid item xs={4} style={{ marginBottom: "1%" }}>
                  <div className={`${style.textCenter} ${style.textBold}`}>
                    Numero
                  </div>
                </Grid>
                <Grid item xs={4} style={{ marginBottom: "1%" }}>
                  <div className={`${style.textCenter} ${style.textBold}`}>
                    Posición
                  </div>
                </Grid>
                <Grid item xs={4} style={{ marginBottom: "1%" }} />

                {vistaModal.map((item, index) => {
                  console.log(vistaModal);
                  return (
                    <Fragment key={index}>
                      <Grid item xs={4} style={{}}>
                        <Input
                          type="number"
                          disableUnderline={true}
                          disabled
                          value={item.numero}
                          className={classes.inputTextModal}
                        />
                      </Grid>
                      <Grid item xs={4} style={{}}>
                        <Input
                          type="number"
                          disableUnderline={true}
                          disabled
                          value={item.posicion}
                          className={classes.inputTextModal}
                        />
                      </Grid>

                      <Grid item xs={4}>
                        <IconButton className={classes.iconCancelModal}>
                          <Cancel fontSize="medium" />
                        </IconButton>
                      </Grid>
                      <Grid item xs={12}>
                        <Divider />
                      </Grid>
                    </Fragment>
                  );
                })}
              </Grid>
            </div>

            <div style={{ width: "100%", height: "100%" }}>
              <Grid
                item
                xs={12}
                style={{ position: "absolute", bottom: "5%", width: "90%" }}
              >
                <Divider />

                <div
                  style={{
                    textAlign: "center",
                    borderRadius: 10,
                    fontSize: "150%",
                    fontWeight: "bold",
                    paddingTop: "5%",
                    paddingBottom: "5%",
                  }}
                >
                  Total a cobrar ${total}
                </div>
              </Grid>
            </div>
          </Card>
        </Grid>
      </Modal>

      <Modal
        open={open22}
        onClose={handleClose22}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{ width: "100%", height: "100%" }}
        >
          <Card elevation={5} className={style.alertCard}>
            <div className={style.buttonCloseModal}>
              <IconButton
                aria-label="delete"
                size="small"
                onClick={handleClose22}
              >
                <HighlightOff fontSize="small" />
              </IconButton>
            </div>
            <div
              style={{ margin: "4%", textAlign: "center", marginTop: "10%" }}
              onClick={handleClose22}
            >
              <IconButton onClick={handleClose22}>
                <ErrorOutline className={style.alertErrorIcon} />
              </IconButton>
              <h1>¡Maximo 3-4 digitos!</h1>
            </div>
          </Card>
        </Grid>
      </Modal>
      <Button
        variant="contained"
        size="small"
        disabled={disabledBtnCarrito}
        className={`${classes.viewColumn9} ${style.finalMargin} ${style.buttonColorPrimary} ${style.buttonText}`}
        onClick={() => {
          

          let total_aumentar = 0;
          for (let index in vistaModal) {
            total_aumentar = total_aumentar + vistaModal[index].importe;
          }
          let dup2 = vistaModal;
          if(redoblonaNumero.numero !== undefined){
            linea1.push(redoblonaNumero);
          }
          if(redoblonaNumero2.numero !== undefined){
            linea1.push(redoblonaNumero2);
          }
         
          for (let index in linea1) {
            if( linea1[index].importe !== null &&  linea1[index].importe !== undefined){
              vistaModal.push(linea1[index]);
              total_aumentar = total_aumentar + linea1[index].importe;
            }
          }
          setVistaModal([...vistaModal]);
          dispatch(
            agregarJugada({
              id: uuidv4(),
              nombre: "Quinela",
              jugadas: linea1,
              juego: props.location.state.juego,
              total,
              tipo: 1,
              Logo: Logo7,
            })
          );
          dispatch(clearQuineela())
          setTotal(total_aumentar);
          setLinea1([
            {
              check: false,
              numero: "",
              posicion: 1,
              importe: "",
              tipo: 0,
            },
          ]);
          props.history.push("/Carrito");
        }}
      >
        Agregar al carro y seguir jugando
      </Button>
      <Fotter />
    </Grid>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    borderRadius: 10,
  },
  containerBanner: {
    width: "100%",
    height: "100%",
    marginTop: "1%",
    marginBottom: "5%",
  },
  iconList: {
    color: theme.colors.darkOrange,
  },
  textButton: {
    fontFamily: theme.fonts.primary,
    fontSize: "90%",
  },
  inputRadio: {
    padding: 0,
    margin: 0,
    marginRight: "3%",
    paddingTop: "10%",
    paddingBottom: "10%",
  },
  inputSelect: {
    width: "100%",
    height: "100%",
    backgroundColor: theme.colors.lightGray,
    borderRadius: 7,
    marginRight: "3%",
    paddingTop: "15%",
    paddingBottom: "15%",
    fontSize: "110%",
    fontWeight: "bold",
  },
  inputSelectRedoblona: {
    width: "100%",
    height: "100%",
    backgroundColor: theme.colors.lightGray,
    borderRadius: 7,
    margin: 0,
    padding: 0,
    marginRight: "3%",
    paddingTop: "30%",
    paddingBottom: "30%",
  },
  colorInputs: {
    backgroundColor: theme.colors.lightGray,
  },
  inputText: {
    textAlign: "center",
    borderRadius: 7,
    marginRight: "3%",
    paddingTop: "10%",
    paddingBottom: "10%",
  },
  iconCancel: {
    color: theme.colors.red,
    margin: "6%",
  },
  containerTotal: {
    width: "100%",
    height: "100%",
    backgroundColor: theme.colors.lightGray,
    borderRadius: 7,
    paddingTop: "6%",
    paddingBottom: "6%",
  },
  cover1: {
    width: 45,
    height: 40,
    alignContent: "center",
    justifyContent: "center",
    borderRadius: 7,
    borderColor: theme.colors.lightGray,
    border: "2px solid",
    marginLeft: "5%",
  },
  gameButton: {
    height: "100%",
    textAlign: "center",
    border: "1px solid",
    borderColor: theme.colors.lightGray,
    borderRadius: 7,
    fontFamily: theme.fonts.primary,
    fontSize: "80%",
    marginTop: "8%",
    textTransform: "none",
    fontWeight: "bold",
  },
  textTotal: {
    color: theme.colors.black,
    marginLeft: "5%",
  },
  textModal: {},
  viewColumn9: {
    width: "96%",
    justifyContent: "center",
    marginTop: "10%",
    borderRadius: 7,
  },
  svg: {
    width: "90%",
    height: "90%",
    marginTop: "5%",
  },
  inputTextModal: {
    width: "80%",
    height: "70%",
    textAlign: "center",
    backgroundColor: theme.colors.lightGray,
    fontWeight: "bold",
    padding: 8,
    borderRadius: 10,
    margin: "5% 0 5% 10%",
  },
  iconCancelModal: {
    width: "100%",
    textAlign: "center",
    "& svg": {
      color: "red",
      fontSize: 40,
    },
  },
}));

const BootstrapInput = withStyles((theme) => ({
  root: {
    "label + &": {
      marginTop: 0,
    },
  },
  input: {
    borderRadius: 7,
    backgroundColor: "transparent",
    border: "0px solid transparent",
    fontSize: 13,
    padding: "7px 26px 7px 12px",
    fontFamily: theme.fonts.primary,
    "&:focus": {
      backgroundColor: "transparent",
      borderRadius: 4,
      borderColor: "transparent",
    },
  },
}))(InputBase);

const BootstrapInput11 = withStyles((theme) => ({
  root: {
    "label + &": {
      marginTop: 0,
    },
  },
  input: {
    borderRadius: 7,
    backgroundColor: "transparent",
    border: "0px solid transparent",
    fontSize: 12,
    padding: "7px 26px 7px 6px",
    fontFamily: theme.fonts.primary,
    "&:focus": {
      backgroundColor: "transparent",
      borderRadius: 4,
      borderColor: "transparent",
    },
  },
}))(InputBase);
