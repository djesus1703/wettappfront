import React from "react";
import { Grid, Input } from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import SkipPreviousIcon from "@material-ui/icons/SkipPrevious";
import Cancel from "@material-ui/icons/Cancel";
import SkipNextIcon from "@material-ui/icons/SkipNext";
import Button from "@material-ui/core/Button";
import InputBase from "@material-ui/core/InputBase";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import Radio from "@material-ui/core/Radio";
import ListAltTwoTone from "@material-ui/icons/ListAltTwoTone";
import imagen1 from "../../src/assets/iconQP.png";
import imagen2 from "../../src/assets/quni.png";
import imagen3 from "../../src/assets/icon_loto.png";
import imagen4 from "../../src/assets/icon_lotoPlus.png";
import imagen5 from "../../src/assets/icon_brinco.png";
import imagen6 from "../../src/assets/icon_prode.png";
import imagen7 from "../../src/assets/icon_billete.png";
import { ReactComponent as Logo } from "../assets/prestador1.svg";
import Header from "../components/Header";
import Fotter from "../components/Fotter";

import Banner1 from "../components/Banner1";
import { DataUsageOutlined } from "@material-ui/icons";

export default function Quiniela1() {
  const classes = useStyles();
  const [age, setAge] = React.useState("");
  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const handleChange3 = () => {
    let multiple = 0;
    if (multiple >= 1) {
      setSelectedValue(event.target.value);
    } else {
      setAge(event.target.value);
    }
    console.log("878798987987", multiple);
  };
  const handleChange2 = (event) => {
    setSelectedValue(event.target.value);
  };

  const principal = (event) => {
    event.dato;
  };
  const [selectedValue, setSelectedValue] = React.useState("a");
  return (
    <Grid container direction="row" justify="center">
      <Header />
      <Card elevation={0} className={classes.root}>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{ margin: "2%" }}
        >
          <Grid item xs={12}>
            <div
              style={{
                width: "100%",
                height: "100%",
                marginTop: "1%",
                marginBottom: "5%",
              }}
            >
              <Banner1
                imagen={<Logo className={classes.svg} />}
                name="Quiniela Provincia"
                tittle="Buenos Aires"
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div
              style={{
                fontFamily: "Roboto",
                marginLeft: "2%",
                marginBottom: "4%",
                fontSize: "150%",
              }}
            >
              Redoblona
            </div>
          </Grid>

          <Grid container item xs={12} style={{ marginBottom: "10%" }}>
            <Grid item xs={3} style={{ margin: "0.5%" }}>
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  textAlign: "center",
                  border: "1px solid",
                  borderColor: "#eeeeee",
                  borderRadius: 7,
                }}
              >
                <div
                  style={{
                    fontFamily: "Roboto",
                    fontSize: "90%",
                    textAlign: "center",
                    marginTop: "8%",
                  }}
                >
                  Redoblona
                </div>
              </div>
            </Grid>
            <Grid item xs={2} style={{ margin: "0.5%" }}>
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  textAlign: "center",
                  border: "1px solid",
                  borderColor: "#eeeeee",
                  borderRadius: 7,
                }}
              >
                <div
                  style={{
                    fontFamily: "Roboto",
                    fontSize: "90%",
                    textAlign: "center",
                    marginTop: "10%",
                  }}
                >
                  Repetir
                </div>
              </div>
            </Grid>
            <Grid item xs={3} style={{ margin: "0.5%" }}>
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  textAlign: "center",
                  border: "1px solid",
                  borderColor: "#eeeeee",
                  borderRadius: 7,
                }}
              >
                <div
                  style={{
                    fontFamily: "Roboto",
                    fontSize: "90%",
                    textAlign: "center",
                    marginTop: "8%",
                  }}
                >
                  Jugar otra
                </div>
              </div>
            </Grid>
            <Grid item xs={2} style={{ margin: "0.5%" }}>
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  textAlign: "center",
                  border: "1px solid",
                  borderColor: "#eeeeee",
                  borderRadius: 7,
                }}
              >
                <div
                  style={{
                    fontFamily: "Roboto",
                    fontSize: "90%",
                    textAlign: "center",
                    marginTop: "10%",
                  }}
                >
                  Bajar
                </div>
              </div>
            </Grid>
            <Grid item xs={1} style={{ margin: "0.5%" }}>
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  textAlign: "center",
                  border: "1px solid",
                  borderColor: "#eeeeee",
                  borderRadius: 7,
                  backgroundColor: "#e97400",
                }}
              >
                <div style={{ textAlign: "center", marginTop: "10%" }}>
                  <ListAltTwoTone fontSize="small" />
                </div>
              </div>
            </Grid>
          </Grid>

          <Grid container item xs={12} style={{ marginBottom: "5%" }}>
            <Grid item xs={1} style={{ margin: "0.5%" }}>
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  textAlign: "center",
                  borderRadius: 7,
                }}
              >
                <div style={{ display: "none" }}>0</div>
              </div>
            </Grid>
            <Grid item xs={2} style={{ margin: "0.5%" }}>
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  textAlign: "center",
                  borderRadius: 7,
                }}
              >
                <div
                  style={{
                    fontFamily: "Roboto",
                    fontSize: "85%",
                    textAlign: "center",
                  }}
                >
                  Número
                </div>
              </div>
            </Grid>
            <Grid item xs={2} style={{ margin: "0.5%" }}>
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  textAlign: "center",
                  borderRadius: 7,
                }}
              >
                <div
                  style={{
                    fontFamily: "Roboto",
                    fontSize: "85%",
                    textAlign: "center",
                  }}
                >
                  Posición
                </div>
              </div>
            </Grid>
            <Grid item xs={2} style={{ margin: "0.5%" }}>
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  textAlign: "center",
                  borderRadius: 7,
                }}
              >
                <div
                  style={{
                    fontFamily: "Roboto",
                    fontSize: "85%",
                    textAlign: "center",
                  }}
                >
                  Importe
                </div>
              </div>
            </Grid>
            <Grid item xs={1} style={{ margin: "0.5%" }}>
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  textAlign: "center",
                  borderRadius: 7,
                }}
              >
                <div
                  style={{
                    fontFamily: "Roboto",
                    fontSize: "85%",
                    textAlign: "center",
                  }}
                >
                  Anular
                </div>
              </div>
            </Grid>
            <Grid item xs={3} style={{ margin: "0.5%" }}>
              <div
                style={{
                  width: "100%",
                  height: "100%",
                  textAlign: "center",
                  borderRadius: 7,
                }}
              >
                <div
                  style={{
                    fontFamily: "Roboto",
                    fontSize: "85%",
                    textAlign: "center",
                  }}
                >
                  Total apuesta
                </div>
              </div>
            </Grid>
          </Grid>

          <Grid container item xs={12}>
            <Grid
              container
              item
              xs={1}
              style={{
                borderRadius: 5,
                margin: "0.5%",
                marginBottom: "2%",
                alignContent: "center",
                justifyContent: "center",
              }}
            >
              <div>
                <Radio
                  checked={selectedValue === "value"}
                  onChange={principal}
                  value="e"
                  color="default"
                  name="radio-button-demo"
                  inputProps={{ "aria-label": "E" }}
                  size="medium"
                />
              </div>
            </Grid>
            <Grid
              container
              item
              xs={2}
              style={{
                borderRadius: 5,
                margin: "0.5%",
                marginBottom: "3%",
                backgroundColor: "#eeeeee",
                alignContent: "center",
                justifyContent: "flex-start",
              }}
            >
              <div className={classes.letra}></div>
            </Grid>
            <Grid
              container
              item
              xs={2}
              style={{
                borderRadius: 5,
                margin: "0.5%",
                marginBottom: "3%",
                backgroundColor: "#eeeeee",
                alignContent: "center",
                justifyContent: "flex-end",
              }}
            >
              <Select
                labelId="demo-customized-select-label"
                id="demo-customized-select"
                value={age}
                onChange={handleChange}
                input={<BootstrapInput />}
              >
                <MenuItem value={10}>1</MenuItem>
                <MenuItem value={20}>2</MenuItem>
                <MenuItem value={30}>3</MenuItem>
              </Select>
            </Grid>
            <Grid
              container
              item
              xs={2}
              style={{
                borderRadius: 5,
                margin: "0.5%",
                marginBottom: "3%",
                backgroundColor: "#eeeeee",
                alignContent: "center",
                justifyContent: "flex-start",
              }}
            >
              <div className={classes.letra}>$</div>
            </Grid>
            <Grid
              container
              item
              xs={1}
              style={{
                borderRadius: 5,
                margin: "0.5%",
                marginBottom: "3%",
                alignContent: "center",
                justifyContent: "center",
              }}
            >
              <Select
                id=""
                value={}
                onChange={handleChange3}
                input={<BootstrapInput />}
              >
                <option value={10}>122</option>
              </Select>
            </Grid>
            <Grid
              container
              item
              xs={3}
              style={{
                borderRadius: 5,
                margin: "0.5%",
                marginBottom: "3%",
                backgroundColor: "#eeeeee",
                alignContent: "center",
                justifyContent: "flex-start",
              }}
            >
              <div className={classes.letra}>$</div>
            </Grid>
          </Grid>

          <Grid container item xs={12} style={{ marginBottom: "10%" }}>
            <Grid
              container
              item
              xs={1}
              style={{
                borderRadius: 5,
                margin: "0.5%",
                marginBottom: "1%",
                alignContent: "center",
                justifyContent: "center",
              }}
            >
              <div>
                <Radio
                  checked={selectedValue === "e"}
                  onChange={handleChange2}
                  value="e"
                  color="default"
                  name="radio-button-demo"
                  inputProps={{ "aria-label": "E" }}
                  size="small"
                />
              </div>
            </Grid>
            <Grid
              container
              item
              xs={2}
              style={{
                borderRadius: 5,
                margin: "0.5%",
                marginBottom: "1%",
                backgroundColor: "#eeeeee",
              }}
            >
              <div className={classes.letra}></div>
            </Grid>
            <Grid
              container
              item
              xs={1}
              style={{
                borderRadius: 5,
                margin: "0.2%",
                marginBottom: "1%",
                backgroundColor: "#eeeeee",
                alignContent: "center",
                justifyContent: "flex-start",
              }}
            >
              <Select
                labelId="demo-customized-select-label"
                id="demo-customized-select-small"
                value={age}
                onChange={handleChange}
                input={<BootstrapInput />}
              >
                <MenuItem value={10}>10</MenuItem>
              </Select>
            </Grid>
            <Grid
              container
              item
              xs={1}
              style={{
                borderRadius: 5,
                margin: "0.3%",
                marginBottom: "1%",
                backgroundColor: "#eeeeee",
                alignContent: "center",
                justifyContent: "flex-start",
              }}
            >
              <Select
                labelId="demo-customized-select-label"
                id="demo-customized-select"
                value={age}
                onChange={handleChange}
                input={<BootstrapInput />}
              >
                <MenuItem value={10}>1</MenuItem>
                <MenuItem value={20}>2</MenuItem>
                <MenuItem value={30}>3</MenuItem>
              </Select>
            </Grid>
            <Grid
              container
              item
              xs={2}
              style={{
                borderRadius: 5,
                margin: "0.5%",
                marginBottom: "1%",
                backgroundColor: "#eeeeee",
              }}
            ></Grid>
            <Grid
              container
              item
              xs={1}
              style={{
                borderRadius: 5,
                margin: "0.5%",
                marginBottom: "1%",
                alignContent: "center",
                justifyContent: "center",
              }}
            >
              <Cancel fontSize="small" style={{ color: "red" }} />
            </Grid>
            <Grid
              container
              item
              xs={3}
              style={{
                borderRadius: 5,
                margin: "0.5%",
                marginBottom: "1%",
                backgroundColor: "#eeeeee",
                alignContent: "center",
                justifyContent: "flex-start",
              }}
            >
              <div className={classes.letra}>$</div>
            </Grid>
          </Grid>

          <div type="number" style={{}}>
            <Input style={{ width: "100%" }} />
          </div>
        </Grid>
      </Card>
      <Button
        variant="contained"
        style={{ backgroundColor: "#ff7f00" }}
        size="small"
        className={classes.viewColumn9}
      >
        <div className={classes.letra2}>Agregar al carro y seguir jugando</div>
      </Button>

      <Fotter />
    </Grid>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: "white",
    borderRadius: 10,
    // marginBottom: '15%',
  },
  root1: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    // backgroundColor: 'white',
    marginTop: "5%",
    marginLeft: "1%",
    // marginBottom: '15%',
  },
  root3: {
    // display: 'flex',
    // width: '100%',
    // zIndex: 1,
    // backgroundColor: 'white',
    // marginTop: '5%',
    // marginTop: '5%',
    // marginLeft: '3%',
    marginBottom: "4%",
  },
  details: {
    display: "flex",
    flexDirection: "column",
  },
  content: {
    flex: "1 0 auto",
  },
  cover: {
    // width: 151,
    width: 90,
    height: 80,
    alignContent: "center",
    justifyContent: "center",
    borderRadius: 7,
    borderColor: "#eeeeee",
    border: "2px solid",
  },
  letra: {
    fontSize: 12,
  },
  viewColumn7: {
    backgroundColor: "transparent",
    alignContent: "center",
    justifyContent: "center",
    // margin: '4%',
    // height: '1%',
    marginTop: "1%",
    marginBottom: "2%",
    marginLeft: "2%",
    marginRight: "1%",
    borderRadius: 7,
    backgroundColor: "#79796a",
    // borderWidth: 100,
    // border: '2px solid',
    width: "45%",
  },
  viewColumn8: {
    backgroundColor: "transparent",
    alignContent: "center",
    justifyContent: "center",

    marginTop: "1%",
    marginBottom: "2%",
    marginLeft: "1%",
    marginRight: "1%",
    borderRadius: 7,
    backgroundColor: "#ff7f00",

    width: "45%",
  },
  viewColumn9: {
    width: "96%",
    // backgroundColor: 'transparent',
    // alignContent: 'center',
    justifyContent: "center",
    // margin: '4%',
    // height: '1%',
    marginTop: "10%",
    marginBottom: "20%",
    // marginLeft: '4%',
    // marginRight: '4%',
    borderRadius: 7,
    backgroundColor: "#ff7f00",
    // borderWidth: 100,
    // border: '2px solid',
    // position: 'fixed',
    // bottom: 0,
  },
  letra2: {
    fontFamily: "Roboto",
    // fontWeight: 'bold',
    fontSize: "100%",
    margin: "4%",
    color: "white",
    textTransform: "none",
  },
  letra3: {
    fontSize: "130%",
    // margin: '3%',
    color: "white",
    fontWeight: "bold",
  },
  svg: {
    width: "90%",
    height: "80%",
    // marginLeft: '20%',
    marginTop: "5%",
    // marginBottom: '5%',
  },
}));

const BootstrapInput = withStyles((theme) => ({
  root: {
    "input:disabled ": {
      marginTop: 2,
    },
  },
  input: {
    borderRadius: 7,
    // position: 'relative',
    backgroundColor: "transparent",
    border: "2px solid transparent",
    fontSize: 15,
    // marginLeft: '3%',
    // margin: '0%',
    // padding: '10px 26px 10px 12px',
    // transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: ["Roboto"].join(","),
    "&:focus": {
      // width: -10,
      backgroundColor: "transparent",
      // height: 40,
      borderRadius: 4,
      borderColor: "green",
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      // marginLeft: '3%',
      // width: 1,
    },
  },
}))(InputBase);
