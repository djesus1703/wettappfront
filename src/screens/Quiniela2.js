import React, { Fragment } from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Modal from '@material-ui/core/Modal';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import HighlightOff from '@material-ui/icons/HighlightOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import Input from '@material-ui/core/Input';
import { TextField, createMuiTheme, ThemeProvider } from "@material-ui/core";
import Typography from '@material-ui/core/Typography';
import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';
import Cancel from '@material-ui/icons/Cancel';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import Button from '@material-ui/core/Button';
import InputBase from '@material-ui/core/InputBase';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Radio from '@material-ui/core/Radio';
import { CheckCircleOutline, ErrorOutline } from '@material-ui/icons';
import ListAltTwoTone from '@material-ui/icons/ListAltTwoTone';
import imagen1 from '../../src/assets/iconQP.png';
import imagen2 from '../../src/assets/quni.png';
import imagen3 from '../../src/assets/icon_loto.png';
import imagen4 from '../../src/assets/icon_lotoPlus.png';
import imagen5 from '../../src/assets/icon_brinco.png';
import imagen6 from '../../src/assets/icon_prode.png';
import imagen7 from '../../src/assets/icon_billete.png';
import { ReactComponent as Logo } from '../assets/prestador1.svg';
import Header from '../components/Header'
import Fotter from '../components/Fotter'

import Banner1 from '../components/Banner1';
import { CardMembershipOutlined } from '@material-ui/icons';

export default function Quiniela1(props) {
    // console.log('prepoosss', props.location.state.cantidad);
    const classes = useStyles();
    const [age3, setAge3] = React.useState('');
    const [age4, setAge4] = React.useState('');
    const handleChange3 = (event) => {
        setAge3(event.target.value);
    };
    const handleChange4 = (event) => {
        setAge4(event.target.value);
    };
    const handleChange2 = (event) => {
        // linea1.check = event.target.value
        // setLinea1(linea1);
    };
    const [redoblonamostrar, setRedoblonamostrar] = React.useState(false)
    const [dinamic, setDinamic] = React.useState([]);
    const cambioa = () => {
        // let temp1 = dinamic
        // temp1.push(0)
        redoblonamostrar ? setRedoblonamostrar(false) : setRedoblonamostrar(true)
        // setDinamic(oldarray => [...oldarray, 0])
        // console.log('ejele', dinamic);
    };
    const [open, setOpen] = React.useState(false);
    const [open22, setOpen22] = React.useState(false);
    const handleOpen = () => {
        // console.log('dsdsdsdsd');
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    const handleOpen22 = () => {
        // console.log('dsdsdsdsd');
        setOpen22(true);
    };
    const handleClose22 = () => {
        setOpen22(false);
    };
    const [linea1, setLinea1] = React.useState([{
        check: false,
        numero: '',
        posicion: '',
        importe: '',
        tipo: 0,
    }]);
    const [total, setTotal] = React.useState(0)
    const [vistaModal, setVistaModal] = React.useState([])
    const repid = () => {
        // console.log('repid');
        let ultimajugada = vistaModal.length
        if (ultimajugada > 0) {
            let ultima = ultimajugada - 1
            setLinea1([{
                check: vistaModal[ultima].check,
                numero: vistaModal[ultima].numero,
                posicion: vistaModal[ultima].posicion,
                importe: vistaModal[ultima].importe,
                tipo: 2,
            }])
        }
    };
    const bajar = () => {
        let copia = linea1[0]
        let tamanoDeJugada = copia.numero.length
        let cantidadRepetitiva = parseInt(tamanoDeJugada / 2)
        if (tamanoDeJugada <= 2) {
            setOpen22(true);
            return
        }
        if (tamanoDeJugada > 4) {
            setOpen22(true);
            return
        }
        if (tamanoDeJugada == 3 || tamanoDeJugada == 4) {
            let dup = linea1

            for (let index = 0; index < cantidadRepetitiva; index++) {
                // console.log('tamanoo222', index);
                linea1.push({
                    check: copia.check,
                    numero: copia.numero.substring(index + 1),
                    posicion: copia.posicion,
                    importe: copia.importe,
                    tipo: 4,
                })

            }
        }
        // console.log('tamanoo', linea1);
        setLinea1([...linea1])
    }

    const jugarOtra = () => {
        linea1.push({
            check: false,
            numero: '',
            posicion: '',
            importe: '',
            tipo: 3,
        })
        setLinea1([...linea1])
    }

    return (
        <Grid container direction="row" justify="center">
            <Header />
            <Card elevation={0} className={classes.root}>
                {/* < Grid container item xs={12} className={classes.root1}>
                    <Banner1 imagen={<Logo className={classes.svg} />} name='Quiniela Provincia' tittle='Buenos Aires' /> */}
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    style={{ margin: '2%', }}
                >
                    < Grid item xs={12}>
                        <div style={{ width: '100%', height: '100%', marginTop: '1%', marginBottom: '5%', }}>
                            <Banner1 imagen={<Logo className={classes.svg} />} name='Quiniela Provincia' tittle='Buenos Aires' />
                        </div>
                    </Grid>
                    {/* < Grid container item xs={12} style={{ marginBottom: '10%' }}> */}
                    < Grid item sm={2} style={{ margin: '0.1%', marginBottom: '5%' }}>
                        <Button size="small" style={{ height: '100%', textAlign: 'center', border: '1px solid', borderColor: '#eeeeee', borderRadius: 7, }}
                            onClick={() => {
                                cambioa()
                            }}
                        >
                            <div style={{ fontFamily: "Roboto", fontSize: '90%', textAlign: 'center', marginTop: '8%', textTransform: 'none', }}>Redoblona</div>
                        </Button>
                    </Grid>
                    < Grid item sm={2} style={{ margin: '0.1%', marginBottom: '5%' }}>
                        <Button size="small" style={{ height: '100%', textAlign: 'center', border: '1px solid', borderColor: '#eeeeee', borderRadius: 7, }}
                            onClick={() => {
                                repid()
                            }}
                        >
                            <div style={{ fontFamily: "Roboto", fontSize: '90%', textAlign: 'center', marginTop: '10%', textTransform: 'none', }}>Repetir</div>

                        </Button>
                    </Grid>
                    < Grid item sm={3} style={{ margin: '0.1%', marginBottom: '5%' }}>
                        <Button size="small" style={{ height: '100%', textAlign: 'center', border: '1px solid', borderColor: '#eeeeee', borderRadius: 7, }}
                            onClick={() => {
                                jugarOtra()
                            }}

                        >
                            <div style={{ fontFamily: "Roboto", fontSize: '90%', textAlign: 'center', marginTop: '8%', textTransform: 'none', }}>Jugar otra</div>
                        </Button>
                    </Grid>
                    < Grid item sm={1} style={{ margin: '0.1%', marginBottom: '5%' }}>
                        <Button size="small" style={{ height: '100%', textAlign: 'center', border: '1px solid', borderColor: '#eeeeee', borderRadius: 7, }}
                            onClick={() => {
                                bajar()
                            }}
                        >
                            <div style={{ fontFamily: "Roboto", fontSize: '90%', textAlign: 'center', marginTop: '10%', textTransform: 'none', }}>Bajar</div>
                        </Button>
                    </Grid>
                    < Grid item sm={1} style={{ margin: '0.1%', marginBottom: '5%' }}>
                        <IconButton
                            onClick={() => {
                                handleOpen()
                            }}
                        >
                            <ListAltTwoTone fontSize='small' style={{ color: '#e97400', }} />
                        </IconButton>
                        {/* <Button size="small" style={{ height: '100%', textAlign: 'center', border: '0.5px solid', borderColor: '#eeeeee', borderRadius: 7, backgroundColor: '#e97400', margin: 0, }}>
                                <ListAltTwoTone fontSize='small' style={{ margin: 0, padding: 0 }} />
                            </Button> */}

                    </Grid>
                    {/* </Grid> */}

                    {/* < Grid container item xs={12} > */}
                    <Grid
                        container
                        justify="center"
                        alignItems="center"
                        style={{ marginBottom: '5%' }}
                    >
                        < Grid item sm={1} style={{}}>
                            <div style={{ width: '100%', height: '100%', textAlign: 'center', }}>
                                <div
                                    style={{ color: 'transparent', fontFamily: "Roboto", fontSize: '50%', textAlign: 'center', }}
                                >0ffgs</div>
                            </div>
                        </Grid>
                        < Grid item sm={2} style={{ marginRight: '0.5%', marginLeft: '1%', }}>
                            <div style={{ width: '100%', height: '100%', textAlign: 'center', }}>
                                <div style={{ fontFamily: "Roboto", fontSize: '90%', textAlign: 'center', }}>Número</div>
                            </div>
                        </Grid>
                        < Grid item sm={2} style={{ marginRight: '0.5%', marginLeft: '1%', }}>
                            <div style={{ width: '100%', height: '100%', textAlign: 'center', }}>
                                <div style={{ fontFamily: "Roboto", fontSize: '90%', textAlign: 'center', }}>Posición</div>
                            </div>
                        </Grid>
                        < Grid item sm={2} style={{ marginRight: '0.5%', marginLeft: '1%', }}>
                            <div style={{ width: '100%', height: '100%', textAlign: 'center', }}>
                                <div style={{ fontFamily: "Roboto", fontSize: '90%', textAlign: 'center', }}>Importe</div>
                            </div>
                        </Grid>
                        < Grid item sm={1} style={{ marginRight: '0.5%', marginLeft: '1%', }} >
                            <div style={{ width: '100%', height: '100%', textAlign: 'center', }}>
                                <div style={{ fontFamily: "Roboto", fontSize: '90%', textAlign: 'center', }}>Anular</div>
                            </div>
                        </Grid>
                        < Grid item sm={3} style={{ marginRight: '0.5%', marginLeft: '1%', }}>
                            <div style={{ width: '100%', height: '100%', textAlign: 'center', }}>
                                <div style={{ fontFamily: "Roboto", fontSize: '90%', textAlign: 'center', }}>Total apuesta</div>
                            </div>
                        </Grid>
                    </Grid>
                    {/* ////////////////////////////// LINEA 1 ////////////////////////////////// */}
                    {/* <pre>{JSON.stringify(linea1)}</pre> */}
                    {linea1.map((item, index) => {
                        return (
                            <Grid
                                container
                                // justify="center"
                                alignItems="center"
                                style={{ marginBottom: '5%' }}
                                key={index}
                            >
                                < Grid item xs={1} style={{}}>
                                    <Radio
                                        checked={item.check}
                                        onClick={() => {
                                            // console.log('fefe', linea1.check);
                                            // linea1.check = !linea1.check
                                            item.check = !item.check
                                            setLinea1([...linea1], item)

                                        }}
                                        onChange={handleChange2}
                                        value={!item.check}
                                        color="default"
                                        name="radio-button-demo"
                                        inputProps={{ 'aria-label': 'E' }}
                                        size="small"
                                        style={{ padding: 0, margin: 0, }}
                                    />
                                </Grid>
                                < Grid item xs={2} style={{ marginRight: '0.5%', marginLeft: '0.5%', }}>
                                    <div type='number' style={{ textAlign: 'center', backgroundColor: '#eeeeee', borderRadius: 7, }}>
                                        <Input type='number' disableUnderline={true}
                                            onChange={(event) => {
                                                // console.log('event', event.target.value);
                                                // setLinea1([{ ...item, numero: event.target.value }])
                                                item.numero = event.target.value
                                                setLinea1([...linea1], item)
                                            }}
                                            value={item.numero}
                                        />
                                    </div>
                                </Grid>

                                < Grid item xs={2} style={{ marginRight: '0.5%', marginLeft: '0.5%', }}>
                                    {/* <div style={{ textAlign: 'center', backgroundColor: '#eeeeee', borderRadius: 7, }}> */}
                                    {/* <Input type='number' /> */}
                                    <Select
                                        labelId="demo-customized-select-label"
                                        id="demo-customized-select"
                                        // value={age}
                                        onChange={(event) => {

                                            // console.log('event', event.target.value);
                                            // setLinea1([{ ...item, posicion: event.target.value }])
                                            item.posicion = event.target.value
                                            setLinea1([...linea1], item)
                                        }}
                                        value={item.posicion}
                                        // IconComponent="none"
                                        input={<BootstrapInput />}
                                        style={{ width: '100%', height: '100%', backgroundColor: '#eeeeee', borderRadius: 7, }}
                                    >
                                        <MenuItem value={1}>01</MenuItem>
                                        <MenuItem value={5}>05</MenuItem>
                                        <MenuItem value={10}>10</MenuItem>
                                        <MenuItem value={20}>20</MenuItem>
                                    </Select>
                                    {/* </div> */}
                                </Grid>
                                < Grid item xs={2} style={{ marginRight: '0.5%', marginLeft: '0.5%', }}>
                                    <div style={{ textAlign: 'center', backgroundColor: '#eeeeee', borderRadius: 7, }}>
                                        <Input type='number' disableUnderline={true} startAdornment={<InputAdornment position="start">$</InputAdornment>}
                                            onChange={(event) => {
                                                // console.log('event', event.target.value);
                                                // setLinea1([{ ...item, importe: parseFloat(event.target.value) }])
                                                item.importe = parseFloat(event.target.value)
                                                setLinea1([...linea1], item)

                                                let total_aumentar = 0
                                                for (let index2 in linea1) {

                                                    total_aumentar = total_aumentar + linea1[index2].importe
                                                }
                                                total_aumentar = total_aumentar * props.location.state.cantidad
                                                setTotal(total_aumentar)
                                            }}
                                            value={item.importe}
                                        />
                                    </div>
                                </Grid>
                                < Grid item xs={1} style={{ marginRight: '0.5%', marginLeft: '0.5%', }} >
                                    <IconButton style={{ padding: 0, margin: 0, alignContent: 'center', }}>
                                        <Cancel fontSize='small' style={{ color: 'red', marginLeft: '40%', }} />
                                    </IconButton>

                                </Grid>
                                < Grid item xs={3} style={{}}>
                                    <div style={{ textAlign: 'center', backgroundColor: '#eeeeee', borderRadius: 7, }}>
                                        <Input type='number' disableUnderline={true} startAdornment={<InputAdornment position="start">$</InputAdornment>}
                                            value={total}
                                        />
                                    </div>
                                </Grid>
                            </Grid>

                        )
                    })}
                    {/* ////////////////////////////// REDOBLONA ////////////////////////////////// */}
                    {redoblonamostrar ? (
                        <Grid
                            container
                            alignItems="center"
                            style={{ marginBottom: '5%' }}
                        // key={index}
                        >
                            < Grid item xs={12} >
                                <div style={{ fontFamily: "Roboto", marginLeft: '2%', marginBottom: '4%', fontSize: '150%', }}>Redoblona</div>
                            </Grid>
                            < Grid item xs={1} style={{}}>
                                <Radio
                                    checked={true}
                                    onChange={handleChange2}
                                    value={true}
                                    color="default"
                                    name="radio-button-demo"
                                    inputProps={{ 'aria-label': 'F' }}
                                    size="small"
                                    style={{ padding: 0, margin: 0, }}
                                />
                            </Grid>
                            < Grid item xs={2} style={{ marginRight: '0.5%', marginLeft: '0.5%', }}>
                                <div type='number' style={{ textAlign: 'center', backgroundColor: '#eeeeee', borderRadius: 7, }}>
                                    <Input type='number' disableUnderline={true} />
                                </div>
                            </Grid>
                            < Grid item xs={1} style={{ marginRight: '0.5%', marginLeft: '0.5%', }}>
                                <Select
                                    labelId="demo-customized-select-label"
                                    id="demo-customized-select"
                                    value={age3}
                                    onChange={handleChange3}
                                    IconComponent="none"
                                    input={<BootstrapInput11 />}
                                    style={{ width: '100%', height: '100%', backgroundColor: '#eeeeee', borderRadius: 7, margin: 0, padding: 0, }}
                                >
                                    <MenuItem value={10}>01</MenuItem>
                                    <MenuItem value={20}>05</MenuItem>
                                    <MenuItem value={30}>10</MenuItem>
                                    <MenuItem value={40}>20</MenuItem>
                                </Select>
                            </Grid>
                            < Grid item xs={1} style={{ marginRight: '0.5%', marginLeft: '0.5%', }}>

                                <Select
                                    labelId="demo-customized-select-label"
                                    id="demo-customized-select"
                                    value={age4}
                                    onChange={handleChange4}
                                    IconComponent="none"
                                    input={<BootstrapInput11 />}
                                    style={{ width: '100%', height: '100%', backgroundColor: '#eeeeee', borderRadius: 7, margin: 0, padding: 0, }}
                                >
                                    <MenuItem value={10}>01</MenuItem>
                                    <MenuItem value={20}>05</MenuItem>
                                    <MenuItem value={30}>10</MenuItem>
                                    <MenuItem value={40}>20</MenuItem>
                                </Select>
                            </Grid>
                            < Grid item xs={2} style={{ marginRight: '0.5%', marginLeft: '0.5%', }}>
                                <div style={{ textAlign: 'center', backgroundColor: '#eeeeee', borderRadius: 7, }}>
                                    <Input type='number' disableUnderline={true} startAdornment={<InputAdornment position="start">$</InputAdornment>} />
                                </div>
                            </Grid>
                            < Grid item xs={1} style={{ marginRight: '0.5%', marginLeft: '0.5%', }} >
                                <IconButton style={{ padding: 0, margin: 0, alignContent: 'center', }}>
                                    <Cancel fontSize='small' style={{ color: 'red', marginLeft: '40%', }} />
                                </IconButton>
                            </Grid>
                            < Grid item xs={3} style={{}}>
                                <div style={{ textAlign: 'center', backgroundColor: '#eeeeee', borderRadius: 7, }}>
                                    <Input type='number' disableUnderline={true} startAdornment={<InputAdornment position="start">$</InputAdornment>} />
                                </div>
                            </Grid>
                        </Grid>
                    ) : null}
                </Grid >
            </Card >
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                <Grid container direction="row" justify="center" alignItems="center" style={{ width: '100%', height: '100%', }}>
                    {/* <Card elevation={5} className={classes.root11}> */}
                    <Card elevation={5} style={{ width: '90%', height: '90%', backgroundColor: 'white', borderRadius: 10, overflowY: 'scroll' }}>
                        <div style={{ justifyContent: 'flex-end', display: 'flex', }}>
                            <IconButton aria-label="delete" size="small" onClick={handleClose}>
                                <HighlightOff fontSize='small' style={{ color: 'grey', }} />
                            </IconButton>
                        </div>
                        <Grid
                            container
                            justify="center"
                            alignItems="center"
                            style={{}}
                        >
                            < Grid item xs={4} style={{ marginBottom: '5%' }}>
                                <div style={{ width: '100%', height: '100%', textAlign: 'center', fontFamily: '150% ', fontWeight: 'bold', }}>Num.</div>
                            </Grid>
                            < Grid item xs={4} style={{ marginBottom: '5%' }}>
                                <div style={{ width: '100%', height: '100%', textAlign: 'center', fontFamily: '150% ', fontWeight: 'bold', }}>U.Extr.</div>
                            </Grid>
                            < Grid item xs={4} style={{ marginBottom: '5%' }}>
                                <div style={{ width: '100%', height: '100%', textAlign: 'center', fontFamily: '150% ', fontWeight: 'bold', }}>Precio.</div>
                            </Grid>
                            {vistaModal.map((item, index) => {
                                return (
                                    <Fragment key={index}>
                                        < Grid item xs={4} style={{}}>
                                            <div style={{ width: '80%', height: '100%', textAlign: 'center', backgroundColor: '#eeeeee', borderRadius: 10, paddingTop: '3%', paddingBottom: '3%', marginLeft: '10%', marginBottom: '5%' }}>
                                                <input
                                                    type='text'
                                                    disableUnderline={true}
                                                    value={item.numero}
                                                    style={{ width: '50%', textAlign: 'center', backgroundColor: 'transparent', border: 'none', fontSize: '100%', padding: 8, outline: 0, }}
                                                />
                                            </div>
                                        </Grid>
                                        < Grid item xs={4} style={{}}>
                                            <div style={{ width: '80%', height: '100%', textAlign: 'center', backgroundColor: '#eeeeee', borderRadius: 10, paddingTop: '3%', paddingBottom: '3%', marginLeft: '10%', marginBottom: '5%' }}>
                                                <input
                                                    type='number'
                                                    disableUnderline={true}
                                                    value={item.posicion}
                                                    style={{ width: '50%', textAlign: 'center', backgroundColor: 'transparent', border: 'none', fontSize: '100%', padding: 8, outline: 0, }}
                                                />
                                            </div>
                                        </Grid>
                                        < Grid item xs={4} style={{}}>
                                            <div style={{ width: '80%', height: '100%', textAlign: 'center', backgroundColor: '#eeeeee', borderRadius: 10, paddingTop: '3%', paddingBottom: '3%', marginLeft: '10%', marginBottom: '5%' }}>
                                                <input
                                                    type='number'
                                                    disableUnderline={true}
                                                    value={item.importe}
                                                    style={{ width: '50%', textAlign: 'center', backgroundColor: 'transparent', border: 'none', fontSize: '100%', padding: 8, outline: 0, }}
                                                />
                                            </div>
                                        </Grid>
                                    </Fragment>
                                )
                            })}
                        </Grid>
                    </Card>
                </Grid>
            </Modal>

            <Modal
                open={open22}
                onClose={handleClose22}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                <Grid container direction="row" justify="center" alignItems="center" style={{ width: '100%', height: '100%', }}>
                    {/* <Card elevation={5} className={classes.root11}> */}
                    <Card elevation={5} style={{ width: '70%', backgroundColor: 'white', borderRadius: 10, }}>
                        <div style={{ justifyContent: 'flex-end', display: 'flex', }}>
                            <IconButton aria-label="delete" size="small" onClick={handleClose22}>
                                <HighlightOff fontSize='small' style={{ color: 'grey', }} />
                            </IconButton>
                        </div>
                        <div style={{ margin: '4%', textAlign: 'center', marginTop: '10%' }} onClick={handleClose22}>
                            <IconButton onClick={handleClose22}>
                                <ErrorOutline style={{ fontSize: '400%', color: '#f7d400', }} />
                            </IconButton>
                            <h1 style={{ fontSize: '100%', fontFamily: "Roboto", textAlign: 'center', }}>¡Maximo 3-4 digitos!</h1>
                        </div>
                    </Card>
                </Grid>
            </Modal>
            <Button
                variant="contained"
                style={{ backgroundColor: '#ff7f00', }}
                size="small"
                className={classes.viewColumn9}
                onClick={() => {
                    // let tempp = []
                    let total_aumentar = 0
                    for (let index in vistaModal) {
                        total_aumentar = total_aumentar + vistaModal[index].importe
                    }
                    let dup2 = vistaModal
                    for (let index in linea1) {
                        // tempp = [...vistaModal, linea1]
                        // 
                        vistaModal.push(linea1[index])
                        total_aumentar = total_aumentar + linea1[index].importe
                    }
                    setVistaModal([...vistaModal])
                    // console.log('visu', linea1, vistaModal, tempp);
                    setTotal(total_aumentar)
                    setLinea1([{
                        check: false,
                        numero: '',
                        posicion: '',
                        importe: '',
                        tipo: 0,
                    }])
                }}
            >
                <div className={classes.letra2}>Agregar al carro y seguir jugando</div>
            </Button>
            < Fotter />
        </Grid >

    )
}

const useStyles = makeStyles((theme) => ({
    
    root: {
        display: 'flex',
        width: '100%',
        zIndex: 1,
        backgroundColor: 'white',
        borderRadius: 10,
        // marginBottom: '15%',
    },
    root1: {
        display: 'flex',
        width: '100%',
        zIndex: 1,
        // backgroundColor: 'white',
        marginTop: '5%',
        marginLeft: '1%',
        // marginBottom: '15%',
    },
    root3: {
        // display: 'flex',
        // width: '100%',
        // zIndex: 1,
        // backgroundColor: 'white',
        // marginTop: '5%',
        // marginTop: '5%',
        // marginLeft: '3%',
        marginBottom: '4%',

    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        // width: 151,
        width: 90,
        height: 80,
        alignContent: 'center',
        justifyContent: 'center',
        borderRadius: 7,
        borderColor: '#eeeeee',
        border: '2px solid',
    },
    letra: {
        fontSize: 12,
    },
    viewColumn7: {
        backgroundColor: 'transparent',
        alignContent: 'center',
        justifyContent: 'center',
        // margin: '4%',
        // height: '1%',
        marginTop: '1%',
        marginBottom: '2%',
        marginLeft: '2%',
        marginRight: '1%',
        borderRadius: 7,
        backgroundColor: '#79796a',
        // borderWidth: 100,
        // border: '2px solid',
        width: '45%',
    },
    viewColumn8: {
        backgroundColor: 'transparent',
        alignContent: 'center',
        justifyContent: 'center',

        marginTop: '1%',
        marginBottom: '2%',
        marginLeft: '1%',
        marginRight: '1%',
        borderRadius: 7,
        backgroundColor: '#ff7f00',

        width: '45%',
    },
    viewColumn9: {
        width: '96%',
        // backgroundColor: 'transparent',
        // alignContent: 'center',
        justifyContent: 'center',
        // margin: '4%',
        // height: '1%',
        marginTop: '10%',
        marginBottom: '20%',
        // marginLeft: '4%',
        // marginRight: '4%',
        borderRadius: 7,
        backgroundColor: '#ff7f00',
        // borderWidth: 100,
        // border: '2px solid',
        // position: 'fixed',
        // bottom: 0,

    },
    letra2: {
        fontFamily: "Roboto",
        // fontWeight: 'bold',
        fontSize: '130%',
        margin: '4%',
        color: 'white',
        textTransform: 'none',
    },
    letra3: {
        fontSize: '130%',
        // margin: '3%',
        color: 'white',
        fontWeight: 'bold',
    },
    svg: {

        width: '90%', height: '90%',
        // marginLeft: '20%',
        marginTop: '5%',
        // marginBottom: '5%',
    },

}));

const BootstrapInput = withStyles((theme) => ({
    root: {
        'label + &': {
            marginTop: 0,
        },
    },
    input: {
        borderRadius: 7,
        // position: 'relative',
        backgroundColor: 'transparent',
        border: '0px solid transparent',
        fontSize: 13,
        // marginLeft: '3%',
        // margin: '0%',
        padding: '7px 26px 7px 12px',
        // transition: theme.transitions.create(['border-color', 'box-shadow']),
        // Use the system font instead of the default Roboto font.
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:focus': {
            // width: -10,
            backgroundColor: 'transparent',
            // height: 40,
            borderRadius: 4,
            borderColor: 'transparent',
            // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
            // marginLeft: '3%',
            // width: 1,
        },

    },
}))(InputBase);

const BootstrapInput11 = withStyles((theme) => ({
    root: {
        'label + &': {
            marginTop: 0,
        },
    },
    input: {
        borderRadius: 7,
        // position: 'relative',
        backgroundColor: 'transparent',
        border: '0px solid transparent',
        fontSize: 12,
        // marginLeft: '3%',
        // margin: 0,
        // padding: 0,
        padding: '7px 26px 7px 6px',
        // transition: theme.transitions.create(['border-color', 'box-shadow']),
        // Use the system font instead of the default Roboto font.
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:focus': {
            // width: -10,
            backgroundColor: 'transparent',
            // height: 40,
            borderRadius: 4,
            borderColor: 'transparent',
            // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
            // marginLeft: '3%',
            // width: 1,
        },

    },
}))(InputBase);