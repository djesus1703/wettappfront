import React, { useEffect, useState } from "react";
import { Grid, makeStyles } from "@material-ui/core";
import moment from "moment";
import { ReactComponent as Logo } from "../assets/prestador2.svg";
import Header from "../components/Header";
import Fotter from "../components/Fotter";
import Alert from "../components/Alert";
import JuegoNumeros from "../components/JuegoNumeros";

const VALOR_JUGADA_QUINELAPLUS = 20;
const CANTIDAD_CAMPOS = 8;
const CIERRE_JUEGO = [
  {
    dia: "Saturday",
    hora: "21:10",
  },
  {
    dia: "Sunday",
  },
];

export default function QuinielaPlus() {
  const [alertCloseGame, setAlertCloseGame] = useState(false);
  const [campos, setCampos] = useState({
    campo0: "",
    campo1: "",
    campo2: "",
    campo3: "",
    campo4: "",
    campo5: "",
    campo6: "",
    campo7: "",
  });

  useEffect(() => {
    const date = moment().format("HH:mm");
    const day = moment().format("dddd");

    CIERRE_JUEGO.forEach((item) => {
      if (date >= item.hora && day === item.dia) {
        setAlertCloseGame(true);
      } else if (day === "Sunday") {
        setAlertCloseGame(true);
      }
    });
  }, []);

  const rango = { inicio: 0, fin: 99 };

  return (
    <Grid container direction="row" justify="center">
      <Header />
      <JuegoNumeros
        Logo={Logo}
        nombre="Quinela Plus"
        rango={rango}
        valorJugada={VALOR_JUGADA_QUINELAPLUS}
        campos={campos}
        setCampos={setCampos}
        cantidad={CANTIDAD_CAMPOS}
        juega="Todos los días por provincia nocturna"
      />
      <Fotter />
      <Alert
        openAlert={alertCloseGame}
        setOpenAlert={setAlertCloseGame}
        closeGame
        title="Quinela a cerrado"
        message="Este juego a cerrado te deseamos la mejor suerte!"
      />
    </Grid>
  );
}

const styles = makeStyles({
  svg: {
    width: "80%",
    height: "80%",
  },
});
