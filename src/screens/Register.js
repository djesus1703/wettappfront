import React, { useRef, useState } from "react";
import {
  Grid,
  makeStyles,
  Card,
  Modal,
  Button,
  TextField,
} from "@material-ui/core";
import QrReader from "react-qr-reader";
import imagen2 from "../../src/assets/icon_inicio_3.png";
import Header from "../components/Header";
import BannerLogin from "../components/BannerLogin";
import styles from "../common/styles";
import Api from "../config/Api";
import Alert from "../components/Alert";
import ModalPolitcs from "../components/ModalPolitcs";
import { useHistory } from "react-router-dom";

export default function Register() {
  const classes = useStyles();
  const history = useHistory();
  const style = styles();
  // const values = {
  //   someDate: "Fecha",
  // };
  // const dateNow = new Date(); // Creating a new date object with the current date and time
  // const year = dateNow.getFullYear(); // Getting current year from the created Date object
  // const monthWithOffset = dateNow.getUTCMonth() + 1; // January is 0 by default in JS. Offsetting +1 to fix date for calendar.

  // const month = // Setting current Month number from current Date object
  // monthWithOffset.toString().length < 2 // Checking if month is < 10 and pre-prending 0 to adjust for date input.
  //   ? `0${monthWithOffset}`
  //   : monthWithOffset;

  // const date =
  //   dateNow.getUTCDate().toString().length < 2 // Checking if date is < 10 and pre-prending 0 if not to adjust for date input.
  //     ? `0${dateNow.getUTCDate()}`
  //     : dateNow.getUTCDate();

  // const materialDateInput = `${year}-${month}-${date}`;
  // const [age, setAge] = useState("");
  // const handleChange = (event) => {
  //   setAge(event.target.value);
  // };
  // const handleChange2 = (event) => {
  //   setSelectedValue(event.target.value);
  // };

  // const [selectedValue, setSelectedValue] = useState("a");
  // const [currency, setCurrency] = useState("EUR");

  const [open, setOpen] = useState(true);
  const [capturador, setCapturador] = useState(null);
  const qrReader = useRef(null);
  const [legacyMode, setLegacyMode] = useState(false);

  const [agencia, setAgencia] = useState({});
  const [codigo_client, setCodigo_client] = useState("");
  const [name, setName] = useState("");
  const [apellido, setApellido] = useState("");
  const [nacimiento, setNacimiento] = useState("");
  const [email, setEmail] = useState("");
  const [telefono, setTelefono] = useState("");
  const [dni, setDni] = useState("");
  const [ciudad, setCiudad] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordComfirm] = useState("");
  const [openAlertSuccess, setOpenAlertSuccess] = useState(false);

  const handleScan = (data) => {
    const dataArray = JSON.parse(data);
    if (data) {
      setCapturador(data);
      setOpen(false);
      setAgencia({
        'id': dataArray.id,
      });
    } else {
      if (legacyMode) {
        setOpenAlert(true);
        setErrorText("No pudo leer el Qr. Intentelo nuevamente.");
      }
    }
  };
  const handleError = (err) => {
    setLegacyMode(true);
    setOpenAlert(true);
    setErrorText("No pudimos acceder a la camara de su dispositivo. Adjunta la imagen manualmente");
  };
  const openImageDialog = () => {
    qrReader.current.openImageDialog()
  };
  const [open2, setOpen2] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [errorText, setErrorText] = useState("");

  const handleOpen2 = () => {
    if (
      name === "" ||
      apellido === "" ||
      nacimiento === "" ||
      email === "" ||
      telefono === "" ||
      dni === "" ||
      ciudad === "" ||
      password === "" ||
      passwordConfirm === ""
    ) {
      setOpenAlert(true);
      setErrorText("Todos los campos deben ser completados");
    } else if (password !== passwordConfirm) {
      setOpenAlert(true);
      setErrorText("Las contraseñas no son iguales");
    } else {
      console.log("agencia", agencia);
      Api.post({
        url: "/test",
        data: {
          agencia_id: agencia.id,
          codigo_client: codigo_client,
          nombre: name,
          apellido: apellido,
          nacimiento: nacimiento,
          email: email,
          telefono: telefono,
          dni: dni,
          ciudad: ciudad,
          password: password,
        },
      }).then((res) => {
        console.log("SI GUARDO", res);
        setOpen2(true);
      }).catch(error => {
        setOpenAlert(true);
        let errorDescription = Api.responseStatus(error.response, error.response.status);;
        setErrorText(errorDescription.text);

      });
    }
  };
  const handleClose2 = () => {
    setOpen2(false);
    setOpenAlertSuccess(true)
  };
  const handleCloseSuccess = () => {
    //login
    history.replace("/");
  };

  const dataUserRegister = [
    {
      value: name,
      type: "text",
      label: "Nombre",
      setState: setName
    },
    {
      value: apellido,
      type: "text",
      label: "Apellido",
      setState: setApellido
    },
    {
      value: nacimiento,
      type: "date",
      label: "",
      setState: setNacimiento,
    },
    { value: email, type: "email", label: "Email", setState: setEmail },
    {
      value: telefono,
      type: "number",
      label: "Teléfono",
      setState: setTelefono,
    },
    { value: ciudad, type: "text", label: "Ciudad", setState: setCiudad },
    { value: dni, type: "number", label: "DNI", setState: setDni },
    {
      value: password,
      type: "password",
      label: "Contraseña",
      setState: setPassword,
    },
    {
      value: passwordConfirm,
      type: "password",
      label: "Confirmar contraseña",
      setState: setPasswordComfirm,
    },
  ];

  return (
    <Grid container direction="row" justify="center">
      <Header />
      <Card elevation={0} className={classes.root}>
        <Grid container item xs={12} className={classes.root1}>
          <BannerLogin imagen={imagen2} tittle="Hola!" />
          <div className={classes.titleCard}>
            Por favor ingresa tus datos para registrarte:
          </div>
          {dataUserRegister.map((data) => (
            <Grid
              key={data.label}
              item
              xs={12}
              style={{ marginTop: "2%", marginBottom: "2%" }}
            >
              <TextField
                required
                type={data.type}
                label={data.label}
                variant="outlined"
                value={data.value}
                className={style.inputText}
                onChange={(text) => data.setState(text.target.value)}
              />
            </Grid>
          ))}
          <Modal
            open={open}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
          >
            <div className={classes.containerQR}>
              {capturador == null ? (
                <QrReader
                  delay={100}
                  style={{
                    height: "80%",
                    width: "100%",
                  }}
                  onError={handleError}
                  onScan={(data) => {
                    handleScan(data);
                  }}
                  legacyMode={legacyMode}
                  ref={qrReader}
                />
              ) : null}
              <div className={style.titleModal}>
                Toma QR Code de tu Agencia Loteria, para continuar con tu
                registro.
                {legacyMode ? (<p><input type="button" value="Adjuntar QR" onClick={() => openImageDialog()} /></p>) : ('')}
              </div>
            </div>
          </Modal>
        </Grid>
      </Card>
      <Button
        variant="contained"
        size="small"
        className={`${style.button} ${style.buttonColorTertiary} ${style.buttonText} ${style.finalMargin}`}
        onClick={handleOpen2}
      >
        Registrarme
      </Button>
      <ModalPolitcs open={open2} close={handleClose2} />
      {/*<Fotter />*/}
      <Alert
        openAlert={openAlert}
        setOpenAlert={setOpenAlert}
        error
        title={"Upss!!"}
        message={errorText}
      />
      <Alert
        openAlert={openAlertSuccess}
        setOpenAlert={handleCloseSuccess}
        title={"Registro satisfactorio"}
        message={"Verifique su correo antes de continuar."}
      />

    </Grid>
  );
}
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    marginBottom: "5%",
    borderRadius: 10,
  },
  root1: {
    display: "flex",
    width: "100%",
    zIndex: 1,
    backgroundColor: theme.colors.white,
    marginTop: "5%",
    margin: "5%",
  },
  titleCard: {
    fontFamily: theme.fonts.primary,
    margin: "3%",
    fontSize: "80%",
  },
  containerQR: {
    width: "100%",
    height: "100%",
    backgroundColor: theme.colors.white,
  },
}));

// const currencies = [
//   {
//     value: "USD",
//     label: "Agencia 1",
//   },
//   {
//     value: "EUR",
//     label: "Agencia 2",
//   },
//   {
//     value: "BTC",
//     label: "Agencia 3",
//   },
//   {
//     value: "JPY",
//     label: "Agencia 4",
//   },
// ];
